export CC=gcc
export CFLAGS=-Wall -ansi -pedantic -O3 -g
export LDFLAGS=-lm -lallegro -lallegro_image -lallegro_audio -lallegro_acodec -lallegro_primitives

export PREFIX=$(shell pwd)
export SRCDIR= $(PREFIX)/src
export DATADIR= $(PREFIX)/data

export BINDIR=$(PREFIX)/package
export BUILDDIR=$(PREFIX)/build

export BIN=$(BINDIR)/opera

OBJ=$(wildcard $(BUILDDIR)/*.o)

all:stable

debug:
	$(MAKE) -C $(SRCDIR)
	$(CC) $(CFLAGS) -o -g $(BIN) $(OBJ) $(LDFLAGS)

stable:
	$(MAKE) -C $(SRCDIR)
	$(CC) $(CFLAGS) -o $(BIN) $(OBJ) $(LDFLAGS)

clean:
	rm -vfr *~ $(BIN)
