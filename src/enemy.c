#include "enemy.h"
#include "enemy_shot.h"
#include "callback.h"
#include "extern.h"
#include "game.h"



/* run straight ahead */
int NewEnemy1(int x, int y)
{
    /* attribute of this object */
    manage->new_obj.data.hit_att = MENEMY;
    /* what objects should hit this object */
    manage->new_obj.data.hit_mask = MPLAYER | MPSHOT;

    manage->new_obj.data.hp = 1;
    manage->new_obj.data.point = 100;
    manage->new_obj.data.enemy_att = ZakoDel;

    /* size for collision detection */
    manage->new_obj.data.width = 32;
    manage->new_obj.data.height = 32;

    manage->new_obj.data.x = x;
    manage->new_obj.data.y = 1 - manage->new_obj.data.height/2;

    manage->new_obj.data.angle = 0;
    manage->new_obj.data.speed = rand()%15 + 1;

    manage->new_obj.data.cnt[0] = 0;

    /* pixmap for this object */
    manage->new_obj.grp.image = &enemy1_bitmap;
    manage->new_obj.grp.sprites = 8;

    /* add action function and hit function to the table */
    return new_obj(MENEMY, EnemyAct1, EnemyHit1, DrawImage);
}

DelAtt EnemyAct1(ObjData *self)
{
    int i;
    if (manage->loop > 1)
    {
        /* shoot in loop 2 or later */
        if (self->shot_time >= SHOT_TIMING)
        {
            for (i =1; i< manage->loop; i++)
                shot_to_point(self->x, self->y, manage->player[0]->data.x, manage->player[0]->data.y, 5);
            self->shot_time = self->start_time;
        }
        else
            self->shot_time++;
    }

    self->image = 4;
    self->y += self->speed;

    if ((self->x < 0 - self->width/2) || (self->x > fieldw + self->width/2))
        return NullDel;
    if ((self->y < 0 - self->height/2) || (self->y > fieldh + self->height/2))
        return NullDel;

    return NoneDel;
}


/* may give you an item */
DelAtt EnemyHit1(ObjData *self, ObjData *your)
{
    int temp = your->attack;
    if (self->hp < your->attack)
        temp = self->hp;

    self->hp -= temp;
    player->rec[0].score += temp;

    if (self->hp <= 0)
    {
        player->rec[0].score -= 1;

        /* always gives you a shot in loop 3 or later */
        if (manage->loop > 2)
            shot_to_point(self->x, self->y, manage->player[0]->data.x, manage->player[0]->data.y, 5);

        if (rand()%100 < 20)
            NewEnemy10(self->x, self->y);
        new_bomb(self->x, self->y);

        self->show_damage_time = 0;
        return self->enemy_att;
    }
    else
    {
        self->show_damage_time = 15;
        return NoneDel;
    }
}


/* codename "sine curve" (in fact it is a parabola) */
int NewEnemy2(int x, int y)
{

    manage->new_obj.data.hit_att = MENEMY;
    manage->new_obj.data.hit_mask = MPLAYER | MPSHOT;

    manage->new_obj.data.hp = 1;
    manage->new_obj.data.point = 100;
    manage->new_obj.data.enemy_att = ZakoDel;

    manage->new_obj.data.width = 32;
    manage->new_obj.data.height = 32;

    manage->new_obj.data.x = x;
    manage->new_obj.data.y = 1 - manage->new_obj.data.height/2;

    manage->new_obj.data.inertx = 0;
    manage->new_obj.data.inerty = 1;

    manage->new_obj.data.angle = 0;
    manage->new_obj.data.speed = 0;
    manage->new_obj.data.cnt[0] = 0;
    /* range of x */
    manage->new_obj.data.cnt[1] = rand()%3 + 5;
    if (x < fieldw/2)
        manage->new_obj.data.cnt[2] = 0;
    else
        manage->new_obj.data.cnt[2] = 1;

    manage->new_obj.data.cnt[3] = 0;

    manage->new_obj.grp.image = &enemy3_bitmap;
    manage->new_obj.grp.sprites = 8;

    return new_obj(MENEMY, EnemyAct2, DamageHit, DrawImage);
}


DelAtt EnemyAct2(ObjData* self)
{
    if (self->shot_time >= SHOT_TIMING)
    {
        if (manage->loop > 1)
            laser_shot(self->x,self->y, 10);
        shot_to_point(self->x,self->y, manage->player[0]->data.x, manage->player[0]->data.y, 4);
        self->shot_time = self->start_time;
    }
    else
        self->shot_time++;

    if (self->cnt[2] == 0)
    {
        self->cnt[3]++;

        if (self->cnt[3] >= 2*self->cnt[1])
            self->cnt[2] = 1;
    }
    else if (self->cnt[2] == 1)
    {
        self->cnt[3]--;

        if (self->cnt[3] <= -2*self->cnt[1])
            self->cnt[2] = 0;
    }

    self->y += self->cnt[1];
    self->x += self->cnt[3];

    /* what number of pixmap we use */
    if (self->cnt[3] > self->cnt[1])
        self->image = 2;
    else if (self->cnt[3] > 0)
        self->image = 3;
    else if (self->cnt[3] < -self->cnt[1])
        self->image = 6;
    else if(self->cnt[3] < 0)
        self->image = 5;
    else
        self->image = 0;

    if ((self->y < 0 - self->height/2) || (self->y > fieldh + self->height/2))
        return NullDel;

    return NoneDel;
}


/* turns left/right */
int NewEnemy3(int x, int y)
{

    manage->new_obj.data.hit_att = MENEMY;
    manage->new_obj.data.hit_mask = MPLAYER | MPSHOT;

    manage->new_obj.data.hp = 2;
    manage->new_obj.data.point = 150;
    manage->new_obj.data.enemy_att = ZakoDel;

    manage->new_obj.data.width = 32;
    manage->new_obj.data.height = 32;

    manage->new_obj.data.x = x;
    manage->new_obj.data.y = 1 - manage->new_obj.data.height/2;

    manage->new_obj.data.cnt[0] = 0;
    manage->new_obj.data.cnt[1] = 0;
    manage->new_obj.data.cnt[2] = 5;

    manage->new_obj.grp.image = &enemy1_bitmap;
    manage->new_obj.grp.sprites = 8;
    
    return new_obj(MENEMY, EnemyAct3, EnemyHit1, DrawImage);
}

DelAtt EnemyAct3(ObjData *self)
{
    if (self->shot_time >= SHOT_TIMING)
    {
        shot_to_point(self->x,self->y,manage->player[0]->data.x,manage->player[0]->data.y,4);
        self->shot_time = self->start_time;
    }
    else
        self->shot_time++;

    if (self->cnt[0] == 0)
    {
        if (self->y<manage->player[0]->data.y+10 && self->y>manage->player[0]->data.y-10)
        {
            if (self->x < manage->player[0]->data.x)
            {
                self->cnt[0] = 1;
                if (manage->loop >2)
                    bound_shot(self->x, self->y, rand()%30-15, rand()%30-15, 3);
            }

            if (self->x > manage->player[0]->data.x)
            {
                self->cnt[0] = 2;
                if (manage->loop >2)
                    bound_shot(self->x, self->y, rand()%30-15, rand()%30-15, 3);
            }
        }
    }

    if (self->cnt[0] == 0)
    {
        self->y += 2 + manage->loop * 3;
        self->image = 4;
    }
    else if (self->cnt[0] == 1)
    {
        self->x += 15;
        self->image = 2;
    }
    else if (self->cnt[0] == 2)
    {
        self->x -= 15;
        self->image = 6;
    }

    if ((self->x < 0 - self->width/2) || (self->x > fieldw + self->width/2))
        return NullDel;
    if ((self->y < 0 - self->height/2) || (self->y > fieldh + self->height/2))
        return NullDel;

    return NoneDel;
}


/* chases you */
int NewEnemy4(int x, int y)
{

    manage->new_obj.data.hit_att = MENEMY;
    manage->new_obj.data.hit_mask = MPLAYER | MPSHOT;

    manage->new_obj.data.hp = 2;
    manage->new_obj.data.point = 150;
    manage->new_obj.data.enemy_att = ZakoDel;

    manage->new_obj.data.width = 32;
    manage->new_obj.data.height = 32;

    manage->new_obj.data.x = x;
    manage->new_obj.data.y = 1 - manage->new_obj.data.height/2;

    manage->new_obj.data.inertx = 0;
    manage->new_obj.data.inerty = 0;

    manage->new_obj.grp.image = &enemy2_bitmap;
    manage->new_obj.grp.sprites = 8;
    
    return new_obj(MENEMY, EnemyAct4, DamageHit, DrawImage);
}

DelAtt EnemyAct4(ObjData *self)
{
    if (self->shot_time >= SHOT_TIMING)
    {
        shot_to_point(self->x,self->y,manage->player[0]->data.x,manage->player[0]->data.y,4);
        if (manage->loop >1)
        {
            shot_to_angle(self->x, self->y, 90, 4);
            shot_to_angle(self->x, self->y, 90, 8);
            shot_to_angle(self->x, self->y, 90, 12);
            shot_to_angle(self->x, self->y, 90, 16);
        }

        self->shot_time = self->start_time;
    }
    else
        self->shot_time++;

    if (self->x < manage->player[0]->data.x)
    {
        if (self->inertx <= 8)
            self->inertx++;
    }
    else if (self->x > manage->player[0]->data.x)
    {
        if (self->inertx >= -8)
            self->inertx--;
    }

    if (self->y < manage->player[0]->data.y)
    {
        if (self->inerty <= 8)
            self->inerty++;
    }
    else if (self->y > manage->player[0]->data.y)
    {
        if (self->inerty >= -8)
            self->inerty--;
    }

    self->x += self->inertx;
    self->y += self->inerty;

    self->image = get_direction(self->x,self->y,manage->player[0]->data.x,manage->player[0]->data.y);

    if ((self->x < 0 - self->width/2) || (self->x > fieldw + self->width/2))
        return NullDel;
    if ((self->y < 0 - self->height/2) || (self->y > fieldh + self->height/2))
        return NullDel;

    return NoneDel;
}

/* bounce on the left/right side of the window */
int NewEnemy5(int x, int y)
{

    manage->new_obj.data.hit_att = MENEMY;
    manage->new_obj.data.hit_mask = MPLAYER | MPSHOT;

    if (x > fieldw/2)
    {
        manage->new_obj.data.x = fieldw;
        manage->new_obj.data.cnt[0] = - (rand()%10 + 15);
    }
    else
    {
        manage->new_obj.data.x = 0;
        manage->new_obj.data.cnt[0] = rand()%10 + 15;
    }

    manage->new_obj.data.hp = 9;
    manage->new_obj.data.point = 200;
    manage->new_obj.data.enemy_att = ZakoDel;

    manage->new_obj.data.width = 32;
    manage->new_obj.data.height = 32;

    manage->new_obj.data.y = 1 - manage->new_obj.data.height/2;

    manage->new_obj.data.inertx = 0;
    manage->new_obj.data.inerty = 0;

    manage->new_obj.grp.image = &enemy5_bitmap;
    manage->new_obj.grp.sprites = 4;
    
    return new_obj(MENEMY, EnemyAct5, EnemyHit1, DrawImage);
}

DelAtt EnemyAct5(ObjData *my)
{
    if (my->cnt[0] < 0)
    {
        my->inertx++;
        if (abs(my->inertx+my->cnt[0]) < 5)
            my->image = 3;
        else
            my->image = 2;
    }
    else
    {
        my->inertx--;
        if (abs(my->inertx+my->cnt[0]) < 5)
            my->image = 1;
        else
            my->image = 0;
    }

    if (my->cnt[0]+my->inertx == 0)
    {
        ring_to_point(my->x, my->y, manage->player[0]->data.x, manage->player[0]->data.y, rand()%4+2);
        ring_to_angle(my->x, my->y, 45, 5);
        ring_to_angle(my->x, my->y, 90, 5);
        ring_to_angle(my->x, my->y, 135, 5);
        ring_to_point(my->x, my->y, manage->player[0]->data.x, manage->player[0]->data.y, rand()%2+4);
        if (manage->loop > 1)
        {
            ring_to_point(my->x,my->y,manage->player[0]->data.x,manage->player[0]->data.y,rand()%3+3);
            ring_to_point(my->x,my->y,manage->player[0]->data.x,manage->player[0]->data.y,rand()%4+4);
        }
    }

    my->x += (my->cnt[0] + my->inertx);
    my->y += 5;

    if ((my->x < 0) || (my->x > fieldw))
        my->inertx = 0;

    if ((my->y < 0 - my->height/2) || (my->y > fieldh + my->height/2))
        return NullDel;

    return NoneDel;
}

/* bounce on any side of the window */
int NewEnemy6(int x, int y)
{

    manage->new_obj.data.hit_att = MENEMY;
    manage->new_obj.data.hit_mask = MPLAYER | MPSHOT;

    manage->new_obj.data.hp = 4;
    manage->new_obj.data.point = 200;
    manage->new_obj.data.enemy_att = ZakoDel;

    manage->new_obj.data.width = 32;
    manage->new_obj.data.height = 32;

    manage->new_obj.data.x = x;
    manage->new_obj.data.y = 1 - manage->new_obj.data.height/2;

    if (x > fieldw/2)
        manage->new_obj.data.inertx = rand()%5 + 10;
    else
        manage->new_obj.data.inertx = rand()%5 - 15;

    manage->new_obj.data.inerty = rand()%10 + 5;

    manage->new_obj.data.cnt[0] = 0;
    manage->new_obj.data.cnt[1] = 8;
    /* "appeared" counter */
    manage->new_obj.data.cnt[2] = 0;

    manage->new_obj.grp.image = &enemy4_bitmap;
    manage->new_obj.grp.sprites = 8;
    
    return new_obj(MENEMY, EnemyAct6, DamageHit, DrawImage);
}

DelAtt EnemyAct6(ObjData *self)
{
    if (self->shot_time >= SHOT_TIMING)
    {
        shot_to_point(self->x,self->y,manage->player[0]->data.x,manage->player[0]->data.y,rand()%4+ manage->loop * 7 -5);
        self->shot_time = self->start_time;
    }
    else
        self->shot_time++;

    if ((self->cnt[2] != 0) && (self->cnt[0] <= self->cnt[1]))
    {
        if ((self->x+self->inertx>fieldw) || (self->x+self->inertx<0))
        {
            self->inertx = self->inertx*(-1);
            self->cnt[0]++;
        }
        if ((self->y+self->inerty>fieldh) || (self->y+self->inerty<0))
        {
            self->inerty = self->inerty*(-1);
            self->cnt[0]++;
        }
    }

    self->image++;
    if (self->image > 7)
        self->image = 0;

    self->x += self->inertx;
    self->y += self->inerty;

    if ((self->x < 0 - self->width/2) || (self->x > fieldw + self->width/2))
        return NullDel;
    if ((self->y < 0 - self->height/2) || (self->y > fieldh + self->height/2))
        return NullDel;

    if ((self->cnt[2] == 0) && (self->x >= 0) && (self->x <= fieldw)
            && (self->y >= 0) && (self->y < fieldh))
        self->cnt[2] = 1;

    return NoneDel;
}


/* appears from the lower side of the window */
int NewEnemy7(int x, int y)
{

    manage->new_obj.data.hit_att = MENEMY;
    manage->new_obj.data.hit_mask = MPLAYER | MPSHOT;

    if (x > fieldw/2)
        manage->new_obj.data.x = fieldw -50;
    else
        manage->new_obj.data.x = 50;

    manage->new_obj.data.hp = 3;
    manage->new_obj.data.point = 250;
    manage->new_obj.data.enemy_att = ZakoDel;

    manage->new_obj.data.width = 32;
    manage->new_obj.data.height = 32;

    manage->new_obj.data.y = fieldh - 1 + manage->new_obj.data.height/2;

    manage->new_obj.data.cnt[1] = 0;

    manage->new_obj.grp.image = &enemy3_bitmap;
    manage->new_obj.grp.sprites = 8;

    return new_obj(MENEMY, EnemyAct7, EnemyHit1, DrawImage);
}

DelAtt EnemyAct7(ObjData *self)
{
    if (manage->loop > 1)
    {
        if (self->shot_time >= SHOT_TIMING)
        {
            homing_shot(self->x,self->y,0,0);
            self->shot_time = self->start_time;
        }
        else
            self->shot_time++;
    }

    if (self->cnt[1] > 20)
    {
        self->y -= 9;
        self->image = 0;
    }
    else if (self->y > fieldh-100)
    {
        self->y -= 6;
        self->image = 0;
    }
    else
    {
        self->cnt[1]++;
        self->image++;
        if (self->image > 7)
            self->image = 0;
    }

    if (self->cnt[1] == 20)
    {
        shot_to_angle(self->x, self->y, 144, 10);
        shot_to_angle(self->x, self->y, 180, 10);
        shot_to_angle(self->x, self->y, 216, 10);
        shot_to_angle(self->x, self->y, 252, 10);
        shot_to_angle(self->x, self->y, 288, 10);
        shot_to_angle(self->x, self->y, 324, 10);
        shot_to_angle(self->x, self->y, 360, 10);
        shot_to_angle(self->x, self->y, 36, 10);
    }

    if ((self->x < 0 - self->width/2) || (self->x > fieldw + self->width/2))
        return NullDel;
    if ((self->y < 0 - self->height/2) || (self->y > fieldh + self->height/2))
        return NullDel;

    return NoneDel;
}

int NewEnemy8(int x, int y)
{

    manage->new_obj.data.hit_att = MENEMY;
    manage->new_obj.data.hit_mask = MPLAYER | MPSHOT;

    manage->new_obj.data.hp = 15;
    manage->new_obj.data.point = 500;
    manage->new_obj.data.enemy_att = ZakoDel;

    manage->new_obj.data.width = 40;
    manage->new_obj.data.height = 50;

    manage->new_obj.data.x = x;
    manage->new_obj.data.y = 1 - manage->new_obj.data.height/2;

    manage->new_obj.data.angle = 0;
    manage->new_obj.data.speed = rand()%5 + 1;

    manage->new_obj.grp.image = &enemy7_bitmap;
    manage->new_obj.grp.sprites = 1;

    return new_obj(MENEMY, EnemyAct8, LargeDamageHit, DrawImage);
}

DelAtt EnemyAct8(ObjData *self)
{
    if (self->shot_time >= SHOT_TIMING)
    {
        homing_shot(self->x+10,self->y-20,-10,-5);
        homing_shot(self->x-10,self->y-20,10,-5);

        if (manage->loop > 1)
        {
            laser_shot(self->x-15,self->y-20,10);
            laser_shot(self->x+15,self->y-20,10);
        }
        self->shot_time = self->start_time;
    }
    else
        self->shot_time++;

    self->y += self->speed;

    if ((self->x < 0 - self->width/2) || (self->x > fieldw + self->width/2))
        return NullDel;
    if ((self->y < 0 - self->height/2) || (self->y > fieldh + self->height/2))
        return NullDel;

    return NoneDel;
}

int NewEnemy9(int x, int y)
{
    manage->new_obj.data.hit_att = MENEMY;
    manage->new_obj.data.hit_mask = MPLAYER | MPSHOT;

    manage->new_obj.data.hp = 6;
    manage->new_obj.data.point = 300;
    manage->new_obj.data.enemy_att = ZakoDel;

    manage->new_obj.data.width = 32;
    manage->new_obj.data.height = 32;

    manage->new_obj.data.x = x;
    manage->new_obj.data.y = 1 - manage->new_obj.data.height/2;

    if (x > fieldw/2)
        manage->new_obj.data.inertx = -(rand()%10+1);
    else
        manage->new_obj.data.inertx = rand()%10+1;
    manage->new_obj.data.inerty = rand()%10 + 15;

    manage->new_obj.data.cnt[0] = 0;

    manage->new_obj.grp.image = &enemy6_bitmap;
    manage->new_obj.grp.sprites = 6;
    
    return new_obj(MENEMY, EnemyAct9, DamageHit, DrawImage);
}

DelAtt EnemyAct9(ObjData *self)
{
    if (manage->loop > 2)
    {
        if (self->shot_time >= SHOT_TIMING)
        {
            shot_to_angle(self->x, self->y, 50, 25);
            shot_to_angle(self->x, self->y, 70, 25);
            shot_to_angle(self->x, self->y, 90, 25);
            shot_to_angle(self->x, self->y, 110, 25);
            shot_to_angle(self->x, self->y, 130, 25);
            self->shot_time = self->start_time;
        }
        else
            self->shot_time++;
    }

    if (self->inerty<0 && self->inerty%2)
        shot_to_point(self->x,self->y,manage->player[0]->data.x,manage->player[0]->data.y,rand()%3+3);

    self->inerty--;

    self->x += self->inertx;
    self->y += self->inerty;

    if (self->inerty > 10)
        self->image = 0;
    else if (self->inerty > 5)
        self->image = 1;
    else if (self->inerty > 0)
        self->image = 2;
    else if (self->inerty > -5)
        self->image = 3;
    else if (self->inerty > -10)
        self->image = 4;
    else
        self->image = 5;

    if ((self->x < 0 - self->width/2) || (self->x > fieldw + self->width/2))
        return NullDel;
    if ((self->y < 0 - self->height/2) || (self->y > fieldh + self->height/2))
        return NullDel;

    return NoneDel;
}

int NewEnemy10(int x, int y)
{
    manage->new_obj.data.hit_att = MITEM;
    manage->new_obj.data.hit_mask = MPLAYER;

    manage->new_obj.data.x = x;
    manage->new_obj.data.y = y;

    manage->new_obj.data.hp = 0;
    manage->new_obj.data.point = 0;
    manage->new_obj.data.enemy_att = NoneDel;

    manage->new_obj.data.width = 32;
    manage->new_obj.data.height = 32;

    manage->new_obj.data.inerty = -10 - rand() % 10;

    /* item counter */
    manage->new_obj.data.cnt[0] = 0;
    /* time counter */
    manage->new_obj.data.cnt[1] = 0;
    /* max speed */
    manage->new_obj.data.cnt[2] = rand()%6 + 1;
    if (rand()%100 < 30)
    {
        manage->new_obj.data.cnt[3] = 1;
        manage->new_obj.data.cnt[0] = 1;
    }
    else
        manage->new_obj.data.cnt[3] = 0;

    manage->new_obj.grp.image = &item_bitmap;
    manage->new_obj.grp.sprites = 4;
    
    return new_obj(MENEMY, EnemyAct10, NullDelHit, DrawImage);
}

DelAtt EnemyAct10(ObjData *self)
{
    self->cnt[1]++;

    if (self->inerty < self->cnt[2])
        self->inerty++;
    self->y += self->inerty;

    if (self->cnt[3] == 1)
    {
        if (self->cnt[1]%25 == 0)
        {
            if (self->cnt[0] < 3)
                self->cnt[0]++;
            else
                self->cnt[0] = 1;
        }
    }

    self->image = self->cnt[0];

    if ((self->x < 0 - self->width/2) || (self->x > fieldw + self->width/2))
        return NullDel;
    if (self->y > fieldh + self->height/2)
        return NullDel;

    return NoneDel;
}



