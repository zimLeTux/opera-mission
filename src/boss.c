#include "extern.h"
#include "manage.h"
#include "boss.h"
#include "graphics.h"
#include "callback.h"
#include "game.h"
#include "enemy_shot.h"


/* local functions for the last boss */
static DelAtt BossAct8_dead(ObjData *self);
static DelAtt BossAct8_rand_shot(ObjData *self);
static DelAtt BossAct8_ring_shot(ObjData *self);
static DelAtt BossAct8_bound_shot(ObjData *self);
static DelAtt BossAct8_homing_shot(ObjData *self);
static DelAtt BossAct8_charge_shot(ObjData *self);
static DelAtt BossAct8_n_way_shot(ObjData *self);
static void BossAct8_next(ObjData *self, int span);

int NewBoss1(void)
{
    manage->new_obj.data.hit_att = MENEMY;
    manage->new_obj.data.hit_mask = MPLAYER | MPSHOT;

    manage->new_obj.data.x = fieldw/2;
    manage->new_obj.data.y = -90;

    manage->new_obj.data.hp = 120;
    manage->new_obj.data.point = 5000;
    manage->new_obj.data.enemy_att = BossDel;

    manage->new_obj.data.width = 150;
    manage->new_obj.data.height = 180;

    manage->new_obj.data.cnt[0] = 0;
    manage->new_obj.data.cnt[1] = 200; /* immutable time */
    manage->new_obj.data.cnt[2] = 0; /* image counter*/
    manage->new_obj.data.cnt[3] = 10; /* x move */
    manage->new_obj.data.cnt[4] = 0; /* y move */
    manage->new_obj.data.cnt[5] = 0;  /* laser counter */
    manage->new_obj.data.cnt[6] = 0;  /* normal shot counter */

    manage->new_obj.grp.image = &boss1_bitmap;
    manage->new_obj.grp.sprites = 1;

    return new_obj(MENEMY, BossAct1, BossHit1, DrawImage);
}

DelAtt BossAct1(ObjData *self)
{
    int i;

    if (self->cnt[0] < self->cnt[1])
    {   /* appears slowly */
        self->y++;
        self->cnt[0]++;
        return NoneDel;
    }

    if (manage->boss_time <= 0)
    {
        if (self->y > 0 - self->height/2 - 100)
            (self->y) -= 4;
        if (self->y < 0 - self->height/2 - 50)
        {
            if ((player->ships <= 0) && (manage->player[0]->data.cnt[3] != 0))
                return NoneDel;
            else
                return BossDel;
        }
        else
            return NoneDel;
    }

    (manage->boss_time)--;

    if (self->cnt[6] >= (100- (manage->loop)*10) )
    {
        shot_to_angle(self->x, self->y, 50, 10);
        shot_to_angle(self->x, self->y, 70, 10);
        shot_to_angle(self->x, self->y, 90, 10);
        shot_to_angle(self->x, self->y, 110, 10);
        shot_to_angle(self->x, self->y, 130, 10);
        self->cnt[6] =0;
    }
    else
        (self->cnt[6])++;

    if (self->cnt[5] >= (25-manage->loop*3))
    {
        for (i = 1; i< manage->loop; i++)
            shot_to_angle(self->x,self->y,rand()%180,10);
        if (manage->loop >1)
            shot_to_point(self->x,self->y,manage->player[0]->data.x,manage->player[0]->data.y,10);
        laser_shot(self->x-30,self->y,20);
        laser_shot(self->x+30,self->y,20);
        laser_shot(self->x-80,self->y,20);
        laser_shot(self->x+80,self->y,20);

        self->cnt[5] = 0;
    }
    else
        self->cnt[5]++;

    if ((self->x+self->cnt[3]>fieldw) || (self->x+self->cnt[3]<0))
        self->cnt[3] = self->cnt[3]*(-1);

    self->x += self->cnt[3];

    return NoneDel;
}

DelAtt BossHit1(ObjData *self, ObjData *your)
{
    int temp = your->attack;

    if ((self->cnt[0] < self->cnt[1]) || (manage->boss_time <=0))
    {
        return NoneDel;
    }

    if (self->hp < temp)
    {
        temp = self->hp;
    }

    self->hp -= temp;
    player->rec[0].score += temp;

    if (self->hp <= 0)
    {
        player->rec[0].score -= 1;
        new_bomb(self->x,self->y);
        new_bomb(self->x+rand()%100-50,self->y+rand()%100-50);
        new_bomb(self->x+rand()%100-50,self->y+rand()%100-50);
        new_bomb(self->x+rand()%100-50,self->y+rand()%100-50);
        new_bomb(self->x+rand()%100-50,self->y+rand()%100-50);
        new_bomb(self->x+rand()%100-50,self->y+rand()%100-50);
        new_bomb(self->x+rand()%100-50,self->y+rand()%100-50);

        self->show_damage_time = 0;
        return self->enemy_att;
    }
    else
    {
        self->show_damage_time = 15;
        return NoneDel;
    }

}


int NewBoss2(void)
{
    manage->new_obj.data.hit_att = MENEMY;
    manage->new_obj.data.hit_mask = MPLAYER | MPSHOT;

    manage->new_obj.data.x = fieldw/2;
    manage->new_obj.data.y = -60;

    manage->new_obj.data.hp = 200;
    manage->new_obj.data.point = 5000;
    manage->new_obj.data.enemy_att = BossDel;

    manage->new_obj.data.width = 90;
    manage->new_obj.data.height = 130;

    manage->new_obj.data.cnt[0] = 0;
    manage->new_obj.data.cnt[1] = 200; /* immutable time */
    manage->new_obj.data.cnt[2] = 0; /* image counter */

    manage->new_obj.data.cnt[3] = 0;
    manage->new_obj.data.cnt[4] = 0;
    manage->new_obj.data.cnt[5] = 0; /* normal shot counter */

    manage->new_obj.grp.image = &boss2_bitmap;
    manage->new_obj.grp.sprites = 1;

    return new_obj(MENEMY, BossAct2, BossHit1, DrawImage);
}

DelAtt BossAct2(ObjData *self)
{
    if (self->cnt[0] < self->cnt[1])
    {   /* appears slowly */
        self->y++;
        self->cnt[0]++;
        return NoneDel;
    }

    if (manage->boss_time <= 0)
    {
        if (self->y > 0 - self->height/2 - 100)
            (self->y) -= 4;
        if (self->y < 0 - self->height/2 - 50)
        {
            if ((player->ships <= 0) && (manage->player[0]->data.cnt[3] != 0))
                return NoneDel;
            else
                return BossDel;
        }
        else
            return NoneDel;
    }

    (manage->boss_time)--;

    if (manage->loop > 2)
    {
        if (self->cnt[5] >= 90- (manage->loop)*10)
        {
            shot_to_point(self->x+40,self->y,manage->player[0]->data.x,manage->player[0]->data.y,rand()%3 + 5);
            shot_to_point(self->x   ,self->y,manage->player[0]->data.x,manage->player[0]->data.y,8);
            shot_to_point(self->x-40,self->y,manage->player[0]->data.x,manage->player[0]->data.y,rand()%5 + 3);
            self->cnt[5] = 0;
        }
        else
            (self->cnt[5])++;
    }

    if (self->cnt[3] >= (15-manage->loop*3))
    {
        if (manage->loop > 1)
        {
            if (manage->player[0]->data.y < self->y)
                shot_to_angle(self->x, self->y, 270, 10);
            else
                shot_to_angle(self->x, self->y, 90, 10);
        }

        homing_shot(self->x-20,self->y-10,-10,-10);
        homing_shot(self->x+20,self->y-10, 10,-10);
        self->cnt[3] = 0;
    }
    else
        self->cnt[3]++;

    if (self->x > manage->player[0]->data.x)
    {
        if (self->x - manage->player[0]->data.x > 20)
        {
            self->x -= 10;
        }
        else
        {
            self->cnt[4]++;
            self->x --;
        }

        if (self->cnt[4] > 10)
        {
            laser_shot(self->x-40,self->y,30);
            laser_shot(self->x+40,self->y,30);
            self->cnt[4] = 0;
        }
    }
    else
    {
        if (manage->player[0]->data.x - self->x > 20)
        {
            self->x += 10;
        }
        else
        {
            self->cnt[4]++;
            self->x ++;
        }

        if (self->cnt[4] > 10)
        {
            laser_shot(self->x-40,self->y,30);
            laser_shot(self->x+40,self->y,30);
            self->cnt[4] = 0;
        }
    }

    return NoneDel;
}

int NewBoss3(void)
{
    manage->new_obj.data.hit_att = MENEMY;
    manage->new_obj.data.hit_mask = MPLAYER | MPSHOT;

    manage->new_obj.data.x = fieldw/2;
    manage->new_obj.data.y = -80;

    manage->new_obj.data.hp = 330;
    manage->new_obj.data.point = 6000;
    manage->new_obj.data.enemy_att = BossDel;

    manage->new_obj.data.width = 130;
    manage->new_obj.data.height = 130;

    manage->new_obj.data.cnt[0] = 0;
    manage->new_obj.data.cnt[1] = 200; /* immutable time */
    manage->new_obj.data.cnt[2] = 0; /* image counter */
    manage->new_obj.data.cnt[3] = 10; /* x move */
    manage->new_obj.data.cnt[4] = 0; /* y move */
    manage->new_obj.data.cnt[5] = 0;  /* laser counter */
    manage->new_obj.data.cnt[6] = 0; /* missile counter */

    manage->new_obj.grp.image = &boss3_bitmap;
    manage->new_obj.grp.sprites = 1;

    return new_obj(MENEMY, BossAct3, BossHit1, DrawImage);
}

DelAtt BossAct3(ObjData *self)
{
    if (self->cnt[0] < self->cnt[1])
    {   /* appears slowly */
        self->y++;
        self->cnt[0]++;
        return NoneDel;
    }

    if (manage->boss_time <= 0)
    {
        if (self->y > 0 - self->height/2 - 100)
            (self->y) -= 4;
        if (self->y < 0 - self->height/2 - 50)
        {
            if ((player->ships <= 0) && (manage->player[0]->data.cnt[3] != 0))
                return NoneDel;
            else
                return BossDel;
        }
        else
            return NoneDel;
    }

    (manage->boss_time)--;

    if (self->cnt[6] >= (80 - manage->loop*10))
    {
        homing_shot(self->x,self->y,0,10);
        homing_shot(self->x,self->y,10,0);
        homing_shot(self->x,self->y,0,-10);
        homing_shot(self->x,self->y,-10,0);
        homing_shot(self->x,self->y,10,10);
        homing_shot(self->x,self->y,-10,10);
        homing_shot(self->x,self->y,10,-10);
        homing_shot(self->x,self->y,-10,-10);

        self->cnt[6] = 0;
    }
    else
        self->cnt[6]++;

    if (self->cnt[5] >= (25 - manage->loop*2))
    {
        if (manage->loop >1)
            laser_shot(self->x- 120+ (rand()%5)*60,self->y,25);
        if (manage->loop >2)
            shot_to_point(self->x,self->y,manage->player[0]->data.x,manage->player[0]->data.y,18);
        laser_shot(self->x-30,self->y,25);
        laser_shot(self->x+30,self->y,25);
        laser_shot(self->x-90,self->y,25);
        laser_shot(self->x+90,self->y,25);
        laser_shot(self->x-150,self->y,25);
        laser_shot(self->x+150,self->y,25);

        self->cnt[5] = 0;
    }
    else
        self->cnt[5]++;

    if ((self->x+self->cnt[3]>fieldw) || (self->x+self->cnt[3]<0))
        self->cnt[3] = self->cnt[3]*(-1);

    self->x += self->cnt[3];

    return NoneDel;
}

int NewBoss4(void)
{
    manage->new_obj.data.hit_att = MENEMY;
    manage->new_obj.data.hit_mask = MPLAYER | MPSHOT;

    manage->new_obj.data.x = fieldw/2;
    manage->new_obj.data.y = -90;

    manage->new_obj.data.inertx = 0;
    manage->new_obj.data.inerty = 0;

    manage->new_obj.data.hp = 550;
    manage->new_obj.data.point = 6000;
    manage->new_obj.data.enemy_att = BossDel;

    manage->new_obj.data.width = 300;
    manage->new_obj.data.height = 170;

    manage->new_obj.data.cnt[0] = 0;
    manage->new_obj.data.cnt[1] = 195; /* immutable time */
    manage->new_obj.data.cnt[2] = 0; /* image counter */
    manage->new_obj.data.cnt[3] = 1; /* x move */
    manage->new_obj.data.cnt[4] = 0; /* y move */
    manage->new_obj.data.cnt[5] = 0;  /* laser counter */

    manage->new_obj.grp.image = &boss4_bitmap;
    manage->new_obj.grp.sprites = 1;

    return new_obj(MENEMY,BossAct4,BossHit1,DrawImage);
}

DelAtt BossAct4(ObjData *self)
{
    if (self->cnt[0] < self->cnt[1])
    {   /* appears slowly */
        self->y++;
        self->cnt[0]++;
        return NoneDel;
    }

    if (manage->boss_time <= 0)
    {
        if (self->y > 0 - self->height/2 - 100)
            (self->y) -= 4;
        if (self->y < 0 - self->height/2 - 50)
        {
            if ((player->ships <= 0) && (manage->player[0]->data.cnt[3] != 0))
                return NoneDel;
            else
                return BossDel;
        }
        else
            return NoneDel;
    }

    (manage->boss_time)--;

    if (self->cnt[5] >= (8-manage->loop))
    {
        laser_shot(self->x + rand()%600-300, self->y, 11 + rand()%3  + manage->loop*7);
        laser_shot(self->x + rand()%600-300, self->y, 6 + rand()%3 + manage->loop*7);
        laser_shot(self->x + rand()%600-300, self->y, 1 + rand()%3 + manage->loop*7);
        laser_shot(self->x + rand()%600-300, self->y, -4 + rand()%3 + manage->loop*7);
        if (manage->loop >2)
            laser_shot(self->x + rand()%600-300, self->y, 6 + rand()%3 + manage->loop*7);
        if (manage->loop >1)
            laser_shot(self->x + rand()%600-300, self->y, 1 + rand()%3 + manage->loop*7);
        self->cnt[5] = 0;
    }
    else
        self->cnt[5]++;

    if (self->x < manage->player[0]->data.x)
    {
        if (self->inertx <= 8)
            self->inertx++;
    }
    if (self->x > manage->player[0]->data.x)
    {
        if (self->inertx >= -8)
            self->inertx--;
    }

    self->x += self->inertx;

    return NoneDel;
}

int NewBoss5(void)
{
    manage->new_obj.data.hit_att = MENEMY;
    manage->new_obj.data.hit_mask = MPLAYER | MPSHOT;

    manage->new_obj.data.x = fieldw/2;
    manage->new_obj.data.y = -128;

    manage->new_obj.data.hp = 350;
    manage->new_obj.data.point = 7000;
    manage->new_obj.data.enemy_att = BossDel;

    manage->new_obj.data.width = 120;
    manage->new_obj.data.height = 120;

    manage->new_obj.data.cnt[0] = 0;
    manage->new_obj.data.cnt[1] = 200; /* immutable time */
    manage->new_obj.data.cnt[2] = 0; /* image counter */
    manage->new_obj.data.cnt[3] = manage->player[0]->data.x; /* x move */
    manage->new_obj.data.cnt[4] = manage->player[0]->data.y; /* y move */
    manage->new_obj.data.cnt[5] = 0;  /* shot counter */
    manage->new_obj.data.cnt[6] = 0;  /* angle counter */

    manage->new_obj.grp.image = &boss5_bitmap;
    manage->new_obj.grp.sprites = 1;

    return new_obj(MENEMY,BossAct5,BossHit1,DrawImage);
}

DelAtt BossAct5(ObjData *self)
{
    int i;

    if (self->cnt[0] < self->cnt[1])
    {   /* appears slowly */
        self->y++;
        self->cnt[0]++;
        return NoneDel;
    }

    if (manage->boss_time <= 0)
    {
        if (self->y > 0 - self->height/2 - 100)
            (self->y) -= 4;
        if (self->y < 0 - self->height/2 - 50)
        {
            if ((player->ships <= 0) && (manage->player[0]->data.cnt[3] != 0))
                return NoneDel;
            else
                return BossDel;
        }
        else
            return NoneDel;
    }

    (manage->boss_time)--;

    if (self->cnt[5] > 120)
    {
        self->cnt[3] = manage->player[0]->data.x;
        self->cnt[4] = manage->player[0]->data.y;
        self->cnt[5] = 0;
    }
    else if (abs(self->x-self->cnt[3]) > 5 || abs(self->y-self->cnt[4]) > 5)
    {
        if (self->x < self->cnt[3])
        {
            if (self->cnt[3]-self->x > 50)
                self->x += 20;
            else if (self->cnt[3]-self->x > 30)
                self->x += 10;
            else if (self->cnt[3]-self->x > 20)
                self->x += 5;
            else
                self->x ++;
        }
        else
        {
            if (self->x-self->cnt[3] > 50)
                self->x -= 20;
            else if (self->x-self->cnt[3] > 30)
                self->x -= 10;
            else if (self->x-self->cnt[3] > 20)
                self->x -= 5;
            else
                self->x --;
        }

        if (self->y < self->cnt[4])
        {
            if (self->cnt[4]-self->y > 50)
                self->y += 20;
            else if (self->cnt[4]-self->y > 30)
                self->y += 10;
            else if (self->cnt[4]-self->y > 20)
                self->y += 5;
            else
                self->y ++;
        }
        else
        {
            if (self->y-self->cnt[4] > 50)
                self->y -= 20;
            else if (self->y-self->cnt[4] > 30)
                self->y -= 10;
            else if (self->y-self->cnt[4] > 20)
                self->y -= 5;
            else
                self->y --;
        }
    }
    else
    {
        if (manage->loop >1)
        {
            if (self->cnt[5] % 2 == 0)
            {
                for (i =1; i <manage->loop; i++)
                    shot_to_angle(self->x, self->y, rand()%360, 3);
            }
        }

        self->cnt[6] += 13;
        if (self->cnt[6] >= 360)
            self->cnt[6] -= 360;
        shot_to_angle(self->x,self->y,self->cnt[6],4 + manage->loop * 4);

        self->cnt[5]++;
    }

    return NoneDel;
}

int NewBoss6(void)
{
    manage->new_obj.data.hit_att = MENEMY;
    manage->new_obj.data.hit_mask = MPLAYER | MPSHOT;

    manage->new_obj.data.x = fieldw/2;
    manage->new_obj.data.y = -64;

    manage->new_obj.data.hp = 400;
    manage->new_obj.data.point = 9000;
    manage->new_obj.data.enemy_att = BossDel;

    manage->new_obj.data.width = 300;
    manage->new_obj.data.height = 100;

    manage->new_obj.data.cnt[0] = 0;
    manage->new_obj.data.cnt[1] = 200; /* immutable time */
    manage->new_obj.data.cnt[2] = 0; /* image counter */

    manage->new_obj.data.cnt[3] = 0; /* stealth */
    manage->new_obj.data.cnt[4] = 0;  /* shot counter */

    manage->new_obj.grp.image = &boss6_bitmap;
    manage->new_obj.grp.sprites = 1;

    return new_obj(MENEMY, BossAct6, BossHit1, DrawImage);
}

DelAtt BossAct6(ObjData *self)
{
    if (self->cnt[0] < self->cnt[1])
    {   /* appears slowly */
        self->y++;
        self->cnt[0]++;
        return NoneDel;
    }

    if (manage->boss_time <= 0)
    {
        if (self->cnt[3] == 50)
        {
            if ((player->ships <= 0) && (manage->player[0]->data.cnt[3] != 0))
                return NoneDel;
            else
                return BossDel;
        }
    }

    if (manage->boss_time >= 1)
        (manage->boss_time)--;

    if (self->cnt[3] == 0)
    {
        self->hit_att = MENEMY;
        self->hit_mask = MPLAYER | MPSHOT;
        self->image = 0;
        switch (self->cnt[4]%10)
        {
        case 0:
            if (manage->loop > 2)
            {
                ring_to_point(self->x-128,self->y+32,manage->player[0]->data.x,manage->player[0]->data.y,rand()%5+3);
                ring_to_point(self->x-128,self->y+32,manage->player[0]->data.x,manage->player[0]->data.y,rand()%8+3);
                ring_to_point(self->x-128,self->y+32,manage->player[0]->data.x,manage->player[0]->data.y,rand()%3+5);
            }
            ring_to_angle(self->x-128,self->y+32,80,8);
            ring_to_angle(self->x-128,self->y+32,90,8);
            ring_to_angle(self->x-128,self->y+32,100,8);
            break;
        case 2:
            ring_to_angle(self->x,self->y+32,80,8);
            ring_to_angle(self->x,self->y+32,90,8);
            ring_to_angle(self->x,self->y+32,100,8);
            break;
        case 4:
            if (manage->loop > 2)
            {
                ring_to_point(self->x+128,self->y+32,manage->player[0]->data.x,manage->player[0]->data.y,rand()%5+3);
                ring_to_point(self->x+128,self->y+32,manage->player[0]->data.x,manage->player[0]->data.y,rand()%8+3);
                ring_to_point(self->x+128,self->y+32,manage->player[0]->data.x,manage->player[0]->data.y,rand()%3+5);
            }
            ring_to_angle(self->x+128,self->y+32,80,8);
            ring_to_angle(self->x+128,self->y+32,90,8);
            ring_to_angle(self->x+128,self->y+32,100,8);
            break;
        case 6:
            homing_shot(self->x-90,self->y-20,-15,-10);
            homing_shot(self->x-60,self->y-20,0,-10);
            homing_shot(self->x+60,self->y-20,0,-10);
            homing_shot(self->x+90,self->y-20,15,-10);
            break;
        case 8:
            laser_shot(self->x-120 +rand()%240,self->y,20);
            laser_shot(self->x-120,self->y,20);
            laser_shot(self->x+120,self->y,20);
            if (manage->loop == 1)
                laser_shot(self->x, self->y, 20);
            else if (manage->loop == 2)
            {
                laser_shot(self->x-30,self->y,20);
                laser_shot(self->x+30,self->y,20);
            }
            else
            {
                laser_shot(self->x -60, self->y, 20);
                laser_shot(self->x, self->y, 20);
                laser_shot(self->x +60,self->y,20);
            }
            break;
        }
        if (manage->loop >2)
            if (self->cnt[4] %2 == 0)
                if ((manage->player[0]->data.x < self->x -120)
                        ||(manage->player[0]->data.x > self->x +120)
                        ||(manage->player[0]->data.y < self->y))
                    shot_to_point(self->x,self->y,manage->player[0]->data.x,manage->player[0]->data.y, 18);
        self->cnt[4]++;
    }
    else
    {
        self->cnt[3]--;
        if (self->cnt[3] < 20)
        {
            if (self->cnt[3]%2 == 0)
                self->image = 0;
            else
                self->image = 1;
        }
    }

    if (self->cnt[4] == 45)
    {
        shot_to_angle(self->x -120, self->y, 105, 25);
        shot_to_angle(self->x -120, self->y, 115, 25);
        shot_to_angle(self->x -120, self->y, 125, 25);
        shot_to_angle(self->x -120, self->y, 135, 25);
        shot_to_angle(self->x -120, self->y, 145, 25);
        shot_to_angle(self->x +120, self->y, 75, 25);
        shot_to_angle(self->x +120, self->y, 65, 25);
        shot_to_angle(self->x +120, self->y, 55, 25);
        shot_to_angle(self->x +120, self->y, 45, 25);
        shot_to_angle(self->x +120, self->y, 35, 25);
        if (manage->loop >1)
        {
            shot_to_angle(self->x -120, self->y, 155, 25);
            shot_to_angle(self->x -120, self->y, 165, 25);
            shot_to_angle(self->x -120, self->y, 175, 25);
            shot_to_angle(self->x +120, self->y, 25, 25);
            shot_to_angle(self->x +120, self->y, 15, 25);
            shot_to_angle(self->x +120, self->y, 5, 25);
        }
    }

    if (self->cnt[4] >= 50)
    {
        self->cnt[3] = 50;
        self->cnt[4] = 0;
        self->hit_att = 0;
        self->hit_mask = 0;
        self->image = 1;
        self->show_damage_time = 0;

        shot_to_angle(self->x -120, self->y, 100, 25);
        shot_to_angle(self->x -120, self->y, 110, 25);
        shot_to_angle(self->x -120, self->y, 120, 25);
        shot_to_angle(self->x -120, self->y, 130, 25);
        shot_to_angle(self->x -120, self->y, 140, 25);
        shot_to_angle(self->x +120, self->y, 80, 25);
        shot_to_angle(self->x +120, self->y, 70, 25);
        shot_to_angle(self->x +120, self->y, 60, 25);
        shot_to_angle(self->x +120, self->y, 50, 25);
        shot_to_angle(self->x +120, self->y, 40, 25);
        if (manage->loop >1)
        {
            shot_to_angle(self->x -120, self->y, 150, 25);
            shot_to_angle(self->x -120, self->y, 160, 25);
            shot_to_angle(self->x -120, self->y, 170, 25);
            shot_to_angle(self->x +120, self->y, 30, 25);
            shot_to_angle(self->x +120, self->y, 20, 25);
            shot_to_angle(self->x +120, self->y, 10, 25);
        }

        self->x = rand()%fieldw;
        self->y = rand()%((fieldh -(fieldh%2))/2);
    }

    return NoneDel;
}

int NewBoss7(void)
{
    manage->new_obj.data.hit_att = MENEMY;
    manage->new_obj.data.hit_mask = MPLAYER | MPSHOT;

    manage->new_obj.data.hp = 150;
    manage->new_obj.data.point = 8000;
    manage->new_obj.data.enemy_att = BossDel;

    manage->new_obj.data.width = 32;
    manage->new_obj.data.height = 32;

    manage->new_obj.data.x = fieldw/2;
    manage->new_obj.data.y = 1 - manage->new_obj.data.height/2;

    manage->new_obj.data.cnt[0] = 0;
    manage->new_obj.data.cnt[1] = 0; /* appears slowly */
    manage->new_obj.data.cnt[2] = 0; /* image counter */

    do {
        manage->new_obj.data.cnt[3] = rand()%30 - 15; /* x move */
    } while (abs(manage->new_obj.data.cnt[3]) < 5);
    do {
        manage->new_obj.data.cnt[4] = rand()% 10 + 5; /* y move */
    } while (abs(manage->new_obj.data.cnt[4]) < 5);

    manage->new_obj.data.cnt[5] = 0;  /* shot counter */

    /* "appeared" counter */
    manage->new_obj.data.cnt[6] = 0;

    manage->new_obj.grp.image = &ebound_bitmap;
    manage->new_obj.grp.sprites = 8;

    return new_obj(MENEMY, BossAct7, BossHit1, DrawImage);
}

DelAtt BossAct7(ObjData *self)
{
    int i;
    if (manage->boss_time >= 1)
        (manage->boss_time)--;

    self->image++;
    if (self->image >= 8)
        self->image = 0;

    if (manage->boss_time >= 1)
    {
        if (manage->loop >1)
        {
            if (self->cnt[5] % 20 == 0)
            {
                for (i = 0; i<360; i+= (90 - (90 % manage->loop))/ manage->loop)
                    shot_to_angle(self->x, self->y, i, manage->loop * 2);
            }
        }

        if (self->cnt[5] % 30 == 0)
        {
            bound_shot(self->x,self->y,rand()%30-15,rand()%30-15, 5);
        }
    }


    (self->cnt[5])++;

    if (self->cnt[5] >= 60)
        self->cnt[5] = 0;

    if ((self->cnt[6] != 0) && (manage->boss_time >= 1))
    {
        if ((self->x+self->cnt[3]>fieldw) || (self->x+self->cnt[3]<0))
            self->cnt[3] = self->cnt[3]*(-1);
        if ((self->y+self->cnt[4]>fieldh) || (self->y+self->cnt[4]<0))
            self->cnt[4] = self->cnt[4]*(-1);
    }

    if ((self->x < 0 - self->width/2) || (self->x > fieldw + self->width/2))
    {
        if ((player->ships <= 0) && (manage->player[0]->data.cnt[3] != 0))
            return NoneDel;
        else
            return BossDel;
    }
    if ((self->y < 0 - self->height/2) || (self->y > fieldh + self->height/2))
    {
        if ((player->ships <= 0) && (manage->player[0]->data.cnt[3] != 0))
            return NoneDel;
        else
            return BossDel;
    }

    self->x += self->cnt[3];
    self->y += self->cnt[4];

    if ((self->cnt[6] == 0) && (self->x >= 0) && (self->x <= fieldw)
            && (self->y >= 0) && (self->y < fieldh))
        self->cnt[6] = 1;

    return NoneDel;
}

int NewBoss8(void)
{
    manage->new_obj.data.hit_att = MENEMY;
    manage->new_obj.data.hit_mask = MPLAYER | MPSHOT;

    manage->new_obj.data.x = fieldw/2;
    manage->new_obj.data.y = -80;

    manage->new_obj.data.hp = 600;
    manage->new_obj.data.point = 10000;
    manage->new_obj.data.enemy_att = BossDel;

    manage->new_obj.data.width = 200;
    manage->new_obj.data.height = 180;

    manage->new_obj.data.cnt[0] = 0;
    manage->new_obj.data.cnt[1] = 200; /* appears slowly */
    manage->new_obj.data.cnt[2] = 0; /* image counter */
    manage->new_obj.data.cnt[3] = 10; /* x move */
    manage->new_obj.data.cnt[4] = 0; /* y move */
    manage->new_obj.data.cnt[5] = 0;  /* shot counter */
    manage->new_obj.data.cnt[6] = rand()%4; /* mode */
    manage->new_obj.data.cnt[7] = 0; /* mode counter */
    manage->new_obj.data.cnt[8] = 0; /* after-death counter */

    manage->new_obj.grp.image = &boss7_bitmap;
    manage->new_obj.grp.sprites = 1;

    return new_obj(MENEMY, BossAct8, BossHit8, DrawImage);
}

DelAtt BossAct8(ObjData *self)
{
    if (self->cnt[0] < self->cnt[1])
    {   /* appears slowly */
        self->y++;
        self->cnt[0]++;
        return NoneDel;
    }

    if (self->kill == true)
    {
        /* the boss is dead */
        return BossAct8_dead(self);
    }

    /* the boss is alive */
    if (manage->boss_time <= 0)
    {
        if (self->y < 0 - self->height/2 - 50)
        {
            if ((player->ships > 0) ||(manage->player[0]->data.cnt[3] == 0))
                return BossDel;
            else
                return NoneDel;
        }
        else
        {
            (self->y) -= 4;
            return NoneDel;
        }
    }

    (manage->boss_time)--;

    if (self->cnt[5] > 8)
        self->cnt[5] = 0;
    else
        self->cnt[5]++;

    if ((self->cnt[5]%2==0) && ((self->cnt[6] ==0) || (self->cnt[6] == 2)
                                || (self->cnt[6] == 3)))
    {
        ring_to_angle(self->x, self->y-60, rand()%50+70, rand()%15+5);
        ring_to_angle(self->x, self->y-60, rand()%20+80, rand()%10+10);
        /*
        if (manage->Loop > 2)
            RingToAngle(self->X, self->Y-60, rand()%20+80, rand()%5+15);
               */
    }

    if (self->cnt[6] != 4)
    {
        if (self->x>manage->player[0]->data.x && self->x>self->harfw)
            self->x--;
        else if (self->x<manage->player[0]->data.x && self->x<(fieldw-self->harfw))
            self->x++;
    }

    if (self->cnt[6] == 0)
    {
        return BossAct8_rand_shot(self);
    }
    else if (self->cnt[6] == 1)
    {
        return BossAct8_ring_shot(self);
    }
    else if (self->cnt[6] == 2)
    {
        return BossAct8_bound_shot(self);
    }
    else if (self->cnt[6] == 3)
    {
        return BossAct8_homing_shot(self);
    }
    else if (self->cnt[6] == 4)
    {
        return BossAct8_charge_shot(self);
    }
    else if (self->cnt[6] == 5)
    {
        return BossAct8_n_way_shot(self);
    }

    return NoneDel;
}

DelAtt BossHit8(ObjData *self, ObjData *your)
{
    int temp = your->attack;
    if (self->cnt[0] < self->cnt[1])
        return NoneDel;

    if (self->hp < your->attack)
        temp = self->hp;

    self->hp -= temp;
    player->rec[0].score += temp;

    if (self->hp <= 0)
    {
        player->rec[0].score -= 1;
        self->show_damage_time = 0;

        self->kill = true;
    }
    else
    {
        self->show_damage_time = 15;
    }

    return NoneDel;
}

static DelAtt BossAct8_dead(ObjData *self)
{
    int i;
    int j;
    /* the boss is dead */
    new_large_bomb(self->x+rand()%230-115,self->y+rand()%180-90);
    new_large_bomb(self->x+rand()%230-115,self->y+rand()%180-90);

    self->cnt[8]++;
    if ((player->ships <= 0) && (manage->player[0]->data.cnt[3] != 0))
        if (self->cnt[8] >= 300)
            self->cnt[8] = 0;

    if (self->cnt[8] %3 == 0)
    {
        if (self->x > manage->player[0]->data.x)
            (self->x)--;
        else if (self->x < manage->player[0]->data.x)
            (self->x)++;
        if (self->y > manage->player[0]->data.y)
            (self->y)--;
        else if (self->y < manage->player[0]->data.y)
            (self->y)++;
    }

    if (self->cnt[8] %30 == 0)
    {
        for (i=0; i<24; i++)
            shot_to_angle(self->x,self->y,i*15, 7);
    }
    else if (self->cnt[8] %30 == 15)
    {
        for (i=0; i<24; i++)
            shot_to_angle(self->x,self->y,i*15 + 7,7);
    }
    if (self->cnt[8] %13 == 0)
    {
        shot_to_point(self->x,self->y,manage->player[0]->data.x,manage->player[0]->data.y, manage->loop *5);
        if (manage->loop >2)
        {
            bound_shot(self->x, self->y-60,rand()%20-10,rand()%4+10,4);
        }
    }
    if (self->cnt[8] %5 == 0)
    {
        if (manage->loop >1)
        {
            for (i=1; i< manage->loop * 3; i++)
                shot_to_angle(self->x,self->y,rand()%360, manage->loop *3);
        }
    }

    if (self->cnt[8] >= 300)
    {
        for (i=0; i<=10; i++)
            for (j=0; j<=10; j++)
                new_large_bomb(50*i,65*j);
        return self->enemy_att;
    }
    else
        return NoneDel;
}

static DelAtt BossAct8_rand_shot(ObjData *self)
{
    int i;
    if (self->cnt[7]% 10 == 0)
    {
        for (i = 1; i <= manage->loop; i++)
        {
            shot_to_angle(self->x-60, self->y-60, rand()%50 + 70, rand()%5+5);
            shot_to_angle(self->x+60, self->y-60, rand()%50 + 70, rand()%5+5);
        }
        shot_to_point(self->x,self->y-60,manage->player[0]->data.x,manage->player[0]->data.y,rand()%5+3);
    }

    BossAct8_next(self, 100);
    return NoneDel;
}

static DelAtt BossAct8_ring_shot(ObjData *self)
{
    if (self->cnt[7]%2 == 0)
    {
        ring_to_point(self->x-60, self->y+10, manage->player[0]->data.x,manage->player[0]->data.y, rand()%10 + 20);
        ring_to_point(self->x+60, self->y+10, manage->player[0]->data.x,manage->player[0]->data.y, rand()%10 + 20);
        if (manage->loop >1)
        {
            ring_to_angle(self->x, self->y - 60, 90, rand()%10 + 20);
            ring_to_angle(self->x, self->y - 60, 90, rand()%10 + 20);
        }
        if (manage->loop >2)
        {
            ring_to_angle(self->x-60, self->y - 60, 90, rand()%10 + 20);
            ring_to_angle(self->x+60, self->y - 60, 90, rand()%10 + 20);
        }
    }

    BossAct8_next(self, 70);
    return NoneDel;
}

static DelAtt BossAct8_bound_shot(ObjData *self)
{
    if (manage->loop <=2)
    {
        if (self->cnt[7]  % 16 == 0)
            bound_shot(self->x+60,self->y-60,rand()%20-10,rand()%4+10,2);
        else if (self->cnt[7]  % 16 == 8)
            bound_shot(self->x-60,self->y-60,rand()%20-10,rand()%4+10,2);
    }
    else
    {
        if (self->cnt[7]  % 16 == 0)
            bound_shot(self->x+60,self->y-60, (rand()%20-10)*2, (rand()%4+10)*2, 2);
        else if (self->cnt[7]  % 16 == 8)
            bound_shot(self->x-60,self->y-60, (rand()%20-10)*2 ,(rand()%4+10)*2, 2);
    }

    BossAct8_next(self, 100);
    return NoneDel;
}

static DelAtt BossAct8_homing_shot(ObjData *self)
{
    if (self->cnt[7] %8 == 0)
    {
        homing_shot(self->x-60,self->y-10,-10, 5);
        homing_shot(self->x+60,self->y-10, 10, 5);
        if (manage->loop > 2)
        {
            homing_shot(self->x-60,self->y-10,-3, 10);
            homing_shot(self->x+60,self->y-10, 3, 10);
        }
    }

    BossAct8_next(self, 100);
    return NoneDel;
}

static DelAtt BossAct8_charge_shot(ObjData *self)
{
    int i;
    if (self->cnt[7] == 48 - manage->loop *8)
    {
        for (i=0; i<24; i++)
            shot_to_angle(self->x,self->y,i*15,15);
    }
    if (self->cnt[7] == 53 - manage->loop *8)
    {
        for (i=0; i<24; i++)
            shot_to_angle(self->x,self->y,i*15+7,20);
    }
    if (self->cnt[7] > 53 - manage->loop *8)
    {
        laser_shot(self->x-120,self->y-60,35);
        laser_shot(self->x-80 ,self->y-60,35);
        laser_shot(self->x-40 ,self->y-60,35);
        laser_shot(self->x    ,self->y-60,35);
        laser_shot(self->x+40 ,self->y-60,35);
        laser_shot(self->x+80 ,self->y-60,35);
        laser_shot(self->x+120,self->y-60,35);
    }

    BossAct8_next(self, 58 - manage->loop *8);
    return NoneDel;
}

static DelAtt BossAct8_n_way_shot(ObjData *self)
{
    int xtemp;
    int ytemp;
    int speedtemp;
    if (self->cnt[7] % 13 == 0)
    {
        xtemp = rand()%fieldw;
        ytemp = rand()%((fieldh -(fieldh%2))/2);
        speedtemp = rand()%7 + 5;

        if (manage->loop == 1)
        {
            shot_to_angle(xtemp, ytemp, 50, speedtemp);
            shot_to_angle(xtemp, ytemp, 70, speedtemp);
            shot_to_angle(xtemp, ytemp, 90, speedtemp);
            shot_to_angle(xtemp, ytemp, 110, speedtemp);
            shot_to_angle(xtemp, ytemp, 130, speedtemp);
        }
        else if (manage->loop == 2)
        {
            shot_to_angle(xtemp, ytemp, 51, speedtemp);
            shot_to_angle(xtemp, ytemp, 64, speedtemp);
            shot_to_angle(xtemp, ytemp, 77, speedtemp);
            shot_to_angle(xtemp, ytemp, 90, speedtemp);
            shot_to_angle(xtemp, ytemp, 103, speedtemp);
            shot_to_angle(xtemp, ytemp, 116, speedtemp);
            shot_to_angle(xtemp, ytemp, 129, speedtemp);
        }
        else
        {
            shot_to_angle(xtemp, ytemp, 50, speedtemp);
            shot_to_angle(xtemp, ytemp, 60, speedtemp);
            shot_to_angle(xtemp, ytemp, 70, speedtemp);
            shot_to_angle(xtemp, ytemp, 80, speedtemp);
            shot_to_angle(xtemp, ytemp, 90, speedtemp);
            shot_to_angle(xtemp, ytemp, 100, speedtemp);
            shot_to_angle(xtemp, ytemp, 110, speedtemp);
            shot_to_angle(xtemp, ytemp, 120, speedtemp);
            shot_to_angle(xtemp, ytemp, 130, speedtemp);
        }
    }

    BossAct8_next(self, 150);
    return NoneDel;
}

static void BossAct8_next(ObjData *self, int span)
{
    int modetemp = 0;
    if (self->cnt[7] > span)
    {
        do
        {
            modetemp = rand()%6;
        } while (modetemp == self->cnt[6]);
        self->cnt[6] = modetemp;
        self->cnt[7] = 0;
    }
    else
        self->cnt[7]++;
}





