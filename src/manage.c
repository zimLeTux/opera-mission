#include <stdio.h>
#include <stdlib.h>

#include <pwd.h>
#include <sys/types.h>

#include <allegro5/allegro.h>

#include "image.h"
#include "opera.h"
#include "manage.h"
#include "score.h"
#include "extern.h"
#include "callback.h"
#include "game.h"
#include "enemy_shot.h"
#include "constants.h"

CharManage* new_manage(int player_max, int enemy_max)
{
    CharManage* self;
    int i;

    self = (CharManage *)malloc(sizeof(CharManage));

    self->player = (CharObj **)malloc(sizeof(CharObj *) * player_max);
    self->enemy = (CharObj **)malloc(sizeof(CharObj *) * enemy_max);

    self->player_max = player_max;
    self->player_num = 0;
    self->enemy_max = enemy_max;
    self->enemy_num = 0;
    
    self->credits = 1;
	self->number_of_players = 1;

    self->level = 0;
    self->stage = 1;
    self->loop = 1;
    self->appear = 0;

    self->stage_enemy = 0;
    self->show_shoot_down = 0;
    self->zako_app = true;
    self->boss_app = false;
    self->boss_kill = false;

    self->boss_time = 3000;
    self->flag_maxlevel = false;
    self->flag_nopausemessage = false;
    self->program_should_quit = false;

    for (i = 0; i < self->player_max; i++)
    {
        self->player[i] = (CharObj *)malloc(sizeof(CharObj));
        self->player[i]->data.used = false;
        self->player[i]->action    = NullAct;
        self->player[i]->realize   = NullReal;
        self->player[i]->hit       = NullHit;
    }

    for (i = 0; i < self->enemy_max; i++)
    {
        self->enemy[i] = (CharObj *)malloc(sizeof(CharObj));
        self->enemy[i]->data.used = false;
        self->enemy[i]->action    = NullAct;
        self->enemy[i]->realize   = NullReal;
        self->enemy[i]->hit       = NullHit;
    }

    /* frequently used objects */

    /* enemy shot */
    self->enemy_shot.data.used = true;
    self->enemy_shot.data.kill = false;
    self->enemy_shot.data.hit_att = MESHOT;
    self->enemy_shot.data.hit_mask = MPLAYER;
    self->enemy_shot.data.enemy_att = NullDel;
    self->enemy_shot.data.width = 8;
    self->enemy_shot.data.height = 8;
    self->enemy_shot.data.harfw = self->enemy_shot.data.width/2;
    self->enemy_shot.data.harfh = self->enemy_shot.data.height/2;
    self->enemy_shot.data.image = 0;
    self->enemy_shot.data.cnt[4] = 0; /* image counter */
    self->enemy_shot.grp.image = &eshot_bitmap;

    self->enemy_shot.grp.width = al_get_bitmap_width(self->enemy_shot.grp.image[0]);
    self->enemy_shot.grp.height = al_get_bitmap_height(self->enemy_shot.grp.image[0])/4;

    self->enemy_shot.grp.harfw = self->enemy_shot.grp.width/2;
    self->enemy_shot.grp.harfh = self->enemy_shot.grp.height/2;

    /* small explosion */
    self->bomb.data.used = true;
    self->bomb.data.kill = false;
    self->bomb.data.hit_att = 0;
    self->bomb.data.hit_mask = 0;
    self->bomb.data.width = 0;
    self->bomb.data.height = 0;
    self->bomb.data.harfw = self->bomb.data.width/2;
    self->bomb.data.harfh = self->bomb.data.height/2;
    self->bomb.data.image = 0;
    self->bomb.data.cnt[0] = 0;
    self->bomb.grp.image = &bomb_bitmap;

    self->bomb.grp.width = al_get_bitmap_width(self->bomb.grp.image[0]);
    self->bomb.grp.height = al_get_bitmap_height(self->bomb.grp.image[0])/5;

    self->bomb.grp.harfw = self->bomb.grp.width/2;
    self->bomb.grp.harfh = self->bomb.grp.height/2;

    /* large explosion */
    self->large_bomb.data.used = true;
    self->large_bomb.data.kill = false;
    self->large_bomb.data.hit_att = 0;
    self->large_bomb.data.hit_mask = 0;
    self->large_bomb.data.width = 0;
    self->large_bomb.data.height = 0;
    self->large_bomb.data.harfw = self->large_bomb.data.width/2;
    self->large_bomb.data.harfh = self->large_bomb.data.height/2;
    self->large_bomb.data.image = 0;
    self->large_bomb.data.cnt[0] = 0;
    self->large_bomb.grp.image = &bigbomb_bitmap;

    self->large_bomb.grp.width = al_get_bitmap_width(self->large_bomb.grp.image[0]);
    self->large_bomb.grp.height = al_get_bitmap_height(self->large_bomb.grp.image[0])/5;

    self->large_bomb.grp.harfw = self->large_bomb.grp.width/2;
    self->large_bomb.grp.harfh = self->large_bomb.grp.height/2;

    return self;
}



void clear_manage(CharManage *manage_temp)
{
    int i;

    if (manage->stage > MAX_STAGE)
        manage->appear = - 100;
    else
        manage->appear = - 50;

    manage->zako_app = true;
    manage->boss_app = false;
    manage->boss_kill = false;

    manage->show_shoot_down = 1;

    manage->stage_enemy = 0;
    manage->stage_shot_down = 0;

    for (i=0; i<manage->enemy_max; i++)
    {
        if (manage->enemy[i]->data.used == true)
        {
            new_large_bomb(manage->enemy[i]->data.x,manage->enemy[i]->data.y);
            del_obj(manage->enemy[i]);
        }
    }
}

void reset_manage(CharManage *manage_temp)
{
    int i;

    manage->level = 10;
    if (manage->level > MAX_LEVEL)
        manage->level = MAX_LEVEL;
    if (manage->level < 0)
        manage->level = 0;

    if (manage->flag_maxlevel == true)
        manage->level = MAX_LEVEL;

    manage->appear = -50;
    manage->zako_app = true;
    manage->boss_app = false;
    manage->boss_kill = false;

    manage->stage_enemy = 0;
    manage->stage_shot_down = 0;
    manage->boss_time = 3000;

    manage->show_shoot_down = 0;

    for (i=0; i<manage->player_max; i++)
    {
        if (manage->player[i]->data.used == true)
            del_obj(manage->player[i]);
    }

    for (i=0; i<manage->enemy_max; i++)
    {
        if (manage->enemy[i]->data.used == true)
            del_obj(manage->enemy[i]);
    }

    for (i=0; i<manage->player_max; i++)
        manage->player[i]->data.used = false;

    for (i=0; i<manage->enemy_max; i++)
        manage->enemy[i]->data.used = false;

    manage->player_num = 0;
    manage->enemy_num = 0;
}

void DeleteManage(CharManage *Del)
{
    int i;

    for (i=0; i<Del->player_max; i++)
        free(Del->player[i]);
    for (i=0; i<Del->enemy_max; i++)
        free(Del->enemy[i]);

    free(Del->player);
    free(Del->enemy);
    free(Del);
}

int new_obj(int mask,
           DelAtt (*action)(ObjData *my),
           DelAtt (*hit)(ObjData *my, ObjData *your),
           void (*realize)(ObjData *my, GrpData *grp))
{
    int i;

    if (mask & (MPLAYER|MPSHOT))
    {
        if (manage->player_num >= manage->player_max)
            return -1;
        for (i=0; i<manage->player_max; i++)
        {
            if (manage->player[i]->data.used == false)
            {
                manage->player[i]->data    = manage->new_obj.data;
                manage->player[i]->grp     = manage->new_obj.grp;
                manage->player[i]->action  = action;
                manage->player[i]->realize = realize;
                manage->player[i]->hit     = hit;

                manage->player[i]->data.used = true;
                manage->player[i]->data.kill = false;
                manage->player[i]->data.harfw = manage->player[i]->data.width / 2;
                manage->player[i]->data.harfh = manage->player[i]->data.height / 2;

                manage->player[i]->data.image = 0;

                manage->player[i]->grp.width = al_get_bitmap_width(manage->player[i]->grp.image[0]);
                manage->player[i]->grp.height = al_get_bitmap_height(manage->player[i]->grp.image[0])/manage->player[i]->grp.sprites;

                manage->player[i]->grp.harfw = manage->player[i]->grp.width /2;
                manage->player[i]->grp.harfh = manage->player[i]->grp.height /2;
                manage->player[i]->data.not_shooting_time = 5;

                manage->player_num++;
                return i;
            }
        }
    }
    else
    {
        if (manage->enemy_num >= manage->enemy_max)
            return -1;

        if (manage->new_obj.data.enemy_att == BossDel)
            i = 0;
        else
            i = 1;
        for ( ; i<manage->enemy_max; i++)
        {
            if (manage->enemy[i]->data.used == false)
            {
                manage->enemy[i]->data    = manage->new_obj.data;
                manage->enemy[i]->grp     = manage->new_obj.grp;
                manage->enemy[i]->action  = action;
                manage->enemy[i]->realize = realize;
                manage->enemy[i]->hit     = hit;

                manage->enemy[i]->data.used = true;
                manage->enemy[i]->data.kill = false;
                manage->enemy[i]->data.harfw = manage->enemy[i]->data.width / 2;
                manage->enemy[i]->data.harfh = manage->enemy[i]->data.height / 2;

                manage->enemy[i]->data.start_time = manage->level;
                manage->enemy[i]->data.shot_time = (SHOT_TIMING + manage->level) / 2;

                manage->enemy[i]->data.image = 0;

                manage->enemy[i]->grp.width = al_get_bitmap_width(manage->enemy[i]->grp.image[0]);
                manage->enemy[i]->grp.height = al_get_bitmap_height(manage->enemy[i]->grp.image[0])/manage->enemy[i]->grp.sprites;

                manage->enemy[i]->grp.harfw = manage->enemy[i]->grp.width / 2;
                manage->enemy[i]->grp.harfh = manage->enemy[i]->grp.height / 2;

                manage->enemy[i]->data.show_damage_time = 0;

                manage->enemy_num++;
                return i;
            }
        }
    }
    return -1;
}

void del_obj(CharObj* del)
{
    if (del->data.used == true)
    {
        del->data.used = false;
        if (del->data.hit_att & (MPLAYER|MPSHOT))
            manage->player_num--;
        else
            manage->enemy_num--;
    }
}


/* TODO : modif cette fonction pour le high-scores !! */
PlayerData *new_player_data()
{
    PlayerData *self;
    char name[6] = "dummy";

    self = (PlayerData*)malloc(sizeof(PlayerData));

    sprintf(self->rec[0].name, name);
    self->rec[0].score = 0;
    self->rec[0].stage = 0;
    self->rec[0].loop = 0;

    /*ReadHiscore(self);*/

    return self;
}

void clear_enemy_shot_manage(CharManage* manage_temp)
{
    int i;
    for (i=0; i<manage->enemy_max; i++)
    {
        if (manage->enemy[i]->data.used == true)
            if ((manage->enemy[i]->action == EnemyShotAct)
                    || (manage->enemy[i]->action == HomingAct)
                    || (manage->enemy[i]->action == EnemyLaserAct)
                    || (manage->enemy[i]->action == BoundShotAct))
            {
                new_bomb(manage->enemy[i]->data.x,manage->enemy[i]->data.y);
                del_obj(manage->enemy[i]);
            }
    }
}


