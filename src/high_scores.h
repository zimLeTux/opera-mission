#ifndef H_HIGHSCORES
#define H_HIGHSCORES

void input_high_score();
void record_high_score(char name[], int player, int score);
void load_high_score(int scores[], int players[], char* names[]);

#endif
