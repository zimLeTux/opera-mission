#include <time.h>
#include <math.h>
#include <allegro5/allegro.h>

#include "game.h"
#include "enemy.h"
#include "boss.h"
#include "opera.h"
#include "callback.h"
#include "extern.h"
#include "manage.h"
#include "star.h"
#include "graphics.h"
#include "player.h"
#include "keys.h"
#include "high_scores.h"

extern CharManage* manage;
int fieldw;
int fieldh;
int action_keys_flags;

static int shoot_down_bonus(int percent, int loop, int stage);
static void collision_detection(void);
static void do_actions(void);

static void draw_infos(void);

int mainloop()
{
    int obj; /* loop counter */
    int ocheck; /* counter for already checked objects */
    int one_up = 0; /* 1up counter */
	int coordx; /* used to store x coord of variable length text */

    bool redraw;
	bool game_finished = false;

    ALLEGRO_EVENT event;
    int key;

    /* functions of normal enemies, weak ones first */
    int (*NewEnemy[])(int x, int y) =
    {
        NewEnemy1,NewEnemy2,NewEnemy3,NewEnemy4,NewEnemy5,
        NewEnemy6,NewEnemy7,NewEnemy8,NewEnemy9
    };

    /* functions of end-of-stage bosses */
    int (*NewBoss[])(void) =
    {
        NewBoss1,NewBoss2,NewBoss3,NewBoss4,NewBoss5,
        NewBoss6,NewBoss7,NewBoss8
    };

    /* number of normal enemies in each stage */
    int stage_obj[] =
    {
        80,80,100,100,120,120,140,140
    };

    char stage_name[][16] =
    {
        "Stage 1","Stage 2","Stage 3","Stage 4","Stage 5",
        "Stage 6","Stage 7","Final Stage",
        "All Clear!"
    };

    debug("loading player(s)...");
    /* add the player ship to the table */
    if (manage->number_of_players == 2)
    {
        new_player(0, fieldw/2 - 32, fieldh - 32);
        new_player(1, fieldw/2 + 32, fieldh - 32);
    }
    else
        new_player(0, fieldw/2, fieldh - 32);

    debug("player(s) loaded!");
	printf("manage->player_max = %d\n", manage->player_max);

    action_keys_flags = 0x0000;
    al_start_timer(fps);

    debug("entering mainloop");

	al_play_sample(level1_sample, 1.0f, 0.0f, 1.0f, ALLEGRO_PLAYMODE_LOOP, NULL);

	debug("playing sound");

    while (1)
    {
        /*
        if (waittime && (signal_delivered==0))
            pause();
        signal_delivered = 0;

        if (event_handle() == 0)
            return 0;
        */

        /**************************************************
         * keyboard events reading
         */

        if(!al_is_event_queue_empty(event_queue))
        {
            while(al_get_next_event(event_queue, &event))
            {
                switch (event.type)
                {
                case ALLEGRO_EVENT_DISPLAY_CLOSE:
                    manage->program_should_quit = true;
                    break;
                case ALLEGRO_EVENT_TIMER:
                    redraw = true;
                    /* debug("redraw!!"); */
                    /*printf("%d\n", iter++);fflush(stdout);*/
                    break;
                case ALLEGRO_EVENT_KEY_DOWN:
                    key = event.keyboard.keycode;
                    if ((manage->player[0]->data.kill == true ||
                    manage->player[1]->data.kill == true) &&
                    player->ships==0)
                        return 0;
                    switch (key)
                    {
                    case KEY_P1_UP:
                        action_keys_flags |= P1_UP;
                        break;
                    case KEY_P1_DOWN:
                        action_keys_flags |= P1_DOWN;
                        break;
                    case KEY_P1_LEFT:
                        action_keys_flags |= P1_LEFT;
                        break;
                    case KEY_P1_RIGHT:
                        action_keys_flags |= P1_RIGHT;
                        break;
                    case KEY_P1_FASTER:
                        action_keys_flags |= P1_FASTER;
                        break;
                    case KEY_P1_SLOWER:
                        action_keys_flags |= P1_SLOWER;
                        break;
                    case KEY_P1_SHOOT:
                        action_keys_flags |= P1_SHOOT;
                        break;
                    case KEY_P2_UP:
                        action_keys_flags |= P2_UP;
                        break;
                    case KEY_P2_DOWN:
                        action_keys_flags |= P2_DOWN;
                        break;
                    case KEY_P2_LEFT:
                        action_keys_flags |= P2_LEFT;
                        break;
                    case KEY_P2_RIGHT:
                        action_keys_flags |= P2_RIGHT;
                        break;
                    case KEY_P2_FASTER:
                        action_keys_flags |= P2_FASTER;
                        break;
                    case KEY_P2_SLOWER:
                        action_keys_flags |= P2_SLOWER;
                        break;
                    case KEY_P2_SHOOT:
                        action_keys_flags |= P2_SHOOT;
                        break;
                    default:
                        break;
                    }
                    break;
                case ALLEGRO_EVENT_KEY_UP:
                    key = event.keyboard.keycode;
                    switch (key)
                    {
                    case KEY_P1_UP:
                        action_keys_flags &= ~P1_UP;
                        break;
                    case KEY_P1_DOWN:
                        action_keys_flags &= ~P1_DOWN;
                        break;
                    case KEY_P1_LEFT:
                        action_keys_flags &= ~P1_LEFT;
                        break;
                    case KEY_P1_RIGHT:
                        action_keys_flags &= ~P1_RIGHT;
                        break;
                    case KEY_P1_FASTER:
                        action_keys_flags &= ~P1_FASTER;
                        break;
                    case KEY_P1_SLOWER:
                        action_keys_flags &= ~P1_SLOWER;
                        break;
                    case KEY_P1_SHOOT:
                        action_keys_flags &= ~P1_SHOOT;
                        break;
                    case KEY_P2_UP:
                        action_keys_flags &= ~P2_UP;
                        break;
                    case KEY_P2_DOWN:
                        action_keys_flags &= ~P2_DOWN;
                        break;
                    case KEY_P2_LEFT:
                        action_keys_flags &= ~P2_LEFT;
                        break;
                    case KEY_P2_RIGHT:
                        action_keys_flags &= ~P2_RIGHT;
                        break;
                    case KEY_P2_FASTER:
                        action_keys_flags &= ~P2_FASTER;
                        break;
                    case KEY_P2_SLOWER:
                        action_keys_flags &= ~P2_SLOWER;
                        break;
                    case KEY_P2_SHOOT:
                        action_keys_flags &= ~P2_SHOOT;
                        break;
                    default:
                        break;
                    }
                    break;
                }
            }
        }


        if (redraw)
        {
#if 1
            /**************************************************
            * check end of game
            */
            /*debug(". check end of game");*/
		    /* TODO add end of game func! */
            if (manage->stage > MAX_STAGE && manage->appear > 0)
                break;
#endif

            /**************************************************
            * check players deaths
            */
#if 1
            if (manage->player[0]->data.used == false)
            {
                /* the player is killed */
                player->rec[0].loop = manage->loop;
                player->rec[0].stage = manage->stage;
                if (player->ships > 0)
                {
                    player->ships--;
                    clear_enemy_shot_manage(manage);
                    player_lose_power(0);
                    restart_player(0, fieldw/2, fieldh - 32);
                }
            }

            /* FIXME: remove -> add a func for this ?*/
            if ((manage->number_of_players == 2) &&
				(manage->player[1]->data.used == false))
            {
                /* the player 2 is killed */
                player->rec[0].loop = manage->loop;
                player->rec[0].stage = manage->stage;
                if (player->ships > 0)
                {
                    player->ships--;
                    clear_enemy_shot_manage(manage);
                    player_lose_power(1);
                    restart_player(1, fieldw/2, fieldh - 32);
                }
            }
#endif


            /**************************************************
            * check boss death
            */
#if 1
            if (manage->boss_kill == true)
            {
                /* the boss is killed, or it escaped */
                manage->stage++;

                manage->level += 5;

                if (manage->boss_time >=1)
                {
                    /* the boss is dead */
                    player->percent = (manage->stage_shot_down/(double)manage->stage_enemy) * 100;
                    /* note that manage->Stage is already incremented */
                    player->rec[0].score += shoot_down_bonus(player->percent, manage->loop, manage->stage);
                    if (player->percent >= 100)
                    {
                        /*
                        player->rec[0].score += perfect_bonus(manage->Loop, manage->Stage);
                        */
                        manage->level += 7;
                    }

                }
                else
                {
                    manage->level -= 3;
                }
                if (manage->level > MAX_LEVEL)
                    manage->level = MAX_LEVEL;
                if (manage->level < 0)
                    manage->level = 0;

                if (manage->flag_maxlevel == true)
                    manage->level = MAX_LEVEL;

                clear_manage(manage);

                change_star_parameter(star_ptn_1, 20);
                change_star_parameter(star_ptn_2, 25);
            }
#endif


            /***************************************************
            * check if boss should appear
            */
#if 1
            if (manage->appear >= 100)
            {
                if ((manage->stage_enemy >= stage_obj[manage->stage-1]) &&
					(manage->boss_app==false))
                {
                    /* the boss appears */
                    if (NewBoss[manage->stage-1]() != -1)
                    {
                        manage->zako_app = false;
                        manage->boss_app = true;
                        manage->stage_enemy++;
                        if (manage->stage == 8)
                            manage->boss_time = 3000;
                        else
                            manage->boss_time = 2000;
                    }
                }
                else if (manage->zako_app == true)
                {
                    /* normal enemy */
                    if (NewEnemy[rand()%(manage->stage+1)]((rand()%fieldw)+1,0) != -1)
                    {
                        manage->stage_enemy++;
                    }
                }
                /* how often normal enemies appear */
                manage->appear = 89;
            }
            else
                manage->appear++;
#endif


            /**************************************************
            * actions
            */

            /*debug(". do actions");*/
            do_actions();

            /*debug(". collision detection");*/
            collision_detection();

#if 1
            /* the player gets 1up? */
            if (player->rec[0].score >= player->next)
            {
                player->next += EVERY1UP;
                player->ships++;

                /* counter to display 1up message */
                one_up = 1;
            }


            if (player->rec[0].score >= 10000000)
                player->rec[0].score = 10000000;
            if (player->rec[0].score < 0)
                player->rec[0].score = 0;

#endif

            /**************************************************
            * draw screen
            */

            /*debug(". redraw screen");*/
            redraw = false;
            al_clear_to_color(al_map_rgb(0,0,0));

            /* pixmaps for the background */
            /*debug(".. pixmaps for the background");*/
            draw_star(star_ptn_1);
            draw_star(star_ptn_2);


            /* pixmaps for objects */

            for (obj=0, ocheck=0;
				 (obj<manage->enemy_max && ocheck<manage->enemy_max);
				 obj++)
            {
                if (manage->enemy[obj]->data.used == true)
                {
                    manage->enemy[obj]->realize(&(manage->enemy[obj]->data),
											  &(manage->enemy[obj]->grp));
                    ocheck++;
                }
            }

            for (obj=manage->player_max-1, ocheck=0;
				 (obj>=0 && ocheck<manage->player_num);
				 obj--)
            {
                if (manage->player[obj]->data.used == true)
                {
                    /*debug("PLAYER CHECK ENTERED");*/
                    manage->player[obj]->realize(&(manage->player[obj]->data),
											   &(manage->player[obj]->grp));
                    ocheck++;
                }
            }

            /* score and other stuff */
            draw_infos();

            /*debug(". flip display");*/
            al_flip_display();


#if 1
            /* yet more misc stuff */
            if (one_up != 0)
            {
                if (one_up%4 > 1)
                    draw_string(440, 620, "1UP", strlen("1UP"));
                one_up++;
                if (one_up > 50)
                    one_up = 0;
            }

            if (((manage->player[0]->data.kill == true ||
				  manage->player[1]->data.kill == true) &&
				 player->ships==0) ||
				game_finished)
			{
                draw_big_string(120, 300, "Game Over", strlen("Game Over"));
				if (!game_finished)
				{
				    debug("GAME OVER!");
				    al_stop_samples();
					input_high_score();
					game_finished = true;
				}
			}

            if (manage->appear < 0)
            {
                char percent[32];
                char bonus[32];
                char perfect[32];

                if (manage->show_shoot_down != 0)
                {
                    /* shoot down bonus message */
                    if (manage->boss_time >= 1)
                    {
                        sprintf(percent,"shoot down %02d%%",player->percent);
                        draw_string(210, 370, percent, strlen(percent));


                        sprintf(bonus,"Bonus %d pts", shoot_down_bonus(player->percent, manage->loop, manage->stage));
                        draw_string(260 + manage->appear*3 , 400,
                                    bonus, strlen(bonus));

                        if (player->percent >= 100)
                        {
                            sprintf(perfect,"Perfect!!");
                            draw_string(170 - manage->appear*3 , 420,
                                        perfect, strlen(perfect));
                        }
                    }
                    else
                    {
                        snprintf(percent, 32, "the boss escaped");
						coordx = (576-16*strlen(percent))/2;
                        draw_big_yellow_string(coordx, 370, percent, strlen(percent));
                    }

                }
                draw_string(230, 320, stage_name[manage->stage-1],
                            strlen(stage_name[manage->stage-1]));
            }
#endif
            if (manage->appear == 0)
            {
                change_star_parameter(star_ptn_1,5);
                change_star_parameter(star_ptn_2,10);
            }
        }
    }

	al_stop_samples();

    /* ending */
    return 1;
}

void new_bomb(int x, int y)
{
    int i;

    if (manage->enemy_num >= manage->enemy_max)
        return;

    for (i = 1; i < manage->enemy_max; i++)
    {
        if (manage->enemy[i]->data.used == false)
        {
            manage->bomb.data.x = x;
            manage->bomb.data.y = y;

            manage->enemy[i]->data    = manage->bomb.data;
            manage->enemy[i]->grp     = manage->bomb.grp;
            manage->enemy[i]->action  = BombAct;
            manage->enemy[i]->realize = DrawImage;
            manage->enemy[i]->hit     = NullHit;

            manage->enemy_num++;
            return;
        }
    }
}

void new_large_bomb(int x, int y)
{
    int i;

    if (manage->enemy_num >= manage->enemy_max)
        return;

    for (i = 1; i < manage->enemy_max; i++)
    {
        if (manage->enemy[i]->data.used == false)
        {
            manage->large_bomb.data.x = x;
            manage->large_bomb.data.y = y;

            manage->enemy[i]->data    = manage->large_bomb.data;
            manage->enemy[i]->grp     = manage->large_bomb.grp;
            manage->enemy[i]->action  = BombAct;
            manage->enemy[i]->realize = DrawImage;
            manage->enemy[i]->hit     = NullHit;

            manage->enemy_num++;
            return;
        }
    }
}

static int shoot_down_bonus(int percent, int loop, int stage)
{
    if (percent < 0)
    {
        fprintf(stderr, "shoot_down_bonus: negative percent given, "
                "assuming 0%%\n");
        percent = 0;
    }
    if (percent > 100)
    {
        fprintf(stderr, "shoot_down_bonus: 101+ percent given, "
                "assuming 100%%\n");
        percent = 100;
    }
    if (percent == 0)
        return 0;


    return (30000 + stage * stage * 1000) * 5 / (105 - percent);
}


static void do_actions(void)
{
    int obj;
    DelAtt DelFlag;

    for (obj=manage->player_max-1; obj>=0; obj--)
        manage->player[obj]->data.should_act = manage->player[obj]->data.used;
    for (obj=manage->player_max-1; obj>=0; obj--)
    {
        if (manage->player[obj]->data.should_act == true)
        {
            if (manage->player[obj]->action(&(manage->player[obj]->data)) == NullDel)
                del_obj(manage->player[obj]);
        }
    }

    for (obj=0; obj<manage->enemy_max; obj++)
        manage->enemy[obj]->data.should_act = manage->enemy[obj]->data.used;
    for (obj=0; obj<manage->enemy_max; obj++)
    {
        if (manage->enemy[obj]->data.used == true)
        {
            DelFlag = manage->enemy[obj]->action(&(manage->enemy[obj]->data));
            switch (DelFlag)
            {
            case NoneDel:
                /* do nothing */
                break;
            case BossDel:
#ifdef DEBUG
                fprintf(stderr, "DelFlag == BossDel while processing Action: obj = %d\n", obj);
#endif
                if ((DelFlag == BossDel) && (manage->boss_time <= 0))
                    player->rec[0].score -= manage->enemy[obj]->data.point;
                manage->boss_kill = true;
            /* fall off */
            case ZakoDel:
                player->rec[0].score += manage->enemy[obj]->data.point;
                manage->stage_shot_down++;
            /* fall off */
            case NullDel:
                del_obj(manage->enemy[obj]);
                break;
            }
        }
    }
}







static void collision_detection(void)
{
    int obj;
    int target;
    DelAtt DelFlag;

    for (obj=0; (obj<manage->player_max); obj++)
    {
        if (manage->player[obj]->data.used == false)
            continue;
        if (manage->player[obj]->data.kill == true)
            continue;

        for (target=0; (target<manage->enemy_max); target++)
        {
            if (manage->enemy[target]->data.used == false)
                continue;
            if (manage->enemy[target]->data.kill == true)
                continue;

            if (manage->player[obj]->data.hit_mask & manage->enemy[target]->data.hit_att)
            {
                if (abs(manage->player[obj]->data.x-manage->enemy[target]->data.x)
                        > manage->player[obj]->data.harfw+manage->enemy[target]->data.harfw)
                    continue;
                if (abs(manage->player[obj]->data.y-manage->enemy[target]->data.y)
                        > manage->player[obj]->data.harfh+manage->enemy[target]->data.harfh)
                    continue;

                /* crash! */
                /* we call the enemy's Hit first because the Hit of
                 * the player shot 3 changes its Attatck */
                DelFlag = manage->enemy[target]->hit(&(manage->enemy[target]->data),&(manage->player[obj]->data));
                switch (DelFlag)
                {
                case NoneDel:
                    /* ignore it */
                    break;

                case BossDel:
#ifdef DEBUG
                    fprintf(stderr, "DelFlag == BossDel while processing Hit: target = %d, obj = %d\n", target, obj);
#endif
                    if ((DelFlag == BossDel) && (manage->boss_time <= 0))
                        player->rec[0].score -= manage->enemy[target]->data.point;
                    manage->boss_kill = true;
                case ZakoDel:
                    player->rec[0].score += manage->enemy[target]->data.point;
                    manage->stage_shot_down++;
                case NullDel:
                    del_obj(manage->enemy[target]);
                }

                if (manage->player[obj]->hit(&(manage->player[obj]->data),&(manage->enemy[target]->data)) == NullDel)
                    del_obj(manage->player[obj]);
            }
        }
    }
}

int get_direction(int mx, int my, int sx, int sy)
{
    static double hi;
    static int uw;
    static int uh;
    static int h;
    static int w;

    uw = abs(sx-mx);
    uh = abs(sy-my);
    h = sy-my;
    w = sx-mx;

    if (!uw) return (uh>0)?4:0;
    if (!uh) return (uw>0)?2:6;

    hi = (double)uh/uw;
    if (hi < 0.42)
        return (w > 0) ? 2: 6;
    else if (hi > 2.42)
        return (h > 0) ? 4: 0;
    else
    {
        return (w>0)?((h>0)?3:1):((h>0)?5:7);
        /***
        if (w > 0 && h > 0) return 3;
        if (w > 0 && h < 0) return 1;
        if (w < 0 && h > 0) return 5;
        if (w < 0 && h < 0) return 7;
        ***/
    }
}


static void draw_infos(void)
{
  /*#define DEBUG*/
    static char Score[64];
    static char Ships[16];
    static char Stage[16];
#ifdef DEBUG
    static char ObjectP[32];
    static char ObjectE[32];
    static char Loop[16];
    static char Level[16];
    static char Weapon[16];
    static char Pow[16];
    static char Speed[16];
    static char Enemy[16];
    static char EnemyKill[16];
#endif
    static char EnemyHP[5];
    static char BossTime[16];

    int i;

    sprintf(Score,"SCORE % 8d", player->rec[0].score);
    sprintf(Stage,"Stage %2d", manage->stage);
    sprintf(Ships,"Ships %3d", player->ships);
#ifdef DEBUG
    sprintf(ObjectE,"Enemy Object %3d", manage->enemy_num);
    sprintf(ObjectP,"Player Object %3d", manage->player_num);
    sprintf(Loop,"Loop %2d", manage->loop);
    sprintf(Level,"Level %3d", manage->level);
    sprintf(Weapon,"Weapon %d", manage->player[0]->data.cnt[5]);
    sprintf(Pow,"Pow %2d", manage->player[0]->data.cnt[6]);
    sprintf(Speed,"Speed %2d", manage->player[0]->data.speed);
    sprintf(Enemy,"Enemy %3d", manage->stage_enemy);
    sprintf(EnemyKill,"EnemyKill %3d", manage->stage_shot_down);
#endif

    draw_big_color_string(10, 20, Score, strlen(Score));
    draw_big_yellow_string(430, 20, Stage, strlen(Stage));
    draw_big_string(430, 640, Ships, strlen(Ships));
#ifdef DEBUG
    draw_string(10, 40, ObjectE, strlen(ObjectE));
    draw_string(10, 60, ObjectP, strlen(ObjectP));
    draw_string(10, 80, Level, strlen(Level));
    draw_string(10, 100, Enemy, strlen(Enemy));
    draw_string(10, 120, EnemyKill, strlen(EnemyKill));
    draw_string(430, 60, Loop, strlen(Loop));
    draw_string(430, 580, Weapon, strlen(Weapon));
    draw_string(430, 600, Pow, strlen(Pow));
    draw_string(430, 620, Speed, strlen(Speed));
#endif
    for (i = 0; i<manage->enemy_max; i++)
        if (manage->enemy[i]->data.used == true)
            if ((manage->enemy[i]->hit == EnemyHit1)
                    ||(manage->enemy[i]->hit == DamageHit)
                    ||(manage->enemy[i]->hit == LargeDamageHit)
                    ||(manage->enemy[i]->hit == BossHit1))
                if (manage->enemy[i]->data.show_damage_time >0)
                {
                    snprintf(EnemyHP, 5, "%d",manage->enemy[i]->data.hp);
                    draw_string(manage->enemy[i]->data.x, manage->enemy[i]->data.y,
                                EnemyHP, strlen(EnemyHP));
                    (manage->enemy[i]->data.show_damage_time)--;
                }
    if (manage->boss_app == true)
    {
        snprintf(BossTime, 16, "Time %4d",manage->boss_time);
        draw_string(430, 40, BossTime, strlen(BossTime));
    }

#if 0
    for (i = 0; i<manage->enemy_max; i++)
        if (manage->enemy[i]->data.used == true)
            /* DrawRec does not use arg 2, so NULL will be enough */
            DrawRec(&(manage->enemy[i]->data), NULL);
    for (i = 0; i<manage->player_max; i++)
        if (manage->player[i]->data.used == true)
            /* DrawRec does not use arg 2, so NULL will be enough */
            DrawRec(&(manage->player[i]->data), NULL);

#endif /* DEBUG */

#undef DEBUG


}
