#ifndef H_GRAPHICS
#define H_GRAPHICS

void load_graphics();
int draw_string(int x, int y, const char *string, int length);
int draw_char(int x, int y, int c);
int draw_big_string(int x, int y, const char *string, int length);
int draw_big_char(int x, int y, int c);
int draw_big_yellow_string(int x, int y, const char *string, int length);
int draw_big_yellow_char(int x, int y, int c);
int draw_big_color_string(int x, int y, const char *string, int length);
int draw_big_color_char(int x, int y, int c);

#endif
