#ifndef H_KEYS
#define H_KEYS

#define KEY_P1_UP       ALLEGRO_KEY_Z
#define KEY_P1_DOWN     ALLEGRO_KEY_S
#define KEY_P1_LEFT     ALLEGRO_KEY_Q
#define KEY_P1_RIGHT    ALLEGRO_KEY_D

#define KEY_P1_FASTER   ALLEGRO_KEY_A
#define KEY_P1_SLOWER   ALLEGRO_KEY_E

#define KEY_P1_SHOOT    ALLEGRO_KEY_V

#define KEY_P2_UP       ALLEGRO_KEY_O
#define KEY_P2_DOWN     ALLEGRO_KEY_L
#define KEY_P2_LEFT     ALLEGRO_KEY_K
#define KEY_P2_RIGHT    ALLEGRO_KEY_M

#define KEY_P2_FASTER   ALLEGRO_KEY_I
#define KEY_P2_SLOWER   ALLEGRO_KEY_P

#define KEY_P2_SHOOT    ALLEGRO_KEY_H

#define KEY_COIN        ALLEGRO_KEY_1
#define KEY_START_1P    ALLEGRO_KEY_2
#define KEY_START_2P    ALLEGRO_KEY_3

#define P1_UP       0x0001
#define P1_DOWN     0x0002
#define P1_LEFT     0x0004
#define P1_RIGHT    0x0008

#define P1_FASTER   0x0010
#define P1_SLOWER   0x0020

#define P1_SHOOT    0x0040

#define P2_UP       0x0100
#define P2_DOWN     0x0200
#define P2_LEFT     0x0400
#define P2_RIGHT    0x0800

#define P2_FASTER   0x1000
#define P2_SLOWER   0x2000

#define P2_SHOOT    0x4000

#define COIN        0x10000
#define START_1P    0x20000
#define START_2P    0x40000

#endif
