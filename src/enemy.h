#ifndef H_ENEMY
#define H_ENEMY

#include "opera.h"

extern int NewEnemy1(int x, int y);
extern DelAtt EnemyAct1(ObjData *self);
extern DelAtt EnemyHit1(ObjData *self, ObjData *your);

extern int NewEnemy2(int x, int y);
extern DelAtt EnemyAct2(ObjData *self);
extern int NewEnemy3(int x, int y);
extern DelAtt EnemyAct3(ObjData *self);
extern int NewEnemy4(int x, int y);
extern DelAtt EnemyAct4(ObjData *self);
extern int NewEnemy5(int x, int y);
extern DelAtt EnemyAct5(ObjData *self);
extern int NewEnemy6(int x, int y);
extern DelAtt EnemyAct6(ObjData *self);
extern int NewEnemy7(int x, int y);
extern DelAtt EnemyAct7(ObjData *self);
extern int NewEnemy8(int x, int y);
extern DelAtt EnemyAct8(ObjData *self);
extern int NewEnemy9(int x, int y);
extern DelAtt EnemyAct9(ObjData *self);
extern int NewEnemy10(int x, int y);
extern DelAtt EnemyAct10(ObjData *self);

#endif /* H_ENEMY */
