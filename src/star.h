#ifndef H_STAR
#define H_STAR

void init_star_module(unsigned int w, unsigned int h);
int create_star(const char* filename, int pattern, int speed, int nstar);
void draw_star(int id);
void change_star_parameter(int id, int speed);
int delete_all_stars();

#endif /* H_STAR */
