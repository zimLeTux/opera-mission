#ifndef H_ENEMY_SHOT
#define H_ENEMY_SHOT

void shot_to_angle(int x, int y, int angle, int speed);
void shot_to_point(int x1, int y1, int x2, int y2, int speed);
int ring_to_angle(int x, int y, int angle, int speed);
int ring_to_point(int x1, int y1, int x2, int y2, int speed);
int homing_shot(int x, int y, int ix, int iy);
int laser_shot(int x, int y, int speed);
int bound_shot(int x, int y, int ix, int iy, int bound);

DelAtt EnemyLaserAct(ObjData *self);
DelAtt BoundShotAct(ObjData *self);
DelAtt HomingAct(ObjData *self);

#endif /* H_ENEMY_SHOT */
