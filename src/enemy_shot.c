#include <stdlib.h>
#include "game.h"

#include "callback.h"
#include "enemy_shot.h"
#include "extern.h"
#include "sin.h"


/* enemy shot */
void shot_to_angle(int x, int y, int angle, int speed)
{
    int i;

    if (manage->enemy_num >= manage->enemy_max)
        return;

    if (speed <= 0)
        speed = 1;

    for (i = 1; i < manage->enemy_max; i++)
    {
        if (manage->enemy[i]->data.used == false)
        {
            manage->enemy_shot.data.x = x;
            manage->enemy_shot.data.y = y;
            manage->enemy_shot.data.angle = angle;
            manage->enemy_shot.data.speed = speed;
            manage->enemy_shot.data.cnt[0] = x << 8;
            manage->enemy_shot.data.cnt[1] = y << 8;
            manage->enemy_shot.data.cnt[2] = icos(angle);
            manage->enemy_shot.data.cnt[3] = isin(angle);

            manage->enemy[i]->data    = manage->enemy_shot.data;
            manage->enemy[i]->grp     = manage->enemy_shot.grp;
            manage->enemy[i]->action  = EnemyShotAct;
            manage->enemy[i]->realize = DrawImage;
            manage->enemy[i]->hit     = NullDelHit;

            manage->enemy_num++;
            return;
        }
    }
}

void shot_to_point(int x1, int y1, int x2, int y2, int speed)
{
    int i;
    int diffx = x2 - x1;
    int diffy = y2 - y1;
    int absx = abs(diffx);
    int absy = abs(diffy);

    if (manage->enemy_num >= manage->enemy_max)
        return;

    if (speed <= 0)
        speed = 1;

    for (i = 1; i < manage->enemy_max; i++)
    {
        if (manage->enemy[i]->data.used == false)
        {
            manage->enemy_shot.data.x = x1;
            manage->enemy_shot.data.y = y1;
            manage->enemy_shot.data.speed = speed;
            manage->enemy_shot.data.cnt[0] = x1 << 8;
            manage->enemy_shot.data.cnt[1] = y1 << 8;
            if (absx >= absy)
            {
                manage->enemy_shot.data.cnt[2] = 1 << 8;
                manage->enemy_shot.data.cnt[3] = (absy/(double)absx) * 256;
            }
            else
            {
                manage->enemy_shot.data.cnt[2] = (absx/(double)absy) * 256;
                manage->enemy_shot.data.cnt[3] = 1 << 8;
            }
            if (diffx < 0)
                manage->enemy_shot.data.cnt[2] *= -1;
            if (diffy < 0)
                manage->enemy_shot.data.cnt[3] *= -1;

            manage->enemy[i]->data    = manage->enemy_shot.data;
            manage->enemy[i]->grp     = manage->enemy_shot.grp;
            manage->enemy[i]->action  = EnemyShotAct;
            manage->enemy[i]->realize = DrawImage;
            manage->enemy[i]->hit     = NullDelHit;

            manage->enemy_num++;
            return;
        }
    }
}

int ring_to_angle(int x, int y, int angle, int speed)
{
    if (speed <= 0)
        speed = 1;

    manage->new_obj.data.hit_att = MENEMY;
    manage->new_obj.data.hit_mask = MPLAYER | MPSHOT;

    manage->new_obj.data.x = x;
    manage->new_obj.data.y = y;
    manage->new_obj.data.hp = 1;
    manage->new_obj.data.point = 0;
    manage->new_obj.data.angle = angle;
    manage->new_obj.data.speed = speed;
    manage->new_obj.data.cnt[0] = x << 8;
    manage->new_obj.data.cnt[1] = y << 8;
    manage->new_obj.data.cnt[2] = icos(angle);
    manage->new_obj.data.cnt[3] = isin(angle);
    manage->new_obj.data.cnt[4] = 0;
    manage->new_obj.data.enemy_att = NullDel;
    manage->new_obj.data.width = 28;
    manage->new_obj.data.height = 28;

    manage->new_obj.grp.image = &ering_bitmap;

    return new_obj(MENEMY, EnemyShotAct, DeleteHit, DrawImage);
}

int ring_to_point(int x1, int y1, int x2, int y2, int speed)
{
    int diffx = x2 - x1;
    int diffy = y2 - y1;
    int absx = abs(diffx);
    int absy = abs(diffy);

    if (speed <= 0)
        speed = 1;

    manage->new_obj.data.hit_att = MENEMY;
    manage->new_obj.data.hit_mask = MPLAYER | MPSHOT;

    manage->new_obj.data.x = x1;
    manage->new_obj.data.y = y1;
    manage->new_obj.data.hp = 1;
    manage->new_obj.data.point = 0;
    manage->new_obj.data.speed = speed;
    manage->new_obj.data.cnt[0] = x1 << 8;
    manage->new_obj.data.cnt[1] = y1 << 8;

    if (absx >= absy)
    {
        manage->new_obj.data.cnt[2] = 1 << 8;
        manage->new_obj.data.cnt[3] = (absy/(double)absx) * 256;
    }
    else
    {
        manage->new_obj.data.cnt[2] = (absx/(double)absy) * 256;
        manage->new_obj.data.cnt[3] = 1 << 8;
    }
    if (diffx < 0)
        manage->new_obj.data.cnt[2] *= -1;
    if (diffy < 0)
        manage->new_obj.data.cnt[3] *= -1;

    manage->new_obj.data.cnt[4] = 0;
    manage->new_obj.data.enemy_att = NullDel;
    manage->new_obj.data.width = 28;
    manage->new_obj.data.height = 28;

    manage->new_obj.grp.image = &ering_bitmap;

    return new_obj(MENEMY, EnemyShotAct, DeleteHit, DrawImage);
}

int homing_shot(int x, int y, int ix, int iy)
{
    manage->new_obj.data.hit_att = MENEMY;
    manage->new_obj.data.hit_mask = MPLAYER | MPSHOT;

    manage->new_obj.data.x = x;
    manage->new_obj.data.y = y;
    manage->new_obj.data.hp = 1;
    manage->new_obj.data.point = 0;
    manage->new_obj.data.enemy_att = NullDel;
    manage->new_obj.data.width = 32;
    manage->new_obj.data.height = 32;
    manage->new_obj.data.inertx = ix;
    manage->new_obj.data.inerty = iy;

    manage->new_obj.grp.image = &emissile_bitmap;

    return new_obj(MENEMY, HomingAct, DeleteHit, DrawImage);
}

DelAtt HomingAct(ObjData *self)
{
    if (self->x < manage->player[0]->data.x)
    {
        if (self->inertx < 15)
            self->inertx += 1;
    }
    else
    {
        if (self->inertx > -15)
            self->inertx -= 1;
    }

    if (self->y < manage->player[0]->data.y)
        self->inerty++;
    else
        self->inerty--;

    self->x += self->inertx;
    self->y += self->inerty;

    self->image = get_direction(0,0,self->inertx,self->inerty);

    if ((self->x < 0 - self->width/2) || (self->x > fieldw + self->width/2))
        return NullDel;
    if ((self->y < 0 - self->height/2) || (self->y > fieldh + self->height/2))
        return NullDel;

    return NoneDel;
}

int laser_shot(int x, int y, int speed)
{
    if (speed <= 0)
        speed = 1;

    manage->new_obj.data.hit_att = MESHOT;
    manage->new_obj.data.hit_mask = MPLAYER;

    manage->new_obj.data.x = x;
    manage->new_obj.data.y = y;
    manage->new_obj.data.enemy_att = NullDel;
    manage->new_obj.data.width = 7;
    manage->new_obj.data.height = 50;
    manage->new_obj.data.speed = speed;

    manage->new_obj.grp.image = &elaser_bitmap;

    return new_obj(MESHOT, EnemyLaserAct, NullHit, DrawImage);
}

DelAtt EnemyLaserAct(ObjData *self)
{
    self->y += self->speed;

    if ((self->x < 0 - self->width/2) || (self->x > fieldw + self->width/2))
        return NullDel;
    if ((self->y < 0 - self->height/2) || (self->y > fieldh + self->height/2))
        return NullDel;

    return NoneDel;
}

int bound_shot(int x, int y, int ix, int iy, int bound)
{
    if ((ix == 0) && (iy == 0))
        ix = 1;

    manage->new_obj.data.hit_att = MENEMY;
    manage->new_obj.data.hit_mask = MPLAYER | MPSHOT;

    manage->new_obj.data.x = x;
    manage->new_obj.data.y = y;
    manage->new_obj.data.enemy_att = NullDel;
    manage->new_obj.data.width = 28;
    manage->new_obj.data.height = 28;
    manage->new_obj.data.inertx = ix;
    manage->new_obj.data.inerty = iy;

    manage->new_obj.data.cnt[0] = 0;
    manage->new_obj.data.cnt[1] = bound;

    manage->new_obj.grp.image = &ebound_bitmap;

    return new_obj(MESHOT, BoundShotAct, NullHit, DrawImage);
}

DelAtt BoundShotAct(ObjData *self)
{
    if (self->cnt[0] <= self->cnt[1])
    {
        if ((self->x + self->inertx > fieldw) || (self->x + self->inertx < 0))
        {
            self->inertx = self->inertx * (-1);
            self->cnt[0]++;
        }
        if ((self->y + self->inerty > fieldh) || (self->y + self->inerty < 0))
        {
            self->inerty = self->inerty*(-1);
            self->cnt[0]++;
        }
    }

    self->image++;
    if (self->image >= 8)
        self->image = 0;

    self->x += self->inertx;
    self->y += self->inerty;

    if ((self->x < 0 - self->width/2) || (self->x > fieldw + self->width/2))
        return NullDel;
    if ((self->y < 0 - self->height/2) || (self->y > fieldh + self->height/2))
        return NullDel;

    return NoneDel;
}
