#ifndef H_SIN
#define H_SIN

double dsin(int theta);
int isin(int theta);
#define dcos(i) dsin(i+90)
#define icos(i) isin(i+90)

#endif /* H_SIN */
