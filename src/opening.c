#include <stdlib.h>
#include <string.h>

#include <allegro5/allegro.h>

#include "opening.h"
#include "star.h"
#include "extern.h"
#include "graphics.h"
#include "keys.h"
#include "constants.h"
#include "high_scores.h"

int star_ptn_1 = 0;
int star_ptn_2 = 1;

void draw_high_scores();
void draw_story();

void opening()
{
    bool status = true;
    bool redraw = false;

    int key;
    
    int frames = 0;
    int seconds = 0;
    ALLEGRO_EVENT event;
    ALLEGRO_BITMAP* title_bitmap;
    
    change_star_parameter(star_ptn_1, 5);
    change_star_parameter(star_ptn_2, 10);
    
    reset_manage(manage);
    
    title_bitmap = al_load_bitmap("data/pixmap/title.tga");
    
    al_start_timer(fps);

	al_stop_samples();
    al_play_sample(intro_sample, 1.0f, 0.0f, 1.0f, ALLEGRO_PLAYMODE_LOOP, NULL);

	debug("ready for mainloop!");
    
    while (status)
    {
        if(!al_is_event_queue_empty(event_queue))
        {
            while(al_get_next_event(event_queue, &event))
            {
                switch (event.type)
                {
                case ALLEGRO_EVENT_DISPLAY_CLOSE:
                    manage->program_should_quit = true;
                    status = false;
                    break;
                case ALLEGRO_EVENT_TIMER:
                    redraw = true;
                    frames++;
                    if (frames >= 50)
                    {
                        frames = 0;
                        seconds++;
                    }
                    break;
                case ALLEGRO_EVENT_KEY_DOWN:
                    key = event.keyboard.keycode;
					
                    switch (key)
                    {
                    case KEY_START_1P:
                        if (manage->credits > 0)
                        {
                            manage->number_of_players = 1;
                            debug("1 yolo");
                            status = false;
                        }
                        break;
                    case KEY_START_2P:
                        if (manage->credits > 0)
                        {
                            manage->number_of_players = 2;
                            debug("2 yolos");
                            status = false;
                        }
                        break;
                    case KEY_COIN:
                        if (manage->credits < 9)
                            manage->credits++;
                        break;
                    }
                    break;
                }
            }
        }
        
        if (redraw)
        {
            redraw = false;
            al_clear_to_color(al_map_rgb(0,0,0));
            
            draw_star(star_ptn_1);
            draw_star(star_ptn_2);
            
            al_draw_bitmap(title_bitmap, 28, 80, 0);
            
            if (seconds <= 5)
                draw_high_scores();
            else
                draw_story();
            
            if (seconds >= 10)
                seconds = 0;
            
            if (seconds%2 && manage->credits == 0)
                draw_big_string(200, 520, "INSERT COIN", strlen("INSERT COIN"));
            else
            {
                draw_big_color_char(216, 504, manage->credits + 48); /* yup it's ugly */
                draw_big_yellow_string(232, 520, " CREDITS", strlen(" CREDITS"));
            }
            
            draw_string(236, 680, "(c) OSAC 2016", strlen("(c) OSAC 2016"));
            
            al_flip_display();
        }
    }
    
    
    
    al_destroy_bitmap(title_bitmap);
    al_stop_samples();
}


void draw_high_scores()
{
    int scores[10];
    int players[10];
    char** names;
    int i;
	char string[100] = {0};

	if (NULL == (names = malloc(10*sizeof(char*))))
	{
		fprintf(stderr, "error initializing high scores");
		exit(EXIT_FAILURE);
	}

	for (i = 0; i < 10; i++)
	{
		if (NULL == (names[i] = malloc(4*sizeof(char))))
		{
			fprintf(stderr, "error initializing high scores");
			exit(EXIT_FAILURE);
		}
	}
    
    draw_big_string(168, 240, "Top 10 Soldiers", strlen("Top 10 Soldiers"));
	load_high_score(scores, players, names);
    
    for (i = 0; i < 10; i++)
    {
	  sprintf(string, "%d %s %d", players[i], names[i], scores[i]);
	  draw_big_string(180, 272+i*20, string, strlen(string));
    }
}

void draw_story()
{
    draw_string(90, 300, "The year is 2640. A rebel base has been assieged.", strlen("The year is 2640. A rebel base has been assieged."));
    draw_string(90, 320, "A couple of brave soldiers managed to escape to", strlen("A couple of brave soldiers managed to escape to"));
    draw_string(90, 340, "find help.", strlen("find help."));
    draw_string(90, 360, "Their mission seems simple: join the closest rebel", strlen("Their mission seems simple: join the closest rebel"));
    draw_string(90, 380, "planet, Opera.", strlen("planet, Opera."));
    draw_string(90, 400, "But will they succeed? Enemies are everywhere...", strlen("But will they succeed? Enemies are everywhere..."));
}
