#ifndef H_OPERA
#define H_OPERA

#include <stdio.h>
#include <allegro5/allegro.h>

#include "image.h"


#ifndef WAIT
#define WAIT 35000
#endif /* WAIT */

#define SCOREFILE "xsoldier.scores"

#define MAX_STAGE 8
#define SHOT_TIMING 100
#define MAX_LEVEL 80

#define FIRST1UP 200000
#define EVERY1UP 200000

/* attribute mask */
#define MPLAYER (1L<<0)
#define MPSHOT  (1L<<1)
#define MENEMY  (1L<<2)
#define MESHOT  (1L<<3)
#define MITEM   (1L<<4)

#ifndef true
#define true 1
#endif /* true */
#ifndef false
#define false 0
#endif /* false */

/* death flag */
typedef enum
{
    NoneDel,
    NullDel,
    ZakoDel,
    BossDel
}
DelAtt;

/* basic data of object */
typedef struct
{
    int used;

    int hit_att;
    int hit_mask;
    int width, height;
    int harfw, harfh;

    /* shoot if shot_time >= ShotTiming */
    int start_time;
    int shot_time;

    int kill;
    int hp;
    int attack;
    int point;
    DelAtt enemy_att;

    int x, y;
    int oldx, oldy;
    int inertx,inerty;
    int angle;
    int speed;
    int cnt[16];

    int image;
    int show_damage_time;
    int not_shooting_time;
    int should_act;
}
ObjData;

/* graphic data of object */
typedef struct
{
    ALLEGRO_BITMAP** image;

    int width, height;
    int harfw, harfh;
    
    int sprites;
}
GrpData;

/* prototype of object data */
typedef struct
{
    ObjData data;
    GrpData grp;

    DelAtt (*action)(ObjData *my);
    DelAtt (*hit)(ObjData *my, ObjData *your);
    void (*realize)(ObjData *my, GrpData *grp);
}
CharObj;

/* table of objects */
typedef struct
{
    CharObj **player;
    CharObj **enemy;

    CharObj new_obj;

    int player_max;
    int player_num;
    int enemy_max;
    int enemy_num;
    
    int credits;
    int number_of_players;

    /* frequently used objects */
    CharObj enemy_shot;
    CharObj bomb;
    CharObj large_bomb;

    int level;
    int stage;
    int loop;
    int appear;

    int stage_enemy;
    int stage_shot_down;
    int zako_app;
    int boss_app;
    int boss_kill;

    int boss_time;
    int flag_maxlevel;
    int start_power;
    int show_shoot_down;
    int flag_nopausemessage;
    int program_should_quit;
}
CharManage;


typedef struct
{
    char name[4];
    int score;
    int stage;
    int loop;
}
Record;

typedef struct
{
    Record rec[11];
    int ships;
    int percent;
    int next;
}
PlayerData;

#endif /* H_OPERA */
