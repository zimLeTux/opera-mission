#include <allegro5/allegro.h>

#include "player.h"
#include "extern.h"
#include "opera.h"
#include "keys.h"
#include "callback.h"
#include "game.h"
#include "sin.h"
#include "enemy.h"
#include "constants.h"

/* TODO: simplifier new_player !! */
void new_player(int p, int x, int y)
{
    printf("p = %d\n", p);
    fflush(stdout);
    manage->player[p]->data.hit_att = MPLAYER;
    manage->player[p]->data.hit_mask = MENEMY | MESHOT | MITEM;

    manage->player[p]->data.used = true;
    debug("DATA USED SET!");
    manage->player[p]->data.kill = false;
    manage->player[p]->data.x = x;
    manage->player[p]->data.y = y;
    manage->player[p]->data.speed = 15;
    manage->player[p]->data.attack = 0;
    manage->player[p]->data.width = 16;
    manage->player[p]->data.height = 16;
    manage->player[p]->data.harfw = manage->player[p]->data.width/2;
    manage->player[p]->data.harfh = manage->player[p]->data.height/2;
    manage->player[p]->data.image = 0;

    manage->player[p]->data.cnt[0] = 0; /* auto shot counter */
    manage->player[p]->data.cnt[1] = 0; /* image counter */
    manage->player[p]->data.cnt[2] = 60; /* immutable counter */
    manage->player[p]->data.cnt[3] = 0; /* death coounter */
    manage->player[p]->data.cnt[4] = false; /* transparent flag */
    manage->player[p]->data.cnt[5] = 1; /* weapon counter */
    manage->player[p]->data.cnt[6] = 0; /* power counter */

    

    manage->player[p]->grp.width = 32;
    manage->player[p]->grp.height = 32;
    manage->player[p]->grp.harfw = manage->player[p]->grp.width/2;
    manage->player[p]->grp.harfh = manage->player[p]->grp.height/2;

    if (0 == p)
    {
        manage->player[p]->grp.image = &player1_bitmap;
        manage->player[p]->action = Player1Action;
    }
    else
    {
        manage->player[p]->grp.image = &player2_bitmap;
        manage->player[p]->action = Player2Action;
    }
    
    manage->player[p]->hit = PlayerHit;
    manage->player[p]->realize = DrawImage;
    manage->player[p]->data.not_shooting_time = 0;

    manage->player_num++;
}


void restart_player(int p, int x, int y)
{
    /* x, y : coordinates
     * p : player to reset
     */
    manage->player[p]->data.hit_att = MPLAYER;
    manage->player[p]->data.hit_mask = MENEMY | MESHOT | MITEM;
    manage->player[p]->data.used = true;
    manage->player[p]->data.kill = false;
    manage->player[p]->data.x = x;
    manage->player[p]->data.y = y;

    manage->player[p]->data.cnt[0] = 0; /* auto shot counter */
    manage->player[p]->data.cnt[1] = 0; /* image counter */
    manage->player[p]->data.cnt[2] = 60; /* immutable counter */
    manage->player[p]->data.cnt[3] = 0; /* death counter */
    manage->player[p]->data.cnt[4] = false; /* transparent flag */
    manage->player[p]->data.not_shooting_time = 0;

    manage->player_num++;
}

void player_do_action(ObjData* self)
{
    /* choose image */
    /* FIXME ? */
    if (self->cnt[1] > 0)
        self->cnt[1]--;
    else if (self->cnt[1] < 0)
        self->cnt[1]++;

    if (self->cnt[1] > 10)
        self->image = 4;
    else if (self->cnt[1] > 0)
        self->image = 3;
    else if (self->cnt[1] < -10)
        self->image = 2;
    else if (self->cnt[1] < 0)
        self->image = 1;
    else
        self->image = 0;

    if (self->cnt[2] != 0)
    {
        if (self->cnt[4] == false)
        {
            self->image = 5;
            self->cnt[4] = true;
        }
        else
            self->cnt[4] = false;
    }

    /* boundary check for moving */
    if (self->x - 16 < 0)
        self->x = 16;
    else if (self->x + 16 > fieldw)
        self->x = fieldw - 16;
    if (self->y - 16 < 0)
        self->y = 16;
    else if (self->y + 16 > fieldh)
        self->y = fieldh - 16;

    if (self->cnt[2] > 0)
        self->cnt[2]--;
}

void player_do_shoot(ObjData* self)
{
    self->not_shooting_time = 5;
    if (self->cnt[0] == 0)
    {
        switch (self->cnt[5])
        {
        case 1:
            /* how often you can shoot */
            if (self->cnt[6] >= 30)
                self->cnt[0] = 2;
            else
                self->cnt[0] = 3;

            if (self->cnt[6] >= 20)
            {
                player_shot1(self->x-10,self->y,35,90, self->cnt[6]);
                player_shot1(self->x+10,self->y,35,90, self->cnt[6]);
            }
            else if (self->cnt[6] >= 10)
                player_shot1(self->x, self->y,35,90, self->cnt[6]);

            if (self->cnt[6] >= 15)
            {
                player_shot1(self->x-40,self->y+10,35,270, self->cnt[6]);
                player_shot1(self->x-10,self->y,35,270, self->cnt[6]);
                player_shot1(self->x+10,self->y,35,270, self->cnt[6]);
                player_shot1(self->x+40,self->y+10,35,270, self->cnt[6]);
            }
            else if (self->cnt[6] >= 5)
            {
                player_shot1(self->x-25,self->y+10,35,270, self->cnt[6]);
                player_shot1(self->x, self->y,35,270, self->cnt[6]);
                player_shot1(self->x+25, self->y+10,35,270, self->cnt[6]);
            }
            else
            {
                player_shot1(self->x-10,self->y,35,270, self->cnt[6]);
                player_shot1(self->x+10,self->y,35,270, self->cnt[6]);
            }

            break;
        case 2:
            /* how often you can shoot */
            if (self->cnt[6] >= 25)
                self->cnt[0] = 1;
            else if (self->cnt[6] >= 19)
                self->cnt[0] = 2;
            else
                self->cnt[0] = 3;
            player_shot2(self->x,self->y,25,255);
            player_shot2(self->x,self->y,25,270);
            player_shot2(self->x,self->y,25,285);
            if (self->cnt[6] >= 15)
            {
                player_shot2(self->x,self->y,25,0);
                player_shot2(self->x,self->y,25,180);
            }
            if (self->cnt[6] >= 11)
            {
                player_shot2(self->x,self->y,25,225);
                player_shot2(self->x,self->y,25,315);
            }
            if (self->cnt[6] >= 7)
            {
                player_shot2(self->x,self->y,25,60);
                player_shot2(self->x,self->y,25,120);
            }
            if (self->cnt[6] >= 3)
            {
                player_shot2(self->x,self->y,25,240);
                player_shot2(self->x,self->y,25,300);
            }

            break;
        case 3:
            /* how often you can shoot */
            if (self->cnt[6] >= 30)
                self->cnt[0] = 3;
            else
                self->cnt[0] = 4;
            if (self->cnt[6] >= 10)
            {
                player_shot3(self->x,self->y-10,0, self->cnt[6]);
                player_shot3(self->x-10,self->y-10,-1, self->cnt[6]);
                player_shot3(self->x+10,self->y-10,1, self->cnt[6]);
            }
            else if (self->cnt[6] >= 5)
            {
                player_shot3(self->x-10,self->y-10,0, self->cnt[6]);
                player_shot3(self->x+10,self->y-10,0, self->cnt[6]);
            }
            else
                player_shot3(self->x,self->y-10,0, self->cnt[6]);
            break;
        }
    }
    else
    {
        self->cnt[0]--;
    }
}

DelAtt Player1Action(ObjData *self)
{
    /* you can change speed at any time */
    if (action_keys_flags & P1_FASTER)
    {
        if (self->speed < 30)
            self->speed += 3;
    }
    if (action_keys_flags & P1_SLOWER)
    {
        if (self->speed > 6)
            self->speed -= 3;
    }

    /* your action is over if you are dead */
    if (self->kill == true)
    {
        self->cnt[3]++;
        if (self->cnt[3] == 30)
            return NullDel;

        new_large_bomb(self->x+rand()%20-10,self->y+rand()%20-10);
        return NoneDel;
    }

    /* move */
    if (action_keys_flags & P1_UP)
        self->y -= self->speed;
    if (action_keys_flags & P1_DOWN)
        self->y += self->speed;
    if (action_keys_flags & P1_LEFT)
    {
        self->x -= self->speed;
        /*printf("...%d\n", self->x);
        fflush(stdout);*/
        self->cnt[1] -= 2;
    }
    if (action_keys_flags & P1_RIGHT)
    {
        self->x += self->speed;
        /*printf("...%d\n", self->x);
        fflush(stdout);*/
        self->cnt[1] += 2;
    }

    player_do_action(self);

    if (action_keys_flags & P1_SHOOT)
    {
        player_do_shoot(self);
    }
    else
    {
        if (self->not_shooting_time <= 0)
        {
            /* releasing the shoot button gives you a score */
            player->rec[0].score += (manage->enemy_num) * 3;
        }
        else
            (self->not_shooting_time)--;

        self->cnt[0] = 0;
    }

    return NoneDel;
}

DelAtt Player2Action(ObjData *self)
{
    /* you can change speed at any time */
    if (action_keys_flags & P2_FASTER)
    {
        if (self->speed < 30)
            self->speed += 3;
    }
    if (action_keys_flags & P2_SLOWER)
    {
        if (self->speed > 6)
            self->speed -= 3;
    }

    /* your action is over if you are dead */
    if (self->kill == true)
    {
        self->cnt[3]++;
        if (self->cnt[3] == 30)
            return NullDel;

        new_large_bomb(self->x+rand()%20-10,self->y+rand()%20-10);
        return NoneDel;
    }

    /* move */
    if (action_keys_flags & P2_UP)
        self->y -= self->speed;
    if (action_keys_flags & P2_DOWN)
        self->y += self->speed;
    if (action_keys_flags & P2_LEFT)
    {
        self->x -= self->speed;
        self->cnt[1] -= 2;
    }
    if (action_keys_flags & P2_RIGHT)
    {
        self->x += self->speed;
        self->cnt[1] += 2;
    }

    player_do_action(self);

    if (action_keys_flags & P2_SHOOT)
    {
        player_do_shoot(self);
    }
    else
    {
        if (self->not_shooting_time <= 0)
        {
            /* releasing the shoot button gives you a score */
            player->rec[0].score += (manage->enemy_num) * 3;
        }
        else
            (self->not_shooting_time)--;

        self->cnt[0] = 0;
    }

    return NoneDel;
}

DelAtt PlayerHit(ObjData *self, ObjData *other)
{
    if (other->hit_att == MITEM)
    {
        if (other->cnt[0] == 0)
            self->cnt[6] += 2;
        else
        {
            self->cnt[6]++;
            self->cnt[5] = other->cnt[0];
        }

        for(; self->cnt[6] > 45; (self->cnt[6])--)
        {
            player->rec[0].score += 1000 * manage->loop;
            (manage->level)++;
        }

        if (manage->level > MAX_LEVEL)
            manage->level = MAX_LEVEL;
        if (manage->level < 0)
            manage->level = 0;

        if (manage->flag_maxlevel == true)
            manage->level = MAX_LEVEL;

        return NoneDel;
    }

    if (self->cnt[2] == 0)
    {
        self->kill = true;
        self->image = 5;
        manage->level -= 5;
        if (manage->level > MAX_LEVEL)
            manage->level = MAX_LEVEL;
        if (manage->level < 0)
            manage->level = 0;

        if (manage->flag_maxlevel == true)
            manage->level = MAX_LEVEL;
    }
    return NoneDel;
}


/* player shot */
void player_shot1(int x, int y, int speed, int angle, int attack)
{
    manage->new_obj.data.hit_att = MPSHOT;
    manage->new_obj.data.hit_mask = MENEMY;

    manage->new_obj.data.x = x;
    manage->new_obj.data.y = y;
    if (attack >= 25)
        manage->new_obj.data.attack = 3;
    else
        manage->new_obj.data.attack = 2;

    manage->new_obj.data.speed = speed;
    manage->new_obj.data.angle = angle;
    manage->new_obj.data.enemy_att = NullDel;
    manage->new_obj.data.width = 10;
    manage->new_obj.data.height = 26;

    manage->new_obj.data.cnt[0] = x << 8;
    manage->new_obj.data.cnt[1] = y << 8;
    manage->new_obj.data.cnt[2] = icos(angle);
    manage->new_obj.data.cnt[3] = isin(angle);

    manage->new_obj.data.cnt[4] = 0;

    manage->new_obj.grp.image = &pshot1_bitmap;
    manage->new_obj.grp.sprites = 2;


    new_obj(MPSHOT, PlayerShotAct1, PlayerShotHit1, DrawImage);
}


void player_shot2(int x, int y, int speed, int angle)
{
    manage->new_obj.data.hit_att = MPSHOT;
    manage->new_obj.data.hit_mask = MENEMY;

    manage->new_obj.data.x = x;
    manage->new_obj.data.y = y;
    manage->new_obj.data.attack = 1;
    manage->new_obj.data.speed = speed;
    manage->new_obj.data.angle = angle;
    manage->new_obj.data.enemy_att = NullDel;
    manage->new_obj.data.width = 12;
    manage->new_obj.data.height = 16;

    manage->new_obj.data.cnt[0] = x << 8;
    manage->new_obj.data.cnt[1] = y << 8;
    manage->new_obj.data.cnt[2] = icos(angle);
    manage->new_obj.data.cnt[3] = isin(angle);

    manage->new_obj.data.cnt[4] = 0;

    manage->new_obj.grp.image = &pshot2_bitmap;
    manage->new_obj.grp.sprites = 2;


    new_obj(MPSHOT,PlayerShotAct1,PlayerShotHit1,DrawImage);
}

DelAtt PlayerShotAct1(ObjData *self)
{
    if (self->cnt[4] >= 1)
        return NullDel;

    /* 2^8 = 256 */
    self->cnt[0] += self->cnt[2]*self->speed;
    self->x = self->cnt[0] / 256;
    self->cnt[1] += self->cnt[3]*self->speed;
    self->y = self->cnt[1] / 256;

    if ((self->x<0) || (self->x>fieldw))
        return NullDel;
    if ((self->y<0) || (self->y>fieldh))
        return NullDel;

    return NoneDel;
}

DelAtt PlayerShotHit1(ObjData *self, ObjData *your)
{
    if (self->cnt[4] >= 1)
        return NullDel;

    self->cnt[4]++;
    self->hit_mask = 0;
    self->image = 1;
    return NoneDel;
}

void player_shot3(int x, int y, int inertx, int attack)
{
    manage->new_obj.data.hit_att = MPSHOT;
    manage->new_obj.data.hit_mask = MENEMY;

    manage->new_obj.data.x = x;
    manage->new_obj.data.y = y;
    if (attack >= 22)
        manage->new_obj.data.attack = 5;
    else if (attack >= 16)
        manage->new_obj.data.attack = 4;
    else
        manage->new_obj.data.attack = 3;
    manage->new_obj.data.enemy_att = NullDel;
    manage->new_obj.data.width = 8;
    manage->new_obj.data.height = 16;

    manage->new_obj.data.inertx = inertx;
    manage->new_obj.data.inerty = 5;
    manage->new_obj.data.cnt[4] = 0;

    manage->new_obj.grp.image = &pshot3_bitmap;
    manage->new_obj.grp.sprites = 3;

    new_obj(MPSHOT, PlayerShotAct3, PlayerShotHit3, DrawImage);
}

DelAtt PlayerShotAct3(ObjData *self)
{
    if (self->cnt[4] != 0)
    {
        /* delete the explosion */
        if (self->cnt[4] >= 4)
            return NullDel;
        /* no longer checks collision */
        if (self->cnt[4] == 3)
            /*
            self->attack = 0;
            */
            self->hit_mask = 0;
        if (self->cnt[4] == 1)
            self->image = 2;
        self->cnt[4]++;
    }

    /* have not exploded yet */
    if (self->cnt[4] == 0)
    {
        self->inerty++;
        self->x += self->inertx;
        self->y -= self->inerty;
    }

    if ((self->x<0) || (self->x>fieldw))
        return NullDel;
    if ((self->y<0) || (self->y>fieldh))
        return NullDel;

    return NoneDel;
}

DelAtt PlayerShotHit3(ObjData *self, ObjData *your)
{
    if (self->cnt[4] >= 4)
        return NullDel;
    else if (self->cnt[4] == 0)
        /* explode */
    {
        self->attack = 1;

        self->width = 80;
        self->harfw = self->width / 2;

        self->height = 80;
        self->harfh = self->height / 2;

        self->cnt[4]++;
        self->image = 1;
        return NoneDel;
    }
    return NoneDel;
}


void player_lose_power(int p)
{
    int i = 0;

    if (manage->player[p]->data.cnt[6] > 30)
        manage->player[p]->data.cnt[6] = 30;

    if (manage->player[p]->data.cnt[6] < manage->start_power + 10)
        return;
    else if (manage->player[p]->data.cnt[6] < manage->start_power + 25)
    {
        manage->player[p]->data.cnt[6] -= 5;
        i = 3;
    }
    else
    {
        manage->player[p]->data.cnt[6] -= 10;
        i = 6;
    }

    if (manage->player[p]->data.cnt[6] < 0)
        /* should not happen */
        manage->player[p]->data.cnt[6] = 0;

    for (; i >= 1; i--)
        /* the Y coordinate is intentionally subtracted */
        NewEnemy10(manage->player[p]->data.x + rand() % 50 - 25,
                   manage->player[p]->data.y - rand() % 20 - 10);
}




