#ifndef H_IMAGE
#define H_IMAGE

#include <allegro5/allegro.h>

void split_bitmap(ALLEGRO_BITMAP* input, ALLEGRO_BITMAP*** output, int nsplit);

#endif /* H_IMAGE */
