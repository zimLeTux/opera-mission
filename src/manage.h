#ifndef H_MANAGE
#define H_MANAGE

#include "opera.h"

CharManage* new_manage(int player_max, int enemy_max);
void clear_manage(CharManage *manage_temp);
void reset_manage(CharManage *manage_temp);
void delete_manage(CharManage *del);

int new_obj(int mask,
            DelAtt (*action)(ObjData *my),
            DelAtt (*hit)(ObjData *my, ObjData *your),
            void (*realize)(ObjData *my, GrpData *grp));
void del_obj(CharObj *del);
PlayerData *new_player_data(void);
void clear_enemy_shot_manage(CharManage *manage_temp);

#endif /* H_MANAGE */
