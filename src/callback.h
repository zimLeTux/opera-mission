#ifndef H_CALLBACK
#define H_CALLBACK

#include "opera.h"

DelAtt NullAct(ObjData *self);

DelAtt NullHit(ObjData *self, ObjData *your);
DelAtt NullDelHit(ObjData *self, ObjData *your);
DelAtt DeleteHit(ObjData *self, ObjData *your);
DelAtt DamageHit(ObjData *self, ObjData *your);
DelAtt LargeDamageHit(ObjData *self, ObjData *your);

DelAtt BombAct(ObjData* self);
DelAtt EnemyShotAct(ObjData *self);

void NullReal(ObjData *self, GrpData *grp);
void DrawRec(ObjData *self, GrpData *grp);
void DrawImage(ObjData *self, GrpData *grp);

#endif /* H_CALLBACK */

