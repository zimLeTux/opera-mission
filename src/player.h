#ifndef H_PLAYER
#define H_PLAYER

#include "opera.h"

extern void new_player(int p, int x, int y);
extern void restart_player(int p, int x, int y);

extern DelAtt Player1Action(ObjData *self);
extern DelAtt Player2Action(ObjData *self);
extern DelAtt PlayerHit(ObjData *my, ObjData *your);

extern void player_shot1(int x, int y, int speed, int angle, int attack);
extern void player_shot2(int x, int y, int speed, int angle);
extern DelAtt PlayerShotAct1(ObjData *my);
extern DelAtt PlayerShotHit1(ObjData *my, ObjData *your);
extern void player_shot3(int x, int y, int inertX, int attack);
extern DelAtt PlayerShotAct3(ObjData *my);
extern DelAtt PlayerShotHit3(ObjData *my, ObjData *your);

extern void player_lose_power(int p);

#endif
