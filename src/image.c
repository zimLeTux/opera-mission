#include <stdlib.h>
#include <stdio.h>

#include <allegro5/allegro.h>

#include "image.h"
#include "extern.h"

void split_bitmap(ALLEGRO_BITMAP* input, ALLEGRO_BITMAP*** output, int nsplit)
{
    int width = 0;
    int height = 0;
    int i;
    
    if (input == NULL)
    {
        fprintf(stderr, "split_bitmap : can't split nothingness!\n");
        exit(EXIT_FAILURE);
    }
    
    debug("     _ malloc output bitmap");
    (*output) = (ALLEGRO_BITMAP**)malloc(nsplit*sizeof(ALLEGRO_BITMAP*));
    
    debug("     _ init bitmap infos");
    width = al_get_bitmap_width(input);
    height = al_get_bitmap_height(input)/nsplit;
    
    debug("     _ split bitmap");
    for (i = 0; i < nsplit; i++)
    {
        (*output)[i] = al_create_sub_bitmap(input, 0, i*height, width, height);
    }
    debug("     _ done!");
}
