#ifndef H_GAME
#define H_GAME

int mainloop();

void new_bomb(int x, int y);
void new_large_bomb(int x, int y);
int get_direction(int mx, int my, int sx, int sy);

#endif /* H_GAME */
