#include <stdio.h>
#include <stdlib.h>

#include <allegro5/allegro_audio.h>

#include "sound.h"
#include "extern.h"

ALLEGRO_SAMPLE* intro_sample;
ALLEGRO_SAMPLE* level1_sample;


void load_sounds()
{
    debug("loading sounds");
    intro_sample = al_load_sample("data/music/intro.ogg");
	level1_sample = al_load_sample("data/music/level1.ogg");
}
