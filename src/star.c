#include <stdio.h>
#include <stdlib.h>

#include <allegro5/allegro.h>
#include <allegro5/allegro_image.h>

#include "star.h"
#include "image.h"
#include "extern.h"

#define MAXSTAR 4

typedef struct point_
{
    /* coordinates of the star */
    int x, y;
    /* pattern number of the star */
    int cpat;
}
Point;

typedef struct star_
{
    Point* point;
    ALLEGRO_BITMAP** bitmaps;
    unsigned int sw, sh;
    int pattern;
    int speed;
    int nstar;
}
Star;

static Star star[MAXSTAR];
static int cust;
static unsigned int dw, dh;

void init_star_module(unsigned int w, unsigned int h)
{
    dw = w;
    dh = h;
    cust = 0;
}

int create_star(const char* filename, int pattern, int speed, int nstar)
{
    ALLEGRO_BITMAP* tmp;
    Star* s = &(star[cust]);
    int i;

    if (cust >= MAXSTAR)
    {
        fprintf(stderr, "create_star: can't create star!\n");
        fflush(stderr);
        return -1;
    }

    debug("   . load temporary bitmap");
    tmp = al_load_bitmap(filename);
    if (tmp == NULL)
    {
        fprintf(stderr, "create_star : unable to load '%s'\n", filename);
        exit(EXIT_FAILURE);
    }

    debug("   . split temporary bitmap");
    split_bitmap(tmp, &(s->bitmaps), pattern);

    debug("   . fill star infos");
    s->pattern = pattern;
    s->speed = speed;
    s->nstar = nstar;

    debug("   . malloc point");
    s->point = (Point*)malloc(nstar*sizeof(Point));

    debug("   . get star w and h");
    s->sw = dw + al_get_bitmap_width(s->bitmaps[0]);
    s->sh = dh + al_get_bitmap_height(s->bitmaps[0]);

    debug("   . randomize star");
    for (i = 0; i < nstar; i++)
    {
        s->point[i].cpat = rand() % pattern;
        s->point[i].x = rand() % s->sw;
        s->point[i].y = rand() % s->sh;
    }

    debug("   . star finished");
    return cust++;
}

void draw_star(int id)
{
    static ALLEGRO_BITMAP* bitmap;
    static Star* self;
    static Point* p;
    int i;

    self = &(star[id]);
    for (i = 0; i < self->nstar; i++)
    {
        p = &(self->point[i]);
        bitmap = self->bitmaps[(p->cpat)++];
        al_draw_bitmap(bitmap, (float)(p->x - al_get_bitmap_width(bitmap)), (float)(p->y - al_get_bitmap_height(bitmap)), 0);
        if (p->cpat >= self->pattern) p->cpat = 0;
    }

    for (i = 0; i < self->nstar; i++)
    {
        p = &(self->point[i]);
        p->y += self->speed;
        if (p->y < 0 || p->y > self->sh)
        {
            p->x = rand() % self->sw;
            p->y = (self->speed > 0)?0:self->sh;
        }
    }
}

void change_star_parameter(int id, int speed)
{
    /* FIXME: old code, not that clean... */
    static Star *self;

    self = &(star[id]);
    self->speed = speed;
}

int delete_all_stars()
{
    /* TODO: NOT IMPLEMENTD! */
    return 0;
}

