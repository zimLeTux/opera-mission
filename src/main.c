#include <stdio.h>
#include <stdlib.h>

#include <allegro5/allegro.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_audio.h>
#include <allegro5/allegro_acodec.h>

#include "extern.h"
#include "constants.h"
#include "graphics.h"
#include "sound.h"
#include "manage.h"
#include "opera.h"
#include "keys.h"
#include "opening.h"
#include "star.h"
#include "game.h"


static void init();

ALLEGRO_DISPLAY* display;
ALLEGRO_EVENT_QUEUE* event_queue;
ALLEGRO_TIMER* fps;
CharManage* manage;
PlayerData* player;

int main(int argc, char *argv[])
{
    /*ALLEGRO_EVENT event;
    int key;*/
    
    
    debug("start program");
    init();
    
    debug("init manage");
    manage = new_manage(256, 256);
    player = new_player_data();
    init_star_module(WINDOW_WIDTH, WINDOW_HEIGHT);
    
    debug("entering game loop");
    while (!(manage->program_should_quit))
    {
        player->rec[0].score = 0;
        player->ships = START_SHIPS;
        player->next = FIRST1UP;
        manage->loop = 1;
        manage->stage = 1;
        
        debug("launch opening");
        opening();
        
        while (1)
        {
            /*reset_manage(manage);
             Move to opening() ?
             FIXME??*/

            if (mainloop() == 0)
                /* game over */
                break;

            /*ending();*/
            manage->loop++;
            manage->stage = 1;
            if (manage->loop > 3)
                break;
        }
        
        /* put highscores here ! */
    }
    
    al_uninstall_audio();
    
    return EXIT_SUCCESS;
}

static void init()
{
    debug("start init");
    if (!al_init())
    {
        fprintf(stderr, "failed to initialize allegro!\n");
        exit(EXIT_FAILURE);
    }

    debug("init image addon");
    al_init_image_addon();

    debug("create display");
    display = al_create_display(WINDOW_WIDTH, WINDOW_HEIGHT);

    if (!display)
    {
        fprintf(stderr, "failed to create display!\n");
        exit(EXIT_FAILURE);
    }

    debug("install keyboard");
    al_install_keyboard();

    debug("create event queue");
    event_queue = al_create_event_queue();
    if (!event_queue)
    {
        fprintf(stderr, "failed to create event_queue!\n");
        al_destroy_display(display);
        exit(EXIT_FAILURE);
    }

    al_register_event_source(event_queue, al_get_display_event_source(display));
    al_register_event_source(event_queue, al_get_keyboard_event_source());
    
    debug("create timer");
    fps = al_create_timer(1.0f/50.0f);
    if (!fps)
    {
        fprintf(stderr, "failed to create timer!\n");
        al_destroy_event_queue(event_queue);
        al_destroy_display(display);
        exit(EXIT_FAILURE);
    }
    
    al_register_event_source(event_queue, al_get_timer_event_source(fps));
    
    debug("init sound addon");
    al_install_audio();
    al_init_acodec_addon();
    al_reserve_samples(2);

    debug("init random");
    srand((unsigned)time(NULL));
    
    debug("other inits");
    fieldh = WINDOW_HEIGHT - 32;
    fieldw = WINDOW_WIDTH;
    
    debug("load stuff...");
    load_graphics();
    load_sounds();
}
