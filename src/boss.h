#ifndef H_BOSS
#define H_BOSS

#include "opera.h"

extern int NewBoss1(void);
extern DelAtt BossAct1(ObjData *self);
extern DelAtt BossHit1(ObjData *self, ObjData *your);

extern int NewBoss2(void);
extern DelAtt BossAct2(ObjData *self);

extern int NewBoss3(void);
extern DelAtt BossAct3(ObjData *self);

extern int NewBoss4(void);
extern DelAtt BossAct4(ObjData *self);

extern int NewBoss5(void);
extern DelAtt BossAct5(ObjData *self);

extern int NewBoss6(void);
extern DelAtt BossAct6(ObjData *self);

extern int NewBoss7(void);
extern DelAtt BossAct7(ObjData *self);

extern int NewBoss8(void);
extern DelAtt BossAct8(ObjData *self);
extern DelAtt BossHit8(ObjData *self, ObjData *your);

#endif /* H_BOSS */
