#include <stdio.h>
#include <stdlib.h>

#include <allegro5/allegro.h>
#include <allegro5/allegro_image.h>

#include "graphics.h"
#include "star.h"
#include "constants.h"
#include "extern.h"

ALLEGRO_BITMAP* player1_bitmap;
ALLEGRO_BITMAP* player2_bitmap;
ALLEGRO_BITMAP* pshot1_bitmap;
ALLEGRO_BITMAP* pshot2_bitmap;
ALLEGRO_BITMAP* pshot3_bitmap;
ALLEGRO_BITMAP* eshot_bitmap;
ALLEGRO_BITMAP* elaser_bitmap;
ALLEGRO_BITMAP* emissile_bitmap;
ALLEGRO_BITMAP* ebound_bitmap;
ALLEGRO_BITMAP* ering_bitmap;
ALLEGRO_BITMAP* bomb_bitmap;
ALLEGRO_BITMAP* bigbomb_bitmap;
ALLEGRO_BITMAP* enemy1_bitmap;
ALLEGRO_BITMAP* enemy2_bitmap;
ALLEGRO_BITMAP* enemy3_bitmap;
ALLEGRO_BITMAP* enemy4_bitmap;
ALLEGRO_BITMAP* enemy5_bitmap;
ALLEGRO_BITMAP* enemy6_bitmap;
ALLEGRO_BITMAP* enemy7_bitmap;
ALLEGRO_BITMAP* boss1_bitmap;
ALLEGRO_BITMAP* boss2_bitmap;
ALLEGRO_BITMAP* boss3_bitmap;
ALLEGRO_BITMAP* boss4_bitmap;
ALLEGRO_BITMAP* boss5_bitmap;
ALLEGRO_BITMAP* boss6_bitmap;
ALLEGRO_BITMAP* boss7_bitmap;
ALLEGRO_BITMAP* item_bitmap;

ALLEGRO_BITMAP* font_bitmap_tmp[6];
ALLEGRO_BITMAP* big_font_bitmap_tmp[6];
ALLEGRO_BITMAP* big_font_color_bitmap_tmp[6];
ALLEGRO_BITMAP* big_font_yellow_bitmap_tmp[6];
ALLEGRO_BITMAP* font8_bitmap[6][16];
ALLEGRO_BITMAP* font16_bitmap[6][16];
ALLEGRO_BITMAP* font16_color_bitmap[6][16];
ALLEGRO_BITMAP* font16_yellow_bitmap[6][16];

int star_ptn1;
int star_ptn2;


void load_graphics()
{
    int i;
    int j;
    
    debug("loading graphics...");

    /* loading pixmaps */
    debug(" - players");
    player1_bitmap = al_load_bitmap("data/pixmap/player1.tga");
    player2_bitmap = al_load_bitmap("data/pixmap/player2.tga");
    
    debug(" - shots");
    pshot1_bitmap = al_load_bitmap("data/pixmap/pshot1.tga");
    pshot2_bitmap = al_load_bitmap("data/pixmap/pshot2.tga");
    pshot3_bitmap = al_load_bitmap("data/pixmap/pshot3.tga");
    
    debug(" - enemy shots");
    eshot_bitmap = al_load_bitmap("data/pixmap/eshot.tga");
    elaser_bitmap = al_load_bitmap("data/pixmap/elaser.tga");
    emissile_bitmap = al_load_bitmap("data/pixmap/emissile.tga");
    ebound_bitmap = al_load_bitmap("data/pixmap/ebound.tga");
    ering_bitmap = al_load_bitmap("data/pixmap/ering.tga");
    
    debug(" - explosions");
    bomb_bitmap = al_load_bitmap("data/pixmap/bomb.tga");
    bigbomb_bitmap = al_load_bitmap("data/pixmap/bigbomb.tga");
    
    debug(" - enemies");
    enemy1_bitmap = al_load_bitmap("data/pixmap/enemy1.tga");
    enemy2_bitmap = al_load_bitmap("data/pixmap/enemy2.tga");
    enemy3_bitmap = al_load_bitmap("data/pixmap/enemy3.tga");
    enemy4_bitmap = al_load_bitmap("data/pixmap/enemy4.tga");
    enemy5_bitmap = al_load_bitmap("data/pixmap/enemy5.tga");
    enemy6_bitmap = al_load_bitmap("data/pixmap/enemy6.tga");
    enemy7_bitmap = al_load_bitmap("data/pixmap/enemy7.tga");
    
    debug(" - bosses");
    boss1_bitmap = al_load_bitmap("data/pixmap/boss1.tga");
    boss2_bitmap = al_load_bitmap("data/pixmap/boss2.tga");
    boss3_bitmap = al_load_bitmap("data/pixmap/boss3.tga");
    boss4_bitmap = al_load_bitmap("data/pixmap/boss4.tga");
    boss5_bitmap = al_load_bitmap("data/pixmap/boss5.tga");
    boss6_bitmap = al_load_bitmap("data/pixmap/boss6.tga");
    boss7_bitmap = al_load_bitmap("data/pixmap/boss7.tga");
    
    debug(" - items");
    item_bitmap = al_load_bitmap("data/pixmap/item.tga");
    
    debug("init stars");
    init_star_module(WINDOW_WIDTH, WINDOW_HEIGHT);
    debug(" - create stars");
    star_ptn1 = create_star("data/pixmap/star1.tga", 4, 5, 5);
    star_ptn2 = create_star("data/pixmap/star2.tga", 4, 10, 10);
    
    /* initialize font */
    /* explanation of font images
     *  14 * 7
     *  0@P`p 14
     * !1AQaq 28
     * "2BRbr 42
     * #3CScs 56
     * $4DTdt 70
     * %5EUeu 84
     * &6FVfv 98
     * '7GWgw 112
     * (8HXhx 126
     * )9IYiy 140
     * *:JZjz 154
     * +;K[k{ 168
     * ,<L\l| 182
     * -=M]m} 196
     * .>N^n~ 210
     * /?O_o  224
     */
    debug("init font");
    font_bitmap_tmp[0] = al_load_bitmap("data/pixmap/font1.tga");
    font_bitmap_tmp[1] = al_load_bitmap("data/pixmap/font2.tga");
    font_bitmap_tmp[2] = al_load_bitmap("data/pixmap/font3.tga");
    font_bitmap_tmp[3] = al_load_bitmap("data/pixmap/font4.tga");
    font_bitmap_tmp[4] = al_load_bitmap("data/pixmap/font5.tga");
    font_bitmap_tmp[5] = al_load_bitmap("data/pixmap/font6.tga");
	big_font_bitmap_tmp[0] = al_load_bitmap("data/pixmap/big_font1.tga");
    big_font_bitmap_tmp[1] = al_load_bitmap("data/pixmap/big_font2.tga");
    big_font_bitmap_tmp[2] = al_load_bitmap("data/pixmap/big_font3.tga");
    big_font_bitmap_tmp[3] = al_load_bitmap("data/pixmap/big_font4.tga");
    big_font_bitmap_tmp[4] = al_load_bitmap("data/pixmap/big_font5.tga");
    big_font_bitmap_tmp[5] = al_load_bitmap("data/pixmap/big_font6.tga");
	big_font_color_bitmap_tmp[0] = al_load_bitmap("data/pixmap/big_font1.tga");
    big_font_color_bitmap_tmp[1] = al_load_bitmap("data/pixmap/big_font2_color.tga");
    big_font_color_bitmap_tmp[2] = al_load_bitmap("data/pixmap/big_font3.tga");
    big_font_color_bitmap_tmp[3] = al_load_bitmap("data/pixmap/big_font4.tga");
    big_font_color_bitmap_tmp[4] = al_load_bitmap("data/pixmap/big_font5_yellow.tga");
    big_font_color_bitmap_tmp[5] = al_load_bitmap("data/pixmap/big_font6_yellow.tga");
	big_font_yellow_bitmap_tmp[0] = al_load_bitmap("data/pixmap/big_font1_yellow.tga");
    big_font_yellow_bitmap_tmp[1] = al_load_bitmap("data/pixmap/big_font2_yellow.tga");
    big_font_yellow_bitmap_tmp[2] = al_load_bitmap("data/pixmap/big_font3_yellow.tga");
    big_font_yellow_bitmap_tmp[3] = al_load_bitmap("data/pixmap/big_font4_yellow.tga");
    big_font_yellow_bitmap_tmp[4] = al_load_bitmap("data/pixmap/big_font5_yellow.tga");
    big_font_yellow_bitmap_tmp[5] = al_load_bitmap("data/pixmap/big_font6_yellow.tga");
    debug("split font");
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 16; j++)
        {
            font8_bitmap[i][j] = al_create_sub_bitmap(font_bitmap_tmp[i],
													  0, j*14, 7, 14);
			font16_bitmap[i][j] = al_create_sub_bitmap(big_font_bitmap_tmp[i],
													   0, j*16, 16, 16);
			font16_color_bitmap[i][j] = al_create_sub_bitmap(big_font_color_bitmap_tmp[i],
													   0, j*16, 16, 16);
			font16_yellow_bitmap[i][j] = al_create_sub_bitmap(
															 big_font_yellow_bitmap_tmp[i],
															 0, j*16, 16, 16);
        }
    }
    debug("graphics loaded !");
}


int draw_string(int x, int y, const char *string, int length)
{
    int i;
    
    y -= 7;
    for (i = 0; (i < length) && (string[i] != '\0'); i++)
    {
        draw_char(x, y, string[i]);
        x += 8;
    }

    return 0;
}

int draw_big_string(int x, int y, const char *string, int length)
{
    int i;
    
    y -= 16;
    for (i = 0; (i < length) && (string[i] != '\0'); i++)
    {
        draw_big_char(x, y, string[i]);
        x += 16;
    }

    return 0;
}

int draw_big_yellow_string(int x, int y, const char *string, int length)
{
    int i;
    
    y -= 16;
    for (i = 0; (i < length) && (string[i] != '\0'); i++)
    {
        draw_big_yellow_char(x, y, string[i]);
        x += 16;
    }

    return 0;
}

int draw_big_color_string(int x, int y, const char *string, int length)
{
    int i;
    
    y -= 16;
    for (i = 0; (i < length) && (string[i] != '\0'); i++)
    {
        draw_big_color_char(x, y, string[i]);
        x += 16;
    }

    return 0;
}

/* return 0 on success, negative value on error */
int draw_char(int x, int y, int c)
{
    /* don't assume ASCII */
    switch (c)
    {
    case ' ':
        /* do nothing */
        return 0;
    case '!':
        al_draw_bitmap(font8_bitmap[0][1], x, y, 0);
        return 0;
    case '"':
        al_draw_bitmap(font8_bitmap[0][2], x, y, 0);
        return 0;
    case '#':
        al_draw_bitmap(font8_bitmap[0][3], x, y, 0);
        return 0;
    case '$':
        al_draw_bitmap(font8_bitmap[0][4], x, y, 0);
        return 0;
    case '%':
        al_draw_bitmap(font8_bitmap[0][5], x, y, 0);
        return 0;
    case '&':
        al_draw_bitmap(font8_bitmap[0][6], x, y, 0);
        return 0;
    case '\'':
        al_draw_bitmap(font8_bitmap[0][7], x, y, 0);
        return 0;
    case '(':
        al_draw_bitmap(font8_bitmap[0][8], x, y, 0);
        return 0;
    case ')':
        al_draw_bitmap(font8_bitmap[0][9], x, y, 0);
        return 0;
    case '*':
        al_draw_bitmap(font8_bitmap[0][10], x, y, 0);
        return 0;
    case '+':
        al_draw_bitmap(font8_bitmap[0][11], x, y, 0);
        return 0;
    case ',':
        al_draw_bitmap(font8_bitmap[0][12], x, y, 0);
        return 0;
    case '-':
        al_draw_bitmap(font8_bitmap[0][13], x, y, 0);
        return 0;
    case '.':
        al_draw_bitmap(font8_bitmap[0][14], x, y, 0);
        return 0;
    case '/':
        al_draw_bitmap(font8_bitmap[0][15], x, y, 0);
        return 0;
    case '0':
        al_draw_bitmap(font8_bitmap[1][0], x, y, 0);
        return 0;
    case '1':
        al_draw_bitmap(font8_bitmap[1][1], x, y, 0);
        return 0;
    case '2':
        al_draw_bitmap(font8_bitmap[1][2], x, y, 0);
        return 0;
    case '3':
        al_draw_bitmap(font8_bitmap[1][3], x, y, 0);
        return 0;
    case '4':
        al_draw_bitmap(font8_bitmap[1][4], x, y, 0);
        return 0;
    case '5':
        al_draw_bitmap(font8_bitmap[1][5], x, y, 0);
        return 0;
    case '6':
        al_draw_bitmap(font8_bitmap[1][6], x, y, 0);
        return 0;
    case '7':
        al_draw_bitmap(font8_bitmap[1][7], x, y, 0);
        return 0;
    case '8':
        al_draw_bitmap(font8_bitmap[1][8], x, y, 0);
        return 0;
    case '9':
        al_draw_bitmap(font8_bitmap[1][9], x, y, 0);
        return 0;
    case ':':
        al_draw_bitmap(font8_bitmap[1][10], x, y, 0);
        return 0;
    case ';':
        al_draw_bitmap(font8_bitmap[1][11], x, y, 0);
        return 0;
    case '<':
        al_draw_bitmap(font8_bitmap[1][12], x, y, 0);
        return 0;
    case '=':
        al_draw_bitmap(font8_bitmap[1][13], x, y, 0);
        return 0;
    case '>':
        al_draw_bitmap(font8_bitmap[1][14], x, y, 0);
        return 0;
    case '?':
        al_draw_bitmap(font8_bitmap[1][15], x, y, 0);
        return 0;
    case '@':
        al_draw_bitmap(font8_bitmap[2][0], x, y, 0);
        return 0;
    case 'A':
        al_draw_bitmap(font8_bitmap[2][1], x, y, 0);
        return 0;
    case 'B':
        al_draw_bitmap(font8_bitmap[2][2], x, y, 0);
        return 0;
    case 'C':
        al_draw_bitmap(font8_bitmap[2][3], x, y, 0);
        return 0;
    case 'D':
        al_draw_bitmap(font8_bitmap[2][4], x, y, 0);
        return 0;
    case 'E':
        al_draw_bitmap(font8_bitmap[2][5], x, y, 0);
        return 0;
    case 'F':
        al_draw_bitmap(font8_bitmap[2][6], x, y, 0);
        return 0;
    case 'G':
        al_draw_bitmap(font8_bitmap[2][7], x, y, 0);
        return 0;
    case 'H':
        al_draw_bitmap(font8_bitmap[2][8], x, y, 0);
        return 0;
    case 'I':
        al_draw_bitmap(font8_bitmap[2][9], x, y, 0);
        return 0;
    case 'J':
        al_draw_bitmap(font8_bitmap[2][10], x, y, 0);
        return 0;
    case 'K':
        al_draw_bitmap(font8_bitmap[2][11], x, y, 0);
        return 0;
    case 'L':
        al_draw_bitmap(font8_bitmap[2][12], x, y, 0);
        return 0;
    case 'M':
        al_draw_bitmap(font8_bitmap[2][13], x, y, 0);
        return 0;
    case 'N':
        al_draw_bitmap(font8_bitmap[2][14], x, y, 0);
        return 0;
    case 'O':
        al_draw_bitmap(font8_bitmap[2][15], x, y, 0);
        return 0;
    case 'P':
        al_draw_bitmap(font8_bitmap[3][0], x, y, 0);
        return 0;
    case 'Q':
        al_draw_bitmap(font8_bitmap[3][1], x, y, 0);
        return 0;
    case 'R':
        al_draw_bitmap(font8_bitmap[3][2], x, y, 0);
        return 0;
    case 'S':
        al_draw_bitmap(font8_bitmap[3][3], x, y, 0);
        return 0;
    case 'T':
        al_draw_bitmap(font8_bitmap[3][4], x, y, 0);
        return 0;
    case 'U':
        al_draw_bitmap(font8_bitmap[3][5], x, y, 0);
        return 0;
    case 'V':
        al_draw_bitmap(font8_bitmap[3][6], x, y, 0);
        return 0;
    case 'W':
        al_draw_bitmap(font8_bitmap[3][7], x, y, 0);
        return 0;
    case 'X':
        al_draw_bitmap(font8_bitmap[3][8], x, y, 0);
        return 0;
    case 'Y':
        al_draw_bitmap(font8_bitmap[3][9], x, y, 0);
        return 0;
    case 'Z':
        al_draw_bitmap(font8_bitmap[3][10], x, y, 0);
        return 0;
    case '[':
        al_draw_bitmap(font8_bitmap[3][11], x, y, 0);
        return 0;
    case '\\':
        al_draw_bitmap(font8_bitmap[3][12], x, y, 0);
        return 0;
    case ']':
        al_draw_bitmap(font8_bitmap[3][13], x, y, 0);
        return 0;
    case '^':
        al_draw_bitmap(font8_bitmap[3][14], x, y, 0);
        return 0;
    case '_':
        al_draw_bitmap(font8_bitmap[3][15], x, y, 0);
        return 0;
    case '`':
        al_draw_bitmap(font8_bitmap[4][0], x, y, 0);
        return 0;
    case 'a':
        al_draw_bitmap(font8_bitmap[4][1], x, y, 0);
        return 0;
    case 'b':
        al_draw_bitmap(font8_bitmap[4][2], x, y, 0);
        return 0;
    case 'c':
        al_draw_bitmap(font8_bitmap[4][3], x, y, 0);
        return 0;
    case 'd':
        al_draw_bitmap(font8_bitmap[4][4], x, y, 0);
        return 0;
    case 'e':
        al_draw_bitmap(font8_bitmap[4][5], x, y, 0);
        return 0;
    case 'f':
        al_draw_bitmap(font8_bitmap[4][6], x, y, 0);
        return 0;
    case 'g':
        al_draw_bitmap(font8_bitmap[4][7], x, y, 0);
        return 0;
    case 'h':
        al_draw_bitmap(font8_bitmap[4][8], x, y, 0);
        return 0;
    case 'i':
        al_draw_bitmap(font8_bitmap[4][9], x, y, 0);
        return 0;
    case 'j':
        al_draw_bitmap(font8_bitmap[4][10], x, y, 0);
        return 0;
    case 'k':
        al_draw_bitmap(font8_bitmap[4][11], x, y, 0);
        return 0;
    case 'l':
        al_draw_bitmap(font8_bitmap[4][12], x, y, 0);
        return 0;
    case 'm':
        al_draw_bitmap(font8_bitmap[4][13], x, y, 0);
        return 0;
    case 'n':
        al_draw_bitmap(font8_bitmap[4][14], x, y, 0);
        return 0;
    case 'o':
        al_draw_bitmap(font8_bitmap[4][15], x, y, 0);
        return 0;
    case 'p':
        al_draw_bitmap(font8_bitmap[5][0], x, y, 0);
        return 0;
    case 'q':
        al_draw_bitmap(font8_bitmap[5][1], x, y, 0);
        return 0;
    case 'r':
        al_draw_bitmap(font8_bitmap[5][2], x, y, 0);
        return 0;
    case 's':
        al_draw_bitmap(font8_bitmap[5][3], x, y, 0);
        return 0;
    case 't':
        al_draw_bitmap(font8_bitmap[5][4], x, y, 0);
        return 0;
    case 'u':
        al_draw_bitmap(font8_bitmap[5][5], x, y, 0);
        return 0;
    case 'v':
        al_draw_bitmap(font8_bitmap[5][6], x, y, 0);
        return 0;
    case 'w':
        al_draw_bitmap(font8_bitmap[5][7], x, y, 0);
        return 0;
    case 'x':
        al_draw_bitmap(font8_bitmap[5][8], x, y, 0);
        return 0;
    case 'y':
        al_draw_bitmap(font8_bitmap[5][9], x, y, 0);
        return 0;
    case 'z':
        al_draw_bitmap(font8_bitmap[5][10], x, y, 0);
        return 0;
    case '{':
        al_draw_bitmap(font8_bitmap[5][11], x, y, 0);
        return 0;
    case '|':
        al_draw_bitmap(font8_bitmap[5][12], x, y, 0);
        return 0;
    case '}':
        al_draw_bitmap(font8_bitmap[5][13], x, y, 0);
        return 0;
    case '~':
        al_draw_bitmap(font8_bitmap[5][14], x, y, 0);
        return 0;
    default:
        fprintf(stderr, "draw_char: unknown char found (\\x%x), ignoring", c);
        return -1;
    }

    /* should not reach here */
    return -2;
}

int draw_big_char(int x, int y, int c)
{
    /* don't assume ASCII */
    switch (c)
    {
    case ' ':
        /* do nothing */
        return 0;
    case '!':
        al_draw_bitmap(font16_bitmap[0][1], x, y, 0);
        return 0;
    case '"':
        al_draw_bitmap(font16_bitmap[0][2], x, y, 0);
        return 0;
    case '#':
        al_draw_bitmap(font16_bitmap[0][3], x, y, 0);
        return 0;
    case '$':
        al_draw_bitmap(font16_bitmap[0][4], x, y, 0);
        return 0;
    case '%':
        al_draw_bitmap(font16_bitmap[0][5], x, y, 0);
        return 0;
    case '&':
        al_draw_bitmap(font16_bitmap[0][6], x, y, 0);
        return 0;
    case '\'':
        al_draw_bitmap(font16_bitmap[0][7], x, y, 0);
        return 0;
    case '(':
        al_draw_bitmap(font16_bitmap[0][8], x, y, 0);
        return 0;
    case ')':
        al_draw_bitmap(font16_bitmap[0][9], x, y, 0);
        return 0;
    case '*':
        al_draw_bitmap(font16_bitmap[0][10], x, y, 0);
        return 0;
    case '+':
        al_draw_bitmap(font16_bitmap[0][11], x, y, 0);
        return 0;
    case ',':
        al_draw_bitmap(font16_bitmap[0][12], x, y, 0);
        return 0;
    case '-':
        al_draw_bitmap(font16_bitmap[0][13], x, y, 0);
        return 0;
    case '.':
        al_draw_bitmap(font16_bitmap[0][14], x, y, 0);
        return 0;
    case '/':
        al_draw_bitmap(font16_bitmap[0][15], x, y, 0);
        return 0;
    case '0':
        al_draw_bitmap(font16_bitmap[1][0], x, y, 0);
        return 0;
    case '1':
        al_draw_bitmap(font16_bitmap[1][1], x, y, 0);
        return 0;
    case '2':
        al_draw_bitmap(font16_bitmap[1][2], x, y, 0);
        return 0;
    case '3':
        al_draw_bitmap(font16_bitmap[1][3], x, y, 0);
        return 0;
    case '4':
        al_draw_bitmap(font16_bitmap[1][4], x, y, 0);
        return 0;
    case '5':
        al_draw_bitmap(font16_bitmap[1][5], x, y, 0);
        return 0;
    case '6':
        al_draw_bitmap(font16_bitmap[1][6], x, y, 0);
        return 0;
    case '7':
        al_draw_bitmap(font16_bitmap[1][7], x, y, 0);
        return 0;
    case '8':
        al_draw_bitmap(font16_bitmap[1][8], x, y, 0);
        return 0;
    case '9':
        al_draw_bitmap(font16_bitmap[1][9], x, y, 0);
        return 0;
    case ':':
        al_draw_bitmap(font16_bitmap[1][10], x, y, 0);
        return 0;
    case ';':
        al_draw_bitmap(font16_bitmap[1][11], x, y, 0);
        return 0;
    case '<':
        al_draw_bitmap(font16_bitmap[1][12], x, y, 0);
        return 0;
    case '=':
        al_draw_bitmap(font16_bitmap[1][13], x, y, 0);
        return 0;
    case '>':
        al_draw_bitmap(font16_bitmap[1][14], x, y, 0);
        return 0;
    case '?':
        al_draw_bitmap(font16_bitmap[1][15], x, y, 0);
        return 0;
    case '@':
        al_draw_bitmap(font16_bitmap[2][0], x, y, 0);
        return 0;
    case 'A':
        al_draw_bitmap(font16_bitmap[2][1], x, y, 0);
        return 0;
    case 'B':
        al_draw_bitmap(font16_bitmap[2][2], x, y, 0);
        return 0;
    case 'C':
        al_draw_bitmap(font16_bitmap[2][3], x, y, 0);
        return 0;
    case 'D':
        al_draw_bitmap(font16_bitmap[2][4], x, y, 0);
        return 0;
    case 'E':
        al_draw_bitmap(font16_bitmap[2][5], x, y, 0);
        return 0;
    case 'F':
        al_draw_bitmap(font16_bitmap[2][6], x, y, 0);
        return 0;
    case 'G':
        al_draw_bitmap(font16_bitmap[2][7], x, y, 0);
        return 0;
    case 'H':
        al_draw_bitmap(font16_bitmap[2][8], x, y, 0);
        return 0;
    case 'I':
        al_draw_bitmap(font16_bitmap[2][9], x, y, 0);
        return 0;
    case 'J':
        al_draw_bitmap(font16_bitmap[2][10], x, y, 0);
        return 0;
    case 'K':
        al_draw_bitmap(font16_bitmap[2][11], x, y, 0);
        return 0;
    case 'L':
        al_draw_bitmap(font16_bitmap[2][12], x, y, 0);
        return 0;
    case 'M':
        al_draw_bitmap(font16_bitmap[2][13], x, y, 0);
        return 0;
    case 'N':
        al_draw_bitmap(font16_bitmap[2][14], x, y, 0);
        return 0;
    case 'O':
        al_draw_bitmap(font16_bitmap[2][15], x, y, 0);
        return 0;
    case 'P':
        al_draw_bitmap(font16_bitmap[3][0], x, y, 0);
        return 0;
    case 'Q':
        al_draw_bitmap(font16_bitmap[3][1], x, y, 0);
        return 0;
    case 'R':
        al_draw_bitmap(font16_bitmap[3][2], x, y, 0);
        return 0;
    case 'S':
        al_draw_bitmap(font16_bitmap[3][3], x, y, 0);
        return 0;
    case 'T':
        al_draw_bitmap(font16_bitmap[3][4], x, y, 0);
        return 0;
    case 'U':
        al_draw_bitmap(font16_bitmap[3][5], x, y, 0);
        return 0;
    case 'V':
        al_draw_bitmap(font16_bitmap[3][6], x, y, 0);
        return 0;
    case 'W':
        al_draw_bitmap(font16_bitmap[3][7], x, y, 0);
        return 0;
    case 'X':
        al_draw_bitmap(font16_bitmap[3][8], x, y, 0);
        return 0;
    case 'Y':
        al_draw_bitmap(font16_bitmap[3][9], x, y, 0);
        return 0;
    case 'Z':
        al_draw_bitmap(font16_bitmap[3][10], x, y, 0);
        return 0;
    case '[':
        al_draw_bitmap(font16_bitmap[3][11], x, y, 0);
        return 0;
    case '\\':
        al_draw_bitmap(font16_bitmap[3][12], x, y, 0);
        return 0;
    case ']':
        al_draw_bitmap(font16_bitmap[3][13], x, y, 0);
        return 0;
    case '^':
        al_draw_bitmap(font16_bitmap[3][14], x, y, 0);
        return 0;
    case '_':
        al_draw_bitmap(font16_bitmap[3][15], x, y, 0);
        return 0;
    case '`':
        al_draw_bitmap(font16_bitmap[4][0], x, y, 0);
        return 0;
    case 'a':
        al_draw_bitmap(font16_bitmap[4][1], x, y, 0);
        return 0;
    case 'b':
        al_draw_bitmap(font16_bitmap[4][2], x, y, 0);
        return 0;
    case 'c':
        al_draw_bitmap(font16_bitmap[4][3], x, y, 0);
        return 0;
    case 'd':
        al_draw_bitmap(font16_bitmap[4][4], x, y, 0);
        return 0;
    case 'e':
        al_draw_bitmap(font16_bitmap[4][5], x, y, 0);
        return 0;
    case 'f':
        al_draw_bitmap(font16_bitmap[4][6], x, y, 0);
        return 0;
    case 'g':
        al_draw_bitmap(font16_bitmap[4][7], x, y, 0);
        return 0;
    case 'h':
        al_draw_bitmap(font16_bitmap[4][8], x, y, 0);
        return 0;
    case 'i':
        al_draw_bitmap(font16_bitmap[4][9], x, y, 0);
        return 0;
    case 'j':
        al_draw_bitmap(font16_bitmap[4][10], x, y, 0);
        return 0;
    case 'k':
        al_draw_bitmap(font16_bitmap[4][11], x, y, 0);
        return 0;
    case 'l':
        al_draw_bitmap(font16_bitmap[4][12], x, y, 0);
        return 0;
    case 'm':
        al_draw_bitmap(font16_bitmap[4][13], x, y, 0);
        return 0;
    case 'n':
        al_draw_bitmap(font16_bitmap[4][14], x, y, 0);
        return 0;
    case 'o':
        al_draw_bitmap(font16_bitmap[4][15], x, y, 0);
        return 0;
    case 'p':
        al_draw_bitmap(font16_bitmap[5][0], x, y, 0);
        return 0;
    case 'q':
        al_draw_bitmap(font16_bitmap[5][1], x, y, 0);
        return 0;
    case 'r':
        al_draw_bitmap(font16_bitmap[5][2], x, y, 0);
        return 0;
    case 's':
        al_draw_bitmap(font16_bitmap[5][3], x, y, 0);
        return 0;
    case 't':
        al_draw_bitmap(font16_bitmap[5][4], x, y, 0);
        return 0;
    case 'u':
        al_draw_bitmap(font16_bitmap[5][5], x, y, 0);
        return 0;
    case 'v':
        al_draw_bitmap(font16_bitmap[5][6], x, y, 0);
        return 0;
    case 'w':
        al_draw_bitmap(font16_bitmap[5][7], x, y, 0);
        return 0;
    case 'x':
        al_draw_bitmap(font16_bitmap[5][8], x, y, 0);
        return 0;
    case 'y':
        al_draw_bitmap(font16_bitmap[5][9], x, y, 0);
        return 0;
    case 'z':
        al_draw_bitmap(font16_bitmap[5][10], x, y, 0);
        return 0;
    case '{':
        al_draw_bitmap(font16_bitmap[5][11], x, y, 0);
        return 0;
    case '|':
        al_draw_bitmap(font16_bitmap[5][12], x, y, 0);
        return 0;
    case '}':
        al_draw_bitmap(font16_bitmap[5][13], x, y, 0);
        return 0;
    case '~':
        al_draw_bitmap(font16_bitmap[5][14], x, y, 0);
        return 0;
    default:
        fprintf(stderr, "draw_char: unknown char found (\\x%x), ignoring", c);
        return -1;
    }

    /* should not reach here */
    return -2;
}

int draw_big_yellow_char(int x, int y, int c)
{
    /* don't assume ASCII */
    switch (c)
    {
    case ' ':
        /* do nothing */
        return 0;
    case '!':
        al_draw_bitmap(font16_yellow_bitmap[0][1], x, y, 0);
        return 0;
    case '"':
        al_draw_bitmap(font16_yellow_bitmap[0][2], x, y, 0);
        return 0;
    case '#':
        al_draw_bitmap(font16_yellow_bitmap[0][3], x, y, 0);
        return 0;
    case '$':
        al_draw_bitmap(font16_yellow_bitmap[0][4], x, y, 0);
        return 0;
    case '%':
        al_draw_bitmap(font16_yellow_bitmap[0][5], x, y, 0);
        return 0;
    case '&':
        al_draw_bitmap(font16_yellow_bitmap[0][6], x, y, 0);
        return 0;
    case '\'':
        al_draw_bitmap(font16_yellow_bitmap[0][7], x, y, 0);
        return 0;
    case '(':
        al_draw_bitmap(font16_yellow_bitmap[0][8], x, y, 0);
        return 0;
    case ')':
        al_draw_bitmap(font16_yellow_bitmap[0][9], x, y, 0);
        return 0;
    case '*':
        al_draw_bitmap(font16_yellow_bitmap[0][10], x, y, 0);
        return 0;
    case '+':
        al_draw_bitmap(font16_yellow_bitmap[0][11], x, y, 0);
        return 0;
    case ',':
        al_draw_bitmap(font16_yellow_bitmap[0][12], x, y, 0);
        return 0;
    case '-':
        al_draw_bitmap(font16_yellow_bitmap[0][13], x, y, 0);
        return 0;
    case '.':
        al_draw_bitmap(font16_yellow_bitmap[0][14], x, y, 0);
        return 0;
    case '/':
        al_draw_bitmap(font16_yellow_bitmap[0][15], x, y, 0);
        return 0;
    case '0':
        al_draw_bitmap(font16_yellow_bitmap[1][0], x, y, 0);
        return 0;
    case '1':
        al_draw_bitmap(font16_yellow_bitmap[1][1], x, y, 0);
        return 0;
    case '2':
        al_draw_bitmap(font16_yellow_bitmap[1][2], x, y, 0);
        return 0;
    case '3':
        al_draw_bitmap(font16_yellow_bitmap[1][3], x, y, 0);
        return 0;
    case '4':
        al_draw_bitmap(font16_yellow_bitmap[1][4], x, y, 0);
        return 0;
    case '5':
        al_draw_bitmap(font16_yellow_bitmap[1][5], x, y, 0);
        return 0;
    case '6':
        al_draw_bitmap(font16_yellow_bitmap[1][6], x, y, 0);
        return 0;
    case '7':
        al_draw_bitmap(font16_yellow_bitmap[1][7], x, y, 0);
        return 0;
    case '8':
        al_draw_bitmap(font16_yellow_bitmap[1][8], x, y, 0);
        return 0;
    case '9':
        al_draw_bitmap(font16_yellow_bitmap[1][9], x, y, 0);
        return 0;
    case ':':
        al_draw_bitmap(font16_yellow_bitmap[1][10], x, y, 0);
        return 0;
    case ';':
        al_draw_bitmap(font16_yellow_bitmap[1][11], x, y, 0);
        return 0;
    case '<':
        al_draw_bitmap(font16_yellow_bitmap[1][12], x, y, 0);
        return 0;
    case '=':
        al_draw_bitmap(font16_yellow_bitmap[1][13], x, y, 0);
        return 0;
    case '>':
        al_draw_bitmap(font16_yellow_bitmap[1][14], x, y, 0);
        return 0;
    case '?':
        al_draw_bitmap(font16_yellow_bitmap[1][15], x, y, 0);
        return 0;
    case '@':
        al_draw_bitmap(font16_yellow_bitmap[2][0], x, y, 0);
        return 0;
    case 'A':
        al_draw_bitmap(font16_yellow_bitmap[2][1], x, y, 0);
        return 0;
    case 'B':
        al_draw_bitmap(font16_yellow_bitmap[2][2], x, y, 0);
        return 0;
    case 'C':
        al_draw_bitmap(font16_yellow_bitmap[2][3], x, y, 0);
        return 0;
    case 'D':
        al_draw_bitmap(font16_yellow_bitmap[2][4], x, y, 0);
        return 0;
    case 'E':
        al_draw_bitmap(font16_yellow_bitmap[2][5], x, y, 0);
        return 0;
    case 'F':
        al_draw_bitmap(font16_yellow_bitmap[2][6], x, y, 0);
        return 0;
    case 'G':
        al_draw_bitmap(font16_yellow_bitmap[2][7], x, y, 0);
        return 0;
    case 'H':
        al_draw_bitmap(font16_yellow_bitmap[2][8], x, y, 0);
        return 0;
    case 'I':
        al_draw_bitmap(font16_yellow_bitmap[2][9], x, y, 0);
        return 0;
    case 'J':
        al_draw_bitmap(font16_yellow_bitmap[2][10], x, y, 0);
        return 0;
    case 'K':
        al_draw_bitmap(font16_yellow_bitmap[2][11], x, y, 0);
        return 0;
    case 'L':
        al_draw_bitmap(font16_yellow_bitmap[2][12], x, y, 0);
        return 0;
    case 'M':
        al_draw_bitmap(font16_yellow_bitmap[2][13], x, y, 0);
        return 0;
    case 'N':
        al_draw_bitmap(font16_yellow_bitmap[2][14], x, y, 0);
        return 0;
    case 'O':
        al_draw_bitmap(font16_yellow_bitmap[2][15], x, y, 0);
        return 0;
    case 'P':
        al_draw_bitmap(font16_yellow_bitmap[3][0], x, y, 0);
        return 0;
    case 'Q':
        al_draw_bitmap(font16_yellow_bitmap[3][1], x, y, 0);
        return 0;
    case 'R':
        al_draw_bitmap(font16_yellow_bitmap[3][2], x, y, 0);
        return 0;
    case 'S':
        al_draw_bitmap(font16_yellow_bitmap[3][3], x, y, 0);
        return 0;
    case 'T':
        al_draw_bitmap(font16_yellow_bitmap[3][4], x, y, 0);
        return 0;
    case 'U':
        al_draw_bitmap(font16_yellow_bitmap[3][5], x, y, 0);
        return 0;
    case 'V':
        al_draw_bitmap(font16_yellow_bitmap[3][6], x, y, 0);
        return 0;
    case 'W':
        al_draw_bitmap(font16_yellow_bitmap[3][7], x, y, 0);
        return 0;
    case 'X':
        al_draw_bitmap(font16_yellow_bitmap[3][8], x, y, 0);
        return 0;
    case 'Y':
        al_draw_bitmap(font16_yellow_bitmap[3][9], x, y, 0);
        return 0;
    case 'Z':
        al_draw_bitmap(font16_yellow_bitmap[3][10], x, y, 0);
        return 0;
    case '[':
        al_draw_bitmap(font16_yellow_bitmap[3][11], x, y, 0);
        return 0;
    case '\\':
        al_draw_bitmap(font16_yellow_bitmap[3][12], x, y, 0);
        return 0;
    case ']':
        al_draw_bitmap(font16_yellow_bitmap[3][13], x, y, 0);
        return 0;
    case '^':
        al_draw_bitmap(font16_yellow_bitmap[3][14], x, y, 0);
        return 0;
    case '_':
        al_draw_bitmap(font16_yellow_bitmap[3][15], x, y, 0);
        return 0;
    case '`':
        al_draw_bitmap(font16_yellow_bitmap[4][0], x, y, 0);
        return 0;
    case 'a':
        al_draw_bitmap(font16_yellow_bitmap[4][1], x, y, 0);
        return 0;
    case 'b':
        al_draw_bitmap(font16_yellow_bitmap[4][2], x, y, 0);
        return 0;
    case 'c':
        al_draw_bitmap(font16_yellow_bitmap[4][3], x, y, 0);
        return 0;
    case 'd':
        al_draw_bitmap(font16_yellow_bitmap[4][4], x, y, 0);
        return 0;
    case 'e':
        al_draw_bitmap(font16_yellow_bitmap[4][5], x, y, 0);
        return 0;
    case 'f':
        al_draw_bitmap(font16_yellow_bitmap[4][6], x, y, 0);
        return 0;
    case 'g':
        al_draw_bitmap(font16_yellow_bitmap[4][7], x, y, 0);
        return 0;
    case 'h':
        al_draw_bitmap(font16_yellow_bitmap[4][8], x, y, 0);
        return 0;
    case 'i':
        al_draw_bitmap(font16_yellow_bitmap[4][9], x, y, 0);
        return 0;
    case 'j':
        al_draw_bitmap(font16_yellow_bitmap[4][10], x, y, 0);
        return 0;
    case 'k':
        al_draw_bitmap(font16_yellow_bitmap[4][11], x, y, 0);
        return 0;
    case 'l':
        al_draw_bitmap(font16_yellow_bitmap[4][12], x, y, 0);
        return 0;
    case 'm':
        al_draw_bitmap(font16_yellow_bitmap[4][13], x, y, 0);
        return 0;
    case 'n':
        al_draw_bitmap(font16_yellow_bitmap[4][14], x, y, 0);
        return 0;
    case 'o':
        al_draw_bitmap(font16_yellow_bitmap[4][15], x, y, 0);
        return 0;
    case 'p':
        al_draw_bitmap(font16_yellow_bitmap[5][0], x, y, 0);
        return 0;
    case 'q':
        al_draw_bitmap(font16_yellow_bitmap[5][1], x, y, 0);
        return 0;
    case 'r':
        al_draw_bitmap(font16_yellow_bitmap[5][2], x, y, 0);
        return 0;
    case 's':
        al_draw_bitmap(font16_yellow_bitmap[5][3], x, y, 0);
        return 0;
    case 't':
        al_draw_bitmap(font16_yellow_bitmap[5][4], x, y, 0);
        return 0;
    case 'u':
        al_draw_bitmap(font16_yellow_bitmap[5][5], x, y, 0);
        return 0;
    case 'v':
        al_draw_bitmap(font16_yellow_bitmap[5][6], x, y, 0);
        return 0;
    case 'w':
        al_draw_bitmap(font16_yellow_bitmap[5][7], x, y, 0);
        return 0;
    case 'x':
        al_draw_bitmap(font16_yellow_bitmap[5][8], x, y, 0);
        return 0;
    case 'y':
        al_draw_bitmap(font16_yellow_bitmap[5][9], x, y, 0);
        return 0;
    case 'z':
        al_draw_bitmap(font16_yellow_bitmap[5][10], x, y, 0);
        return 0;
    case '{':
        al_draw_bitmap(font16_yellow_bitmap[5][11], x, y, 0);
        return 0;
    case '|':
        al_draw_bitmap(font16_yellow_bitmap[5][12], x, y, 0);
        return 0;
    case '}':
        al_draw_bitmap(font16_yellow_bitmap[5][13], x, y, 0);
        return 0;
    case '~':
        al_draw_bitmap(font16_yellow_bitmap[5][14], x, y, 0);
        return 0;
    default:
        fprintf(stderr, "draw_char: unknown char found (\\x%x), ignoring", c);
        return -1;
    }

    /* should not reach here */
    return -2;
}

int draw_big_color_char(int x, int y, int c)
{
    /* don't assume ASCII */
    switch (c)
    {
    case ' ':
        /* do nothing */
        return 0;
    case '!':
        al_draw_bitmap(font16_color_bitmap[0][1], x, y, 0);
        return 0;
    case '"':
        al_draw_bitmap(font16_color_bitmap[0][2], x, y, 0);
        return 0;
    case '#':
        al_draw_bitmap(font16_color_bitmap[0][3], x, y, 0);
        return 0;
    case '$':
        al_draw_bitmap(font16_color_bitmap[0][4], x, y, 0);
        return 0;
    case '%':
        al_draw_bitmap(font16_color_bitmap[0][5], x, y, 0);
        return 0;
    case '&':
        al_draw_bitmap(font16_color_bitmap[0][6], x, y, 0);
        return 0;
    case '\'':
        al_draw_bitmap(font16_color_bitmap[0][7], x, y, 0);
        return 0;
    case '(':
        al_draw_bitmap(font16_color_bitmap[0][8], x, y, 0);
        return 0;
    case ')':
        al_draw_bitmap(font16_color_bitmap[0][9], x, y, 0);
        return 0;
    case '*':
        al_draw_bitmap(font16_color_bitmap[0][10], x, y, 0);
        return 0;
    case '+':
        al_draw_bitmap(font16_color_bitmap[0][11], x, y, 0);
        return 0;
    case ',':
        al_draw_bitmap(font16_color_bitmap[0][12], x, y, 0);
        return 0;
    case '-':
        al_draw_bitmap(font16_color_bitmap[0][13], x, y, 0);
        return 0;
    case '.':
        al_draw_bitmap(font16_color_bitmap[0][14], x, y, 0);
        return 0;
    case '/':
        al_draw_bitmap(font16_color_bitmap[0][15], x, y, 0);
        return 0;
    case '0':
        al_draw_bitmap(font16_color_bitmap[1][0], x, y, 0);
        return 0;
    case '1':
        al_draw_bitmap(font16_color_bitmap[1][1], x, y, 0);
        return 0;
    case '2':
        al_draw_bitmap(font16_color_bitmap[1][2], x, y, 0);
        return 0;
    case '3':
        al_draw_bitmap(font16_color_bitmap[1][3], x, y, 0);
        return 0;
    case '4':
        al_draw_bitmap(font16_color_bitmap[1][4], x, y, 0);
        return 0;
    case '5':
        al_draw_bitmap(font16_color_bitmap[1][5], x, y, 0);
        return 0;
    case '6':
        al_draw_bitmap(font16_color_bitmap[1][6], x, y, 0);
        return 0;
    case '7':
        al_draw_bitmap(font16_color_bitmap[1][7], x, y, 0);
        return 0;
    case '8':
        al_draw_bitmap(font16_color_bitmap[1][8], x, y, 0);
        return 0;
    case '9':
        al_draw_bitmap(font16_color_bitmap[1][9], x, y, 0);
        return 0;
    case ':':
        al_draw_bitmap(font16_color_bitmap[1][10], x, y, 0);
        return 0;
    case ';':
        al_draw_bitmap(font16_color_bitmap[1][11], x, y, 0);
        return 0;
    case '<':
        al_draw_bitmap(font16_color_bitmap[1][12], x, y, 0);
        return 0;
    case '=':
        al_draw_bitmap(font16_color_bitmap[1][13], x, y, 0);
        return 0;
    case '>':
        al_draw_bitmap(font16_color_bitmap[1][14], x, y, 0);
        return 0;
    case '?':
        al_draw_bitmap(font16_color_bitmap[1][15], x, y, 0);
        return 0;
    case '@':
        al_draw_bitmap(font16_color_bitmap[2][0], x, y, 0);
        return 0;
    case 'A':
        al_draw_bitmap(font16_color_bitmap[2][1], x, y, 0);
        return 0;
    case 'B':
        al_draw_bitmap(font16_color_bitmap[2][2], x, y, 0);
        return 0;
    case 'C':
        al_draw_bitmap(font16_color_bitmap[2][3], x, y, 0);
        return 0;
    case 'D':
        al_draw_bitmap(font16_color_bitmap[2][4], x, y, 0);
        return 0;
    case 'E':
        al_draw_bitmap(font16_color_bitmap[2][5], x, y, 0);
        return 0;
    case 'F':
        al_draw_bitmap(font16_color_bitmap[2][6], x, y, 0);
        return 0;
    case 'G':
        al_draw_bitmap(font16_color_bitmap[2][7], x, y, 0);
        return 0;
    case 'H':
        al_draw_bitmap(font16_color_bitmap[2][8], x, y, 0);
        return 0;
    case 'I':
        al_draw_bitmap(font16_color_bitmap[2][9], x, y, 0);
        return 0;
    case 'J':
        al_draw_bitmap(font16_color_bitmap[2][10], x, y, 0);
        return 0;
    case 'K':
        al_draw_bitmap(font16_color_bitmap[2][11], x, y, 0);
        return 0;
    case 'L':
        al_draw_bitmap(font16_color_bitmap[2][12], x, y, 0);
        return 0;
    case 'M':
        al_draw_bitmap(font16_color_bitmap[2][13], x, y, 0);
        return 0;
    case 'N':
        al_draw_bitmap(font16_color_bitmap[2][14], x, y, 0);
        return 0;
    case 'O':
        al_draw_bitmap(font16_color_bitmap[2][15], x, y, 0);
        return 0;
    case 'P':
        al_draw_bitmap(font16_color_bitmap[3][0], x, y, 0);
        return 0;
    case 'Q':
        al_draw_bitmap(font16_color_bitmap[3][1], x, y, 0);
        return 0;
    case 'R':
        al_draw_bitmap(font16_color_bitmap[3][2], x, y, 0);
        return 0;
    case 'S':
        al_draw_bitmap(font16_color_bitmap[3][3], x, y, 0);
        return 0;
    case 'T':
        al_draw_bitmap(font16_color_bitmap[3][4], x, y, 0);
        return 0;
    case 'U':
        al_draw_bitmap(font16_color_bitmap[3][5], x, y, 0);
        return 0;
    case 'V':
        al_draw_bitmap(font16_color_bitmap[3][6], x, y, 0);
        return 0;
    case 'W':
        al_draw_bitmap(font16_color_bitmap[3][7], x, y, 0);
        return 0;
    case 'X':
        al_draw_bitmap(font16_color_bitmap[3][8], x, y, 0);
        return 0;
    case 'Y':
        al_draw_bitmap(font16_color_bitmap[3][9], x, y, 0);
        return 0;
    case 'Z':
        al_draw_bitmap(font16_color_bitmap[3][10], x, y, 0);
        return 0;
    case '[':
        al_draw_bitmap(font16_color_bitmap[3][11], x, y, 0);
        return 0;
    case '\\':
        al_draw_bitmap(font16_color_bitmap[3][12], x, y, 0);
        return 0;
    case ']':
        al_draw_bitmap(font16_color_bitmap[3][13], x, y, 0);
        return 0;
    case '^':
        al_draw_bitmap(font16_color_bitmap[3][14], x, y, 0);
        return 0;
    case '_':
        al_draw_bitmap(font16_color_bitmap[3][15], x, y, 0);
        return 0;
    case '`':
        al_draw_bitmap(font16_color_bitmap[4][0], x, y, 0);
        return 0;
    case 'a':
        al_draw_bitmap(font16_color_bitmap[4][1], x, y, 0);
        return 0;
    case 'b':
        al_draw_bitmap(font16_color_bitmap[4][2], x, y, 0);
        return 0;
    case 'c':
        al_draw_bitmap(font16_color_bitmap[4][3], x, y, 0);
        return 0;
    case 'd':
        al_draw_bitmap(font16_color_bitmap[4][4], x, y, 0);
        return 0;
    case 'e':
        al_draw_bitmap(font16_color_bitmap[4][5], x, y, 0);
        return 0;
    case 'f':
        al_draw_bitmap(font16_color_bitmap[4][6], x, y, 0);
        return 0;
    case 'g':
        al_draw_bitmap(font16_color_bitmap[4][7], x, y, 0);
        return 0;
    case 'h':
        al_draw_bitmap(font16_color_bitmap[4][8], x, y, 0);
        return 0;
    case 'i':
        al_draw_bitmap(font16_color_bitmap[4][9], x, y, 0);
        return 0;
    case 'j':
        al_draw_bitmap(font16_color_bitmap[4][10], x, y, 0);
        return 0;
    case 'k':
        al_draw_bitmap(font16_color_bitmap[4][11], x, y, 0);
        return 0;
    case 'l':
        al_draw_bitmap(font16_color_bitmap[4][12], x, y, 0);
        return 0;
    case 'm':
        al_draw_bitmap(font16_color_bitmap[4][13], x, y, 0);
        return 0;
    case 'n':
        al_draw_bitmap(font16_color_bitmap[4][14], x, y, 0);
        return 0;
    case 'o':
        al_draw_bitmap(font16_color_bitmap[4][15], x, y, 0);
        return 0;
    case 'p':
        al_draw_bitmap(font16_color_bitmap[5][0], x, y, 0);
        return 0;
    case 'q':
        al_draw_bitmap(font16_color_bitmap[5][1], x, y, 0);
        return 0;
    case 'r':
        al_draw_bitmap(font16_color_bitmap[5][2], x, y, 0);
        return 0;
    case 's':
        al_draw_bitmap(font16_color_bitmap[5][3], x, y, 0);
        return 0;
    case 't':
        al_draw_bitmap(font16_color_bitmap[5][4], x, y, 0);
        return 0;
    case 'u':
        al_draw_bitmap(font16_color_bitmap[5][5], x, y, 0);
        return 0;
    case 'v':
        al_draw_bitmap(font16_color_bitmap[5][6], x, y, 0);
        return 0;
    case 'w':
        al_draw_bitmap(font16_color_bitmap[5][7], x, y, 0);
        return 0;
    case 'x':
        al_draw_bitmap(font16_color_bitmap[5][8], x, y, 0);
        return 0;
    case 'y':
        al_draw_bitmap(font16_color_bitmap[5][9], x, y, 0);
        return 0;
    case 'z':
        al_draw_bitmap(font16_color_bitmap[5][10], x, y, 0);
        return 0;
    case '{':
        al_draw_bitmap(font16_color_bitmap[5][11], x, y, 0);
        return 0;
    case '|':
        al_draw_bitmap(font16_color_bitmap[5][12], x, y, 0);
        return 0;
    case '}':
        al_draw_bitmap(font16_color_bitmap[5][13], x, y, 0);
        return 0;
    case '~':
        al_draw_bitmap(font16_color_bitmap[5][14], x, y, 0);
        return 0;
    default:
        fprintf(stderr, "draw_char: unknown char found (\\x%x), ignoring", c);
        return -1;
    }

    /* should not reach here */
    return -2;
}
