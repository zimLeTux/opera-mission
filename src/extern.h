#ifndef H_EXTERN
#define H_EXTERN

#include <allegro5/allegro.h>
#include <allegro5/allegro_audio.h>

#include "manage.h"

#define debug(s) printf(s "\n");fflush(stdout)

extern ALLEGRO_DISPLAY* display;
extern ALLEGRO_EVENT_QUEUE* event_queue;
extern ALLEGRO_TIMER* fps;

extern ALLEGRO_BITMAP* player1_bitmap;
extern ALLEGRO_BITMAP* player2_bitmap;
extern ALLEGRO_BITMAP* pshot1_bitmap;
extern ALLEGRO_BITMAP* pshot2_bitmap;
extern ALLEGRO_BITMAP* pshot3_bitmap;
extern ALLEGRO_BITMAP* eshot_bitmap;
extern ALLEGRO_BITMAP* elaser_bitmap;
extern ALLEGRO_BITMAP* emissile_bitmap;
extern ALLEGRO_BITMAP* ebound_bitmap;
extern ALLEGRO_BITMAP* ering_bitmap;
extern ALLEGRO_BITMAP* bomb_bitmap;
extern ALLEGRO_BITMAP* bigbomb_bitmap;
extern ALLEGRO_BITMAP* enemy1_bitmap;
extern ALLEGRO_BITMAP* enemy2_bitmap;
extern ALLEGRO_BITMAP* enemy3_bitmap;
extern ALLEGRO_BITMAP* enemy4_bitmap;
extern ALLEGRO_BITMAP* enemy5_bitmap;
extern ALLEGRO_BITMAP* enemy6_bitmap;
extern ALLEGRO_BITMAP* enemy7_bitmap;
extern ALLEGRO_BITMAP* boss1_bitmap;
extern ALLEGRO_BITMAP* boss2_bitmap;
extern ALLEGRO_BITMAP* boss3_bitmap;
extern ALLEGRO_BITMAP* boss4_bitmap;
extern ALLEGRO_BITMAP* boss5_bitmap;
extern ALLEGRO_BITMAP* boss6_bitmap;
extern ALLEGRO_BITMAP* boss7_bitmap;
extern ALLEGRO_BITMAP* item_bitmap;

extern ALLEGRO_BITMAP* font_bitmap[6];

extern ALLEGRO_SAMPLE* intro_sample;
extern ALLEGRO_SAMPLE* level1_sample;

extern CharManage* manage;
extern PlayerData* player;

extern int fieldw;
extern int fieldh;

extern int action_keys_flags;

extern int star_ptn_1;
extern int star_ptn_2;

extern Record high_score[10];

#endif /* H_EXTERN */
