#include <string.h>

#include <allegro5/allegro.h>

#include "high_scores.h"
#include "star.h"
#include "extern.h"
#include "graphics.h"
#include "keys.h"
#include "constants.h"

ALLEGRO_BITMAP* cursor_bitmap;

void display_high_score_input(char* name, int cur)
{
    al_draw_bitmap(cursor_bitmap, 264+16*cur, 236, 0);
    draw_big_yellow_string(168, 200, "Enter your name", strlen("Enter your name"));
    draw_big_string(264, 252, name, strlen(name));
}

void input_high_score()
{
    bool status = true;
    bool redraw = false;

    int key;
    ALLEGRO_EVENT event;

    char name[4] = {'A', 'A', 'A', 0};
    int cur = 0;

	cursor_bitmap = al_load_bitmap("data/pixmap/cursor.tga");
    
    change_star_parameter(star_ptn_1, 5);
    change_star_parameter(star_ptn_2, 10);

	al_start_timer(fps);

    while (status)
    {
        if(!al_is_event_queue_empty(event_queue))
        {
            while(al_get_next_event(event_queue, &event))
            {
                switch (event.type)
                {
				case ALLEGRO_EVENT_TIMER:
                    redraw = true;
					break;
                case ALLEGRO_EVENT_DISPLAY_CLOSE:
                    manage->program_should_quit = true;
                    status = false;
                    break;
                case ALLEGRO_EVENT_KEY_DOWN:
                    key = event.keyboard.keycode;
                    switch (key)
                    {
                    case KEY_P1_RIGHT:
                        if (cur < 3)
						    cur++;
                        break;
                    case KEY_P1_LEFT:
                        if (cur > 0)
						    cur--;
                        break;
                    case KEY_P1_UP:
                        if (name[cur] < 'A')
                            name[cur] = 'Z';
                        else
                            name[cur]--;
                        break;
                    case KEY_P1_DOWN:
                        if (name[cur] > 'Z')
                            name[cur] = 'A';
                        else
                            name[cur]++;
                        break;
                    case KEY_P1_SHOOT:
                        record_high_score(name,
										  manage->number_of_players,
										  player->rec[0].score);
                        return;
                    }

                    break;
                }
            }
        }

        if (redraw)
        {
            redraw = false;
            al_clear_to_color(al_map_rgb(0,0,0));
            
            draw_star(star_ptn_1);
            draw_star(star_ptn_2);

			display_high_score_input(name, cur);
            
            al_flip_display();
        }
    }
}


void record_high_score(char name[], int player, int score)
{
    FILE* score_file = NULL;
    int scores[11];
    int players[11];
    char names[11][4];
    int i, j;
    char buffer[100];

    scores[10] = 0;

    printf("inputs: %s %d %d\n", name, player, score);

    if (NULL == (score_file = fopen("highscores", "r")))
    {
        fprintf(stderr, "record_high_score: unable to open best scores file");
        exit(EXIT_FAILURE);
    }

    for (i = 0; i < 10; i++)
    {
        fgets(buffer, 100, score_file);
        buffer[99] = 0;
        players[i] = atoi(&buffer[0]);
        buffer[0] = ' ';
        names[i][0] = buffer[1];
        names[i][1] = buffer[2];
        names[i][2] = buffer[3];
        names[i][3] = 0;
        buffer[1] = ' ';
        buffer[2] = ' ';
        buffer[3] = ' ';
        scores[i] = atoi(buffer);
        printf("%s, players: %d, score: %d\n", names[i], players[i], scores[i]);
    }

	fclose(score_file);
	if (NULL == (score_file = fopen("highscores", "w")))
    {
        fprintf(stderr, "record_high_score: unable to open best scores file");
        exit(EXIT_FAILURE);
    }

    /* sort scores */
    for (i = 9; i >= 0; i--)
    {
        if (score < scores[i])
        {
            for (j = 9; j > i+1; j--)
            {
                scores[j] = scores[j-1];
                strcpy(names[j], names[j-1]);
                players[j] = players[j-1];
            }
            break;
        }
		else
		  continue;
    }
    scores[j] = score;
    players[j] = player;
    strcpy(names[j], name);

    printf("---\n");
    for (i = 0; i < 10; i++)
    {
        printf("%d%s%d\n", players[i], names[i], scores[i]);
        fprintf(score_file, "%d%s%d\n",
				  players[i], names[i], scores[i]);
    }

    fclose(score_file);
}


void load_high_score(int scores[], int players[], char** names)
{
    FILE* score_file = NULL;
    int i;
    char buffer[100];
    if (NULL == (score_file = fopen("highscores", "r")))
    {
        fprintf(stderr, "load_high_score: unable to open best scores file");
        exit(EXIT_FAILURE);
    }

    for (i = 0; i < 10; i++)
    {
        fgets(buffer, 100, score_file);
        buffer[99] = 0;
        players[i] = atoi(&buffer[0]);
        buffer[0] = ' ';
        names[i][0] = buffer[1];
        names[i][1] = buffer[2];
        names[i][2] = buffer[3];
        names[i][3] = 0;
        buffer[1] = ' ';
        buffer[2] = ' ';
        buffer[3] = ' ';
        scores[i] = atoi(buffer);
    }

	fclose(score_file);
}
