#include <allegro5/allegro.h>

#include <allegro5/allegro_primitives.h>


#include "image.h"
#include "game.h"
#include "opera.h"
#include "extern.h"
#include "enemy_shot.h"
#include "callback.h"

int fieldw;
int fieldh;

/* action */
/* do nothing */
DelAtt NullAct(ObjData *self)
{
    return NoneDel;
}


/* hit */
/* nothing can hit me, I am immutable */
DelAtt NullHit(ObjData *self, ObjData *your)
{
    return NoneDel;
}

/* simply die */
DelAtt NullDelHit(ObjData *self, ObjData *your)
{
    return NullDel;
}

/* die with explosion */
DelAtt DeleteHit(ObjData *self, ObjData *your)
{
    new_bomb(self->x, self->y);
    return self->enemy_att;
}

/* deal damage, explode if dead */
DelAtt DamageHit(ObjData *self, ObjData *your)
{
    int temp = your->attack;
    if (self->hp < your->attack)
        temp = self->hp;

    self->hp -= temp;
    player->rec[0].score += temp;
    if (self->hp <= 0)
    {
        player->rec[0].score -= 1;
        if (manage->loop > 2)
            shot_to_point(self->x, self->y, manage->player[0]->data.x, manage->player[0]->data.y,5);
        new_bomb(self->x, self->y);

        self->show_damage_time = 0;
        return self->enemy_att;
    }
    else
    {
        self->show_damage_time = 15;
        return NoneDel;
    }

}

/* same above, but with big explosion */
DelAtt LargeDamageHit(ObjData *self, ObjData *your)
{
    int temp = your->attack;
    if (self->hp < your->attack)
        temp = self->hp;

    self->hp -= temp;
    player->rec[0].score += temp;

    if (self->hp <= 0)
    {
        player->rec[0].score -= 1;
        if (manage->loop > 2)
            shot_to_point(self->x, self->y, manage->player[0]->data.x, manage->player[0]->data.y, 5);
        new_large_bomb(self->x, self->y);

        self->show_damage_time = 0;
        return self->enemy_att;
    }
    else
    {
        self->show_damage_time = 15;
        return NoneDel;
    }

}


DelAtt BombAct(ObjData *self)
{
    self->image = self->cnt[0];
    self->cnt[0]++;
    if (self->cnt[0] > 5)
        return NullDel;
    return NoneDel;
}

DelAtt EnemyShotAct(ObjData *self)
{
    self->cnt[0] += self->cnt[2]*self->speed;
    self->x = self->cnt[0] / 256;
    self->cnt[1] += self->cnt[3]*self->speed;
    self->y = self->cnt[1] / 256;

    self->cnt[4]++;
    if (self->cnt[4] >= 3)
    {
        self->cnt[4] = 0;
        self->image++;
        if (self->image > 3)
            self->image = 0;
    }

    if ((self->x < 0 - self->width/2) || (self->x > fieldw + self->width/2))
        return NullDel;
    if ((self->y < 0 - self->height/2) || (self->y > fieldh + self->height/2))
        return NullDel;

    return NoneDel;
}

/* display */
void NullReal(ObjData *self, GrpData *grp)
{
    return;
}


/* display pixmap */
void DrawImage(ObjData *self, GrpData *grp)
{
    /*debug("DRAW IMAGE?");
    printf("grp->image = %p\n", grp->image);
    printf("self->image = %d\n", self->image);
    printf("grp->image[self->image] = %p\n", grp->image[self->image]);*/
    /* TODO: provisoire... */
    /*if (NULL == grp->image[self->image])
        return;*/
    al_draw_bitmap_region(*(grp->image),
                          0, self->image * grp->height,
                          grp->width, grp->height,
                          self->x - grp->harfw, self->y - grp->harfh,
                          0);


    return;
}
